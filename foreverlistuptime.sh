{
forever columns reset
forever columns rm uid
forever columns rm command
forever columns rm script
forever columns rm forever 
forever columns rm pid
forever columns rm logfile
forever columns rm uptime

forever columns add uptime
} &> /dev/null
forever list
{
forever columns reset
} &> /dev/null
