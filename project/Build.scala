import sbt._
import Keys._
import sbtassembly.AssemblyKeys._
import tools.nsc.io.File


object Build extends Build {

  // -------------------------------------------------------- [Libraries] --------------------------------------------------------

  val commons = Seq("commons-lang" % "commons-lang" % "2.6")
  val logback = Seq("ch.qos.logback" % "logback-classic" % "1.1.2")
  val inject = Seq("com.google.inject" % "guice" % "3.0")
  val bouncy = Seq("org.bouncycastle" % "bcprov-jdk15on" % "1.47")
  val scalatest = Seq("org.scalatest" % "scalatest_2.10.0" % "2.0.M5")
  val commonsIo = Seq("commons-io" % "commons-io" % "2.4")
  val akka = Seq("com.typesafe.akka" % "akka-actor_2.10" % "2.3.10")
  val akkaCluster = Seq("com.typesafe.akka" % "akka-cluster_2.10" % "2.3.4")
  val akkaTestkit = Seq("com.typesafe.akka" % "akka-testkit_2.10" % "2.3.4")
  val akkaSlf4j = Seq("com.typesafe.akka" % "akka-slf4j_2.10" % "2.3.4")
  val commonsEmail = Seq("org.apache.commons" % "commons-email" % "1.3.2")
  val httpClient = Seq("net.databinder.dispatch" %% "dispatch-core" % "0.11.1")
  val json = Seq("io.spray" %% "spray-json" % "1.3.0")
  val joda = Seq("joda-time" % "joda-time" % "2.5") ++ Seq("org.joda" % "joda-convert" % "1.2")
  val webSockets = Seq("io.backchat.hookup" % "hookup_2.10" % "0.2.3")
  val mysql = Seq("mysql" % "mysql-connector-java" % "5.1.12")
  val json4sNative = Seq("org.json4s" %% "json4s-native" % "3.2.11")
  val scalaFx = Seq("org.scalafx" %% "scalafx" % "2.2.60-R9")
  val websocketClient =  Seq("org.eclipse.jetty.websocket" % "websocket-client" % "9.3.0.M2")

  //========================================================= [General] =========================================================


  lazy val utils = project in file("hawu-utils") settings(
    name := "hawu-utils",
    libraryDependencies ++=  websocketClient ++ json4sNative ++ scalatest ++ akka ++ akkaSlf4j ++ commons ++ logback ++ inject ++ bouncy ++ commonsIo ++ joda ++ mysql
    )

  lazy val manager = project in file(".") settings(
    name := "virtualclass-manager",
    assemblyJarName in assembly := "edusio-virtualclass-manager.jar",
    libraryDependencies ++= scalatest ++ akka ++ akkaSlf4j ++ commons ++ logback ++ inject ++ bouncy ++ commonsIo ++ joda ++ mysql
    ) dependsOn(utils)

  lazy val stat = (project in file("edusio-stat"))
    .settings(
      assemblyJarName in assembly := "edusio-statistics-generator.jar",
      libraryDependencies ++= commons ++ logback ++ inject ++ akka ++ akkaSlf4j ++ mysql ++ joda
    ) dependsOn(utils)

  lazy val classConnectorLanguage = project in file("virtualclass-connector-language") settings(
    name := "virtualclass-connector-language",
    libraryDependencies ++= json ++ json4sNative
    )

  lazy val classConnector = project in file("virtualclass-connector") settings(
    name := "virtualclass-connector",
    assemblyJarName in assembly := "edusio-virtualclass-connector.jar",
    libraryDependencies ++= scalatest ++ akka ++ akkaSlf4j ++ commons ++ logback ++ inject ++ bouncy ++ commonsIo ++ joda ++ mysql ++ webSockets ++ json
    ) dependsOn (classConnectorLanguage, utils)


  val javaHome = if (System.getenv("JAVA_HOME") == null) {
    System.getProperty("java_home")
  } else {
    System.getenv("JAVA_HOME")
  }


  lazy val classConnectorClient = project in file("virtualclass-connector-client") settings(
    name := "virtualclass-connector-client",
    libraryDependencies ++=  scalatest ++ akka ++ commons ++ logback ++ inject ++ bouncy ++ commonsIo ++ joda ++ webSockets ++ scalaFx ++ json,
    unmanagedJars in Compile += Attributed.blank(file(javaHome + "/jre/lib/jfxrt.jar")),
    fork := false
    ) dependsOn(classConnectorLanguage, utils)

  lazy val classConnectorTest = project in file("virtualclass-connector-test") settings(
    name := "virtualclass-connector-test",
    libraryDependencies ++= scalatest ++ akka ++ commons ++ logback ++ inject ++ bouncy ++ commonsIo ++ joda ++ webSockets ++ scalaFx ++ json,
    unmanagedJars in Compile += Attributed.blank(file(javaHome + "/jre/lib/jfxrt.jar")),
    fork := false
    ) dependsOn(classConnectorClient, classConnector)
}
