package com.edusio.virtualclassconnector.messages

import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._
import spray.json.DefaultJsonProtocol

/**
 * Created by arnostkuchar on 04.12.14.
 */

trait JsonDeserializable {
  def fromJson(json: JValue): JsonDeserializable
}

trait JsonSerializable {
  def toJson(json: JsonSerializable): JValue
}

case class Login(userName: String = "", hashedPassword: String = "", salt: String = "") extends JsonDeserializable with JsonSerializable {
  def fromJson(json: JValue): JsonDeserializable = {
    implicit lazy val formats = org.json4s.DefaultFormats
    Login(
      (json \ "UserName").extract[String],
      (json \ "HashedPassword").extract[String],
      (json \ "Salt").extract[String]
    )
  }

  def toJson(json: JsonSerializable): JValue = {
    ("UserName" -> userName) ~
      ("HashedPassword" -> hashedPassword) ~
      ("Salt" -> salt)
  }

  def this() =
    this("", "", "")

}

case class LoginResult(errorCode: String, logged: Boolean,  user: String, ngrokToken: String = "", ngrokDomain: String = "", vNCPassword: String = "", virtualClasses: List[String] = List()) extends JsonDeserializable with JsonSerializable {

  def fromJson(json: JValue): JsonDeserializable = {
    implicit lazy val formats = org.json4s.DefaultFormats
    LoginResult(
      (json \ "ErrorCode").extract[String],
      (json \ "Logged").extract[Boolean],
      (json \ "User").extract[String],
      (json \ "NgrokToken").extract[String],
      (json \ "NgrokDomain").extract[String],
      (json \ "VNCPassword").extract[String],
      (json \ "VirtualClasses").extract[List[String]]
    )
  }

  def toJson(json: JsonSerializable): JValue = {
    ("ErrorCode" -> errorCode) ~
      ("Logged" -> logged) ~
      ("NgrokToken" -> ngrokToken) ~
      ("NgrokDomain" -> ngrokDomain) ~
      ("VNCPassword" -> vNCPassword) ~
      ("VirtualClasses" -> virtualClasses) ~
      ("User" -> user)
  }

  def this() =
    this("", false, "", "", "", "", List())
}

case class ServicesStatus(ngrokRunning: Boolean = false, ngrokPort: Int = 0, vNCProxyRunning: Boolean = false, vNCRunning: Boolean = false, vNCPort: Int = 0, vNCDefinedPassword: String = "") extends JsonDeserializable with JsonSerializable {
  def fromJson(json: JValue): JsonDeserializable = {
    implicit lazy val formats = org.json4s.DefaultFormats
    ServicesStatus(
      (json \ "NgrokRunning").extract[Boolean],
      (json \ "NgrokPort").extract[Int],
      (json \ "VNCProxyRunning").extract[Boolean],
      (json \ "VNCRunning").extract[Boolean],
      (json \ "VNCPort").extract[Int],
      (json \ "VNCUserPassword").extract[String]
    )
  }

  def toJson(json: JsonSerializable): JValue = {
    ("NgrokRunning" -> ngrokRunning) ~
      ("NgrokPort" -> ngrokPort) ~
      ("VNCProxyRunning" -> vNCProxyRunning) ~
      ("VNCRunning" -> vNCRunning) ~
      ("VNCPort" -> vNCPort) ~
      ("VNCUserPassword" -> vNCDefinedPassword)
  }

  def this() =
    this(false, 0, false, false, 0)
}

case class VirtualClassesUpdate(classes: List[String]) extends JsonDeserializable with JsonSerializable {
  def toJson(json: JsonSerializable): JValue = {
    "Classes" -> classes
  }

  def fromJson(json: JValue): JsonDeserializable = {
    implicit lazy val formats = org.json4s.DefaultFormats
    VirtualClassesUpdate(
      (json \ "Classes").extract[List[String]]
    )
  }

  def this() =
    this(List())
}


case class Ask(value: String = "") extends JsonDeserializable with JsonSerializable {
  def fromJson(json: JValue): JsonDeserializable = {
    implicit lazy val formats = org.json4s.DefaultFormats
    Ask(
      (json \ "Value").extract[String]
    )
  }

  def toJson(json: JsonSerializable): JValue = {
    ("Value" -> value)
  }

  def this() =
    this("")
}

case class Ack(value: String = "") extends JsonDeserializable with JsonSerializable {
  def fromJson(json: JValue): JsonDeserializable = {
    implicit lazy val formats = org.json4s.DefaultFormats
    Ack(
      (json \ "Value").extract[String]
    )
  }

  def toJson(json: JsonSerializable): JValue = {
    ("Value" -> value)
  }

  def this() =
    this("")
}


case class GetToken(value: String="") extends JsonDeserializable with JsonSerializable {
  def fromJson(json: JValue): JsonDeserializable = {
    implicit lazy val formats = org.json4s.DefaultFormats
    GetToken("")
  }

  def toJson(json: JsonSerializable): JValue = {
    ("Value" -> value)
  }

  def this() =
    this("")
}

case class Token(token: List[String], user: List[String]) extends JsonDeserializable with JsonSerializable {
  def fromJson(json: JValue): JsonDeserializable = {
    implicit lazy val formats = org.json4s.DefaultFormats
    Token(
      (json \ "Token").extract[List[String]],
      (json \ "User").extract[List[String]]
    )
  }

  def toJson(json: JsonSerializable): JValue = {
    ("Token" -> token) ~
      ("User" -> user)
  }

  def this() =
    this(List(), List())
}