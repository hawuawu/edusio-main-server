package com.edusio.virtualclassconnector.client.helpers

import javafx.animation.{Interpolator, Transition}
import javafx.geometry.Rectangle2D
import javafx.scene.image.ImageView
import javafx.util.Duration

/**
 * Created by arnostkuchar on 05.06.15.
 */

object SpriteAnimationSpriteSheetFormat extends Enumeration {
  val RECTANGLE, LINE = Value
}

class SpriteAnimation(
                       val imageView: ImageView,
                       val duration: Duration,
                       val count: Int,
                       val columns: Int,
                       val offsetX: Int,
                       val offsetY: Int,
                       val width: Int,
                       val height: Int,
                       val format: SpriteAnimationSpriteSheetFormat.Value
                       ) extends Transition {

  private var lastIndex: Int = _

  setCycleDuration(duration);
  setInterpolator(Interpolator.LINEAR);

  protected def interpolate(k: Double) {
    format match {
      case SpriteAnimationSpriteSheetFormat.LINE =>
        val index = Math.min(Math.floor(k * count).toInt, count - 1)
        if (index != lastIndex) {
          val x = (index % columns) * width + offsetX
          val y = offsetY
          imageView.setViewport(new Rectangle2D(x, y, width, height))
          lastIndex = index
        }

      case SpriteAnimationSpriteSheetFormat.RECTANGLE =>
        val index = Math.min(Math.floor(k * count).toInt, count - 1)
        if (index != lastIndex) {
          val x = (index % columns) * width + offsetX
          val y = (index / columns) * height + offsetY
          imageView.setViewport(new Rectangle2D(x, y, width, height))
          lastIndex = index
        }
    }
  }
}