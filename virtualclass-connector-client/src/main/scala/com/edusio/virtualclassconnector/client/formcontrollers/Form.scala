package com.edusio.virtualclassconnector.client.formcontrollers

import java.awt.event.{ActionEvent, ActionListener}
import java.net.URL
import java.util.ResourceBundle
import javafx.animation.Animation
import javafx.application.{Platform}
import javafx.event.Event
import javafx.fxml
import javafx.fxml.{FXML, Initializable}
import javafx.scene.control._
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.{VBox, Pane}
import javafx.scene.paint.Color
import javafx.stage.{Stage, Window}
import javafx.util.Duration
import javax.swing.Timer
import com.apple.eawt.Application;


import akka.actor.ActorRef
import com.edusio.virtualclassconnector.client.HostServices
import com.edusio.virtualclassconnector.client.controllers._
import com.edusio.virtualclassconnector.client.helpers.{SpriteAnimationSpriteSheetFormat, SpriteAnimation}
import com.sun.deploy.uitoolkit.impl.fx.HostServicesFactory
import scala.collection.JavaConverters._

/**
 * Created by arnostkuchar on 11.12.14.
 */
class Form extends Initializable {

  private var xOffset: Double = 0;
  private var yOffset: Double = 0;

  private var actorController: ActorRef = _
  private var bouncingTimer: Option[Timer] = None
  @FXML
  var rootPanel: Pane = _

  @FXML
  var errorPaneStyleHolder: Pane = _

  @FXML
  var normalPaneStyleHolder: Pane = _

  @FXML
  var applicationTitleLabel: Label = _

  @FXML
  var localizationComboBox: ComboBox[String] = _

  @FXML
  var closeApplicationImageView: ImageView = _

  @FXML
  var classTemplateEvenLabel: Label = _

  @FXML
  var classTemplateOddLabel: Label = _

  @FXML
  var classTemplateEvenPane: Pane = _

  @FXML
  var classTemplateOddPane: Pane = _

  @FXML
  var contentPanel: Pane = _

  @FXML
  var classesPane: VBox = _

  @FXML
  var ngrokServiceLabel: Label = _

  @FXML
  var noVncServiceLabel: Label = _

  @FXML
  var vncPasswordLabel: Label = _

  @FXML
  var vncPasswordValueTextField: PasswordField = _


  @FXML
  var vncPortTextField: TextField = _

  @FXML
  var changeVNCPasswordButton: Button = _

  @FXML
  var ngrokServiceStateLabel: Label = _

  @FXML
  var noVncServiceStateLabel: Label = _

  @FXML
  var vncServiceStateLabel: Label = _

  @FXML
  var noKeyPanel: Pane = _

  @FXML
  var noKeyLabel: Label = _


  @FXML
  var noDefaultVncPane: Pane = _

  @FXML
  var noDefaultVncLabel: Label = _

  @FXML
  var noExtensionPanel: Pane = _

  @FXML
  var noExtensionLabel: Label = _

  @FXML
  var disconnectedPanel: Pane = _

  @FXML
  var disconnectedIconImageView: ImageView = _

  @FXML
  var disconnectedLabel: Label = _

  @FXML
  var applicationLoadPanel: Pane = _

  @FXML
  var loadingAppImageView: ImageView = _

  @FXML
  var loadingAppLabel: Label = _

  @FXML
  var tutorialPane: Pane = _

  @FXML
  var tutorialGoogleChromeLabel: Label = _

  @FXML
  var tutorialGoogleChromeDownloadHyperlink: Hyperlink = _

  @FXML
  var tutorialGoogleExtensionLabel: Label = _

  @FXML
  var tutorialGoogleExtensionInstallHyperlink: Hyperlink = _

  @FXML
  var tutorialRemoteVncLabel: Label = _

  @FXML
  var tutorialRemoteVncHyperlink: Hyperlink = _


  @FXML
  var tutorialDontAskAgain: CheckBox = _

  @FXML
  var tutorialNextButton: Button = _

  @FXML
  var extensionLoader: ImageView = _
  var extensionLoaderAnimation: Animation = _

  @FXML
  var connectionsLoader: ImageView = _
  var connectionsLoaderAnimation: Animation = _

  @FXML
  var virtualClassLoader: ImageView = _
  var virtualClassLoaderAnimation: Animation = _

  @FXML
  private def titleMousePressedHandler(event: Event): Unit = {
    //    print(event)
    xOffset = event.asInstanceOf[MouseEvent].getSceneX();
    yOffset = event.asInstanceOf[MouseEvent].getSceneY();
  }

  @FXML
  private def titleMouseDraggedHandler(event: Event): Unit = {
    //    print(event)
    rootPanel.getScene.getWindow.setX(event.asInstanceOf[MouseEvent].getScreenX() - xOffset);
    rootPanel.getScene.getWindow.setY(event.asInstanceOf[MouseEvent].getScreenY() - yOffset);
  }

  @FXML
  def applicationExitClicked(event: Event): Unit = {
    //    print(event)
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        actorController ! CloseApplicationInteraction
       // rootPanel.getScene().getWindow.asInstanceOf[Stage].close()
      }
    })
  }

  @FXML
  def vncPasswordHelpClicked(event: Event): Unit = {
    showTutorial
  }

  @FXML
  def languageChange(event: Event): Unit = {
    actorController ! ChangeLocaleInteraction(localizationComboBox.getValue)
  }

  @FXML
  def changeVncPasswordClicked(event: Event): Unit = {
    //    print(event)
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        try {
          actorController ! ChangeDefaultVNCInteraction(vncPasswordValueTextField.getText().trim, vncPortTextField.getText().trim.toInt)
        } catch {
          case e: Exception =>
        }
      }
    })
  }

  def loginClicked(event: Event): Unit = {/*
    if (passwordValueTextField.getText.trim != "" && userNameValueTextField.getText.trim != "") {
      actorController ! LoginInteraction(userNameValueTextField.getText.trim, passwordValueTextField.getText.trim)
    }*/
  }


  @FXML
  def noDefaultVncClicked(event: Event): Unit = {
    //    print(event)
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        hideNoDefaultVnc
      }
    })
  }

  @FXML
  def tutorialDontShowAgainAction(event: Event): Unit = {
    actorController ! DontShowAgainInteraction
  }

  @FXML
  def tutorialCloseAction(event: Event): Unit = {
    hideTutorial
  }

  @FXML
  def tutorialGoogleChromeHyperlinkAction(event: Event): Unit = {
    //val hs = new HostServices()
    //hs.showDocument("http://www.yahoo.com")
    HostServices().showDocument("http://edusio.com")
  }

  @FXML
  def tutorialExtensioChromeHyperlinkAction(event: Event): Unit = {
    //val hs = new HostServices()
    ///hs.showDocument("http://www.yahoo.com")
    HostServices().showDocument("http://edusio.com")
  }

  @FXML
  def tutorialRemoteVncHyperlinkAction(event: Event): Unit = {
    //val hs = HostServicesFactory.getInstance(rootPanel.getScene.getWindow.a);
    //hs.showDocument("http://www.yahoo.com")
    HostServices().showDocument("http://edusio.com")
  }


  override def initialize(p1: URL, p2: ResourceBundle): Unit = {
    this.disconnectedPanel.setLayoutX(this.contentPanel.getLayoutX)
    this.disconnectedPanel.setLayoutY(this.contentPanel.getLayoutY)
    this.applicationLoadPanel.setLayoutX(this.contentPanel.getLayoutX)
    this.applicationLoadPanel.setLayoutY(this.contentPanel.getLayoutY)
    this.noExtensionPanel.setLayoutX(this.contentPanel.getLayoutX)
    this.noExtensionPanel.setLayoutY(this.contentPanel.getLayoutY)
    this.noKeyPanel.setLayoutX(this.contentPanel.getLayoutX)
    this.noKeyPanel.setLayoutY(this.contentPanel.getLayoutY)
    this.noDefaultVncPane.setLayoutX(this.contentPanel.getLayoutX)
    this.noDefaultVncPane.setLayoutY(this.contentPanel.getLayoutY)
    this.tutorialPane.setLayoutX(this.contentPanel.getLayoutX)
    this.tutorialPane.setLayoutY(this.contentPanel.getLayoutY)
    this.loadingAppImageView.setVisible(true)

    connectionsLoaderAnimation = new SpriteAnimation (
      connectionsLoader,Duration.millis(500), 20, 20, 0, 0, 48, 48, SpriteAnimationSpriteSheetFormat.LINE
    )
    connectionsLoaderAnimation.setCycleCount(Animation.INDEFINITE)
    connectionsLoaderAnimation.play()

    extensionLoaderAnimation = new SpriteAnimation (
      extensionLoader,Duration.millis(500), 20, 20, 0, 0, 48, 48, SpriteAnimationSpriteSheetFormat.LINE
    )
    extensionLoaderAnimation.setCycleCount(Animation.INDEFINITE)
    extensionLoaderAnimation.play()

    virtualClassLoaderAnimation = new SpriteAnimation (
      virtualClassLoader,Duration.millis(500), 20, 20, 0, 0, 48, 48, SpriteAnimationSpriteSheetFormat.LINE
    )
    virtualClassLoaderAnimation.setCycleCount(Animation.INDEFINITE)
    virtualClassLoaderAnimation.play()

    refreshBackground
  }


  def postInitialize(controller: ActorRef): Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        actorController = controller
      }
    })
  }

  def showTutorial: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        tutorialPane.setVisible(true)
      }
    })
  }

  def hideTutorial: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
       tutorialPane.setVisible(false)
      }
    })
  }

  def showDisconnected: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        disconnectedPanel.setVisible(true)
        rootPanel.setStyle(errorPaneStyleHolder.getStyle)
      }
    })
  }

  def hideDisconnected: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        disconnectedPanel.setVisible(false)
        refreshBackground
      }
    })
  }

  def disableContent: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        contentPanel.setVisible(false)
      }
    })
  }

  def enableContent: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        contentPanel.setVisible(true)
      }
    })
  }


  def showNoLogin: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
      //  loginPanel.setVisible(true)
        noKeyPanel.setVisible(true)
        refreshBackground
      }
    })
  }

  def hideNoLogin: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
      //  loginPanel.setVisible(false)
        noKeyPanel.setVisible(false)
        refreshBackground
      }
    })
  }


  def showNoExtension: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        //  loginPanel.setVisible(true)
        noExtensionPanel.setVisible(true)
        refreshBackground
      }
    })
  }

  def hideNoExtension: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        //  loginPanel.setVisible(false)
        noExtensionPanel.setVisible(false)
        refreshBackground
      }
    })
  }

  def showNoDefaultVnc: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        //  loginPanel.setVisible(true)
        noDefaultVncPane.setVisible(true)
      }
    })
  }

  def hideNoDefaultVnc: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        //  loginPanel.setVisible(false)
        noDefaultVncPane.setVisible(false)
      }
    })
  }

  def showLoading: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        //  loginPanel.setVisible(true)
        applicationLoadPanel.setVisible(true)
      }
    })
  }

  def hideLoading: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        //  loginPanel.setVisible(false)
        applicationLoadPanel.setVisible(false)
      }
    })
  }

  def startBouncingIcon: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        //  loginPanel.setVisible(false)
        bouncingTimer.map(t => t.stop())
        val application = Application.getApplication();
        application.requestUserAttention(true);

        val time = new Timer(2000, new ActionListener() {
          @Override
          def actionPerformed(e: ActionEvent) {
            application.requestUserAttention(true);
           if( !rootPanel.getScene.getWindow.isFocused){
              showNoDefaultVnc
            }
          }
        });
        time.setRepeats(true);
        time.setCoalesce(true);
        time.start();
        bouncingTimer = Some(time)

      }
    })
  }

  def stopBouncingIcon: Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        //  loginPanel.setVisible(false)

        bouncingTimer.map(t => t.stop())

      }
    })
  }

  def refreshClasses(classes: List[String]): Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        var counter = 0
        classesPane.getChildren.clear()
        classes.map(c => {
          val tLabel = if(counter % 2 == 0) {
            classTemplateEvenLabel
          } else {
            classTemplateOddLabel
          }
          val tPane = if(counter % 2 == 0) {
            classTemplateEvenPane
          } else {
            classTemplateOddPane
          }

          val nLabel = new Label()
          nLabel.setFont(tLabel.getFont)
          nLabel.setTextFill(tLabel.getTextFill)
          nLabel.setText(c)
          nLabel.setLayoutX(tLabel.getLayoutX)
          nLabel.setLayoutY(tLabel.getLayoutY)

          val nPane = new Pane()
          nPane.getChildren.add(nLabel)
          nPane.setPrefHeight(tPane.getPrefHeight)
          nPane.setPrefWidth(tPane.getPrefWidth)
          nPane.setStyle(tPane.getStyle)

          classesPane.getChildren.add(nPane)

          counter += 1
        })
      }
    })
  }


  def refreshLocalization(loc: Localization): Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        applicationTitleLabel.setText(loc << "applicationTitle")
        disconnectedLabel.setText(loc << "disconnected")
        loadingAppLabel.setText(loc << "applicationLoading")
        ngrokServiceLabel.setText(loc << "ngrokService")
        noVncServiceLabel.setText(loc << "noVncService")
        vncPasswordLabel.setText(loc << "vncPassword")
        changeVNCPasswordButton.setText(loc << "change")
        noExtensionLabel.setText(loc << "noExtension")
        noKeyLabel.setText(loc << "noKey")
        noDefaultVncLabel.setText(loc << "noDefaultVnc")
        tutorialNextButton.setText(loc << "close")
        tutorialGoogleChromeLabel.setText(loc << "googleChrome")
        tutorialGoogleExtensionLabel.setText(loc << "chromeExtension")
        tutorialRemoteVncLabel.setText(loc << "remoteVnc")
        tutorialGoogleChromeDownloadHyperlink.setText(loc << "more")
        tutorialGoogleExtensionInstallHyperlink.setText(loc << "more")
        tutorialRemoteVncHyperlink.setText(loc << "more")
        tutorialDontAskAgain.setText(loc << "dontShowAgain")
      }
    })
  }



  def setNoVNCStatus(status: String, color: String): Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        noVncServiceStateLabel.setText(status)
        noVncServiceStateLabel.setTextFill(Color.web(color))
      }
    })
  }

  def setNgrokStatus(status: String, color: String): Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        ngrokServiceStateLabel.setText(status)
        ngrokServiceStateLabel.setTextFill(Color.web(color))
      }
    })
  }

  def setVNCDefaultPassword(pass: String): Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        vncPasswordValueTextField.setText(pass)
      }
    })
  }


  def setVNCDefaultPort(port: String): Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        vncPortTextField.setText(port)
      }
    })
  }

  def setDefaultLocale(string: String): Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        localizationComboBox.getSelectionModel.select(string)
      }
    })
  }

  def setSupportedLocales(locales: List[String]): Unit = {
    Platform.runLater(new Runnable {
      override def run(): Unit = {
        localizationComboBox.getItems.addAll(locales.asJava)
      }
    })
  }

  def refreshBackground: Unit = {
    if(noExtensionPanel.isVisible || noKeyPanel.isVisible || disconnectedPanel.isVisible) {
      rootPanel.setStyle(errorPaneStyleHolder.getStyle)
    } else {
      rootPanel.setStyle(normalPaneStyleHolder.getStyle)
    }
  }
}
