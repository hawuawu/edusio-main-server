package com.edusio.virtualclassconnector.client

import java.awt.Image
import java.io.File
import java.net.URLDecoder
import java.security.KeyStore
import javafx.application.HostServices
import javafx.embed.swing.JFXPanel
import javafx.event.EventHandler
import javafx.stage.{WindowEvent, Stage}
import javax.swing.ImageIcon

import akka.actor.{Props, ActorSystem}
import com.edusio.virtualclassconnector.client.controllers._
import org.eclipse.jetty.util.resource.Resource
import org.hawu.utils.{KillProcess, ProcessKiller, GetFreeSocketInRangeTrait}

import scalafx.application.Platform

/**
 * Created by arnostkucha
 *
 *
 *
 *
 * r on 11.12.14.
 */

object HostServices {
  private var hs: HostServices = _
  def apply(hs: HostServices) = {
    this.hs = hs
  }
  def apply(): HostServices = {
    hs
  }
}

object EntryPoint extends javafx.application.Application  with GetFreeSocketInRangeTrait {

  def apply(as: ActorSystem) = {
    new JFXPanel()
    this.init();

    HostServices(getHostServices)

    val rHost = as.settings.config.getString("virtual-class.remoteHost")
    val rPort = as.settings.config.getInt("virtual-class.remotePort")

    val actualPath = URLDecoder.decode( new File(this.getClass.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile.getPath, "UTF-8")
    val keyStoreResource = Resource.newResource(actualPath + File.separator + "keystore.jks");

    val processKiller = as.actorOf(Props[ProcessKiller])

    val wsc = as.actorOf(Props(classOf[WebSocketController], rHost, rPort, true,  keyStoreResource))

    val port = freePortInRange(50001 to 50005)
    val wsec = as.actorOf(Props(classOf[ExtensionController], "127.0.0.1", port.get))

    val lc = as.actorOf(Props(classOf[LoginController], wsc, wsec))
    val sc = as.actorOf(Props(classOf[AppStorageController], "client.properties"))
    val locc = as.actorOf(Props(classOf[LocalizationController]))
    val serviceController = as.actorOf(Props(classOf[ServiceControllerFactory], processKiller, lc))

    var appController = as.actorOf(Props(classOf[ApplicationController], wsc, wsec, lc, sc, locc, serviceController))

  }

  override def start(p1: Stage): Unit = {
  }
}
