package com.edusio.virtualclassconnector.client

import java.io.IOException
import java.net.ServerSocket
import java.util.logging.Level
import javafx.embed.swing.JFXPanel
import javafx.fxml.FXMLLoader
import javafx.scene.{Scene, Parent}
import javafx.stage.{StageStyle, Stage}

import akka.actor.{Props, ActorSystem}
import com.edusio.virtualclassconnector.client.controllers._
import com.edusio.virtualclassconnector.client.helpers.StringEncryption

import akka.pattern.ask
import akka.util.Timeout
import io.backchat.hookup.HookupServer
import org.hawu.utils.GetFreeSocketInRangeTrait

import scala.compat.Platform
import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by arnostkuchar on 10.12.14.
 */
object Application extends App with StringEncryption {

  var stage: Option[Stage] = None

  override def main(args: Array[String]): Unit = {


    val as = ActorSystem("VirtualClassConnector-client")

    try {
        EntryPoint(as)
    } catch {
      case t: Throwable =>
        println("Error " + t)
    }

    as.awaitTermination()
    Runtime.getRuntime().halt(0);

    //readLine()
   // as.shutdown()
  }
}

