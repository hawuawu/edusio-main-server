package com.edusio.virtualclassconnector.client.controllers

import java.io._
import java.nio.charset.Charset
import java.nio.file.{Paths, Path, Files}

import akka.actor._
import org.hawu.utils.{TargetProcess, KillProcess}
import scala.concurrent.duration._
import scala.collection.JavaConverters._

/**
 * Created by arnostkuchar on 12.12.14.
 */

case class StartNoVNCService(vncPort: Int, port: Int)

case class StartNgrokService(domain: String, token: String, port: Int)

case object StopAllServices

case class NoVNCServiceStatusChange(status: Boolean)

case class NgrokServiceStatusChange(status: Boolean)

object ServiceEvents extends Enumeration {
  val STARTED_NO_VNC_SERVICE, STOPPED_NO_VNC_SERVICE, STARTED_NGROK_SERVICE, STOPPED_NGROK_SERVICE = Value
}

case class ReceivedServiceEvent(event: ServiceEvents.Value, param: Any)

case class RegisterServiceListener(event: ServiceEvents.Value)

case class RemoveServiceListener(event: ServiceEvents.Value)


class ServiceControllerFactory(killer: ActorRef, loginController: ActorRef) extends Actor with ActorLogging {

  private var serviceController: Option[ActorRef] = None
  loginController ! RegisterLoginListener(AuthorizationEvent.LOG_OUTED)

  def receive = {
    case ReceivedAuthorizationEvent(AuthorizationEvent.LOG_OUTED, data) =>
      serviceController.map(sc => sc ! StopAllServices)
      Thread.sleep(3000)
      serviceController = None

    case t => forward(t)
  }

  def forward(msg: Any) = {
    serviceController
      .map(sc => sc)
      .getOrElse({
      serviceController = Some(context.actorOf(Props(classOf[ServiceController], killer)))
      serviceController.get
    }).tell(msg, sender)
  }
}

class ServiceController(killer: ActorRef) extends Actor with ActorLogging {

  private val ngrokFilePath = getClass().getClassLoader().getResource("ngrok/ngrok").getPath
  private val ngrokPidFilePath = getClass().getClassLoader().getResource("ngrok").getPath + File.separator + "ngrok.pid"

  if (new File(ngrokPidFilePath).exists()) {
    Files.readAllLines(Paths.get(ngrokPidFilePath), Charset.forName("UTF-8")).asScala.toList.map(f => {
      killer ! KillProcess(Some(Integer.parseInt(f.trim)))
    })
  }
  new File(ngrokPidFilePath).delete()
  private val ngrokPath = getClass().getClassLoader().getResource("ngrok/ngrok.sh").getPath

  private val noVncFilePath = getClass().getClassLoader().getResource("novnc/utils/websockify").getPath
  private val noVncPidFilePath = getClass().getClassLoader().getResource("novnc").getPath + File.separator + "novnc.pid"
  if (new File(noVncPidFilePath).exists()) {
    Files.readAllLines(Paths.get(noVncPidFilePath), Charset.forName("UTF-8")).asScala.toList.map(f => {
      killer ! KillProcess(Some(Integer.parseInt(f.trim)))
    })
  }
  new File(noVncPidFilePath).delete()
  private val noVncPath = getClass().getClassLoader().getResource("novnc/utils/launch.sh").getPath

  val rt = Runtime.getRuntime
  rt.exec("chmod +x " + ngrokPath)
  rt.exec("chmod +x " + ngrokFilePath)

  rt.exec("chmod +x " + noVncPath)
  rt.exec("chmod +x " + noVncFilePath)

  var listeners: List[Pair[ActorRef, ServiceEvents.Value]] = List()

  var noVNCProcess: Option[java.lang.Process] = None
  var noVncStartSetup: Option[StartNoVNCService] = None

  var NgrokProcess: Option[java.lang.Process] = None
  var ngrokStartSetup: Option[StartNgrokService] = None

  killer ! KillProcess(None, Some("ngrok"))
  killer ! KillProcess(None, Some("vnc"))

  def receive = {
    case StopAllServices =>
      killer ! KillProcess(None, Some("vnc"))
      killer ! KillProcess(None, Some("ngrok"))
      noVNCProcess.map(p => p.destroy())
      NgrokProcess.map(p => p.destroy())
      context.stop(self)

    case msg: RegisterServiceListener => {
      listeners = sender -> msg.event :: listeners
    }

    case msg: RemoveServiceListener => {
      listeners = listeners.filterNot(l => l._1 == sender && l._2 == msg.event)
    }


    case StartNoVNCService(vnc, ngrok) =>
      noVncStartSetup = Some(StartNoVNCService(vnc, ngrok))
      val command = noVncPath + " --vnc localhost:" + vnc + " --listen " + ngrok
      log.info("Running command: " + "novnc/utils/launch.sh" + " --vnc localhost:" + vnc + " --listen " + ngrok)
      val p = rt.exec(command)
      val f = p.getClass().getDeclaredField("pid");
      f.setAccessible(true);
      val pid = f.getInt(p)
      killer ! TargetProcess(pid, "vnc")
      noVNCProcess = Some(p)
      context.actorOf(Props(classOf[PidFileHook], self, noVncPidFilePath, 20))


    case StartNgrokService(domain, token, port) =>
      ngrokStartSetup = Some(StartNgrokService(domain, token, port))
      val command = ngrokPath + " " + domain + " " + port
      val p = rt.exec(command)
      log.info("Running command: " + command)
      val f = p.getClass().getDeclaredField("pid");
      f.setAccessible(true)
      val pid = f.getInt(p)
      killer ! TargetProcess(pid, "ngrok")
      NgrokProcess = Some(p)
      context.actorOf(Props(classOf[PidFileHook], self, ngrokPidFilePath, 20))


    case msg: PidFileRead =>
      msg.path match {
        case `noVncPidFilePath` =>
          log.info("Started novnc service")
          msg.files.map(f => {
            killer ! TargetProcess(Integer.parseInt(f.trim), "vnc")
          })

          listeners.filter(f => f._2 == ServiceEvents.STARTED_NO_VNC_SERVICE).map(l => {
            noVncStartSetup.map(s => {
              l._1 ! ReceivedServiceEvent(ServiceEvents.STARTED_NO_VNC_SERVICE, s)
            })
          })

          listeners.filter(f => f._2 == ServiceEvents.STOPPED_NO_VNC_SERVICE).map(l => {
            noVNCProcess.map(p => {
              context.actorOf(Props(classOf[ServiceHook], l._1, self, ServiceEvents.STOPPED_NO_VNC_SERVICE)) ! WaitForProcess(p)
            })
          })

        case `ngrokPidFilePath` =>
          msg.files.map(f => {
            killer ! TargetProcess(Integer.parseInt(f.trim), "ngrok")
          })


          log.info("Started ngrok service")

          listeners.filter(f => f._2 == ServiceEvents.STARTED_NGROK_SERVICE).map(l => {
            ngrokStartSetup.map(s => {
              l._1 ! ReceivedServiceEvent(ServiceEvents.STARTED_NGROK_SERVICE, s)
            })
          })

          listeners.filter(f => f._2 == ServiceEvents.STOPPED_NGROK_SERVICE).map(l => {
            NgrokProcess.map(p => {
              context.actorOf(Props(classOf[ServiceHook], l._1, self, ServiceEvents.STOPPED_NGROK_SERVICE)) ! WaitForProcess(p)
            })
          })
      }

    case CloseApplicationInteraction =>
      val hook = {
        if (noVNCProcess.isDefined && NgrokProcess.isDefined) {
          context.actorOf(Props(classOf[ProcessKillHook], 2))
        } else if (noVNCProcess.isDefined || NgrokProcess.isDefined) {
          context.actorOf(Props(classOf[ProcessKillHook], 1))
        } else {
          context.actorOf(Props(classOf[ProcessKillHook], 0))
        }
      }

      noVNCProcess.map(k => {
        killer.tell(KillProcess(None, Some("vnc")), hook)
        k.destroy()
        k.waitFor()
      })

      noVNCProcess = None

      NgrokProcess.map(k => {
        killer.tell(KillProcess(None, Some("ngrok")), hook)
        k.destroy()
        k.waitFor()
      })

      NgrokProcess = None
  }

  override def postStop: Unit = {
  }
}

case class WaitForProcess(process: java.lang.Process)

class ServiceHook(sendTo: ActorRef, parent: ActorRef, event: ServiceEvents.Value) extends Actor with ActorLogging {
  def receive = {
    case p: WaitForProcess =>
      p.process.waitFor()
      log.info("Service hook exited with exit status: " + p.process.exitValue.toString)
      sendTo.tell(ReceivedServiceEvent(event, Nil), parent)
      context.stop(self)
  }
}


case class PidFileRead(path: String, files: List[String])

class PidFileHook(parent: ActorRef, file: String, numberOfTries: Int) extends Actor {
  private var actualTry = 0
  checkFile

  def receive = {
    case akka.actor.ReceiveTimeout =>
      checkFile
  }

  def checkFile = {
    if (actualTry + 1 > numberOfTries) throw new FileNotFoundException("Cannot find " + file)
    actualTry += 1
    if (new File(file).exists()) {
      parent ! PidFileRead(file, Files.readAllLines(Paths.get(file), Charset.forName("UTF-8")).asScala.toList)
      context.stop(self)
    } else {
      context.setReceiveTimeout(1000 millis)
    }
  }
}


class ProcessKillHook(requests: Int) extends Actor {
  var actual = requests
  check

  def receive = {
    case t =>
      actual -= 1
      check
  }

  def check = if (actual == 0) {
    context.system.shutdown()
  }

}
