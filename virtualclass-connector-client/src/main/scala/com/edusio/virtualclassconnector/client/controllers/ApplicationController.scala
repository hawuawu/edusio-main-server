package com.edusio.virtualclassconnector.client.controllers

import java.io.IOException
import javafx.fxml.FXMLLoader
import javafx.scene.{Parent, Scene}
import javafx.stage.{StageStyle, Stage}

import akka.actor.{PoisonPill, ActorRef, ActorLogging, Actor}
import com.edusio.virtualclassconnector.client.controllers
import com.edusio.virtualclassconnector.client.formcontrollers.Form
import com.edusio.virtualclassconnector.messages.{ServicesStatus, Login, LoginResult, VirtualClassesUpdate}
import org.hawu.utils.GetFreeSocketInRangeTrait

import scalafx.scene.image.Image

/**
 * Created by arnostkuchar on 11.12.14.
 */

case object CloseApplicationInteraction

case class ChangeLocaleInteraction(locale: String)

case class LoginInteraction(userName: String, password: String)

case class ChangeDefaultVNCInteraction(vncPassword: String, vncPort: Int)

case object DontShowAgainInteraction

class ApplicationController(
                              wsController: ActorRef,
                              wsExtensionController: ActorRef,
                              loginController: ActorRef,
                              applicationStorageController: ActorRef,
                              localizationController: ActorRef,
                              servicesController: ActorRef
                             ) extends Actor with ActorLogging with GetFreeSocketInRangeTrait {

  var form: Option[Form] = None
  var localization: Option[Localization] = None
  var actualConfiguration: Option[AppProperties] = None

  var ngrokService = false
  var noVNCService = false
  var vncService = false

  var actualNgrokPort = 0
  var ngrokDomain = ""
  var ngrokToken = ""
  var stopping = false

  applicationStorageController ! GetProperties

  wsController ! RegisterConnectionListener(ConnectionEvent.CANNOT_CONNECT)
  wsController ! RegisterConnectionListener(ConnectionEvent.DISCONNECTED)
  wsController ! RegisterConnectionListener(ConnectionEvent.CONNECTED)
  wsController ! RegisterConnectionListener(ConnectionEvent.MESSAGE_RECEIVED)

  loginController ! RegisterLoginListener(AuthorizationEvent.LOGIN_SUCCESSFUL)
  loginController ! RegisterLoginListener(AuthorizationEvent.LOG_OUTED)

  wsExtensionController ! RegisterExtensionConnectionListener(ExtensionEvents.CONNECTED)
  wsExtensionController ! RegisterExtensionConnectionListener(ExtensionEvents.DISCONNECTED)

  def receive = {
    case CloseApplicationInteraction => {
      //context.system.shutdown()
      stopping = true
      servicesController ! CloseApplicationInteraction
     // servicesController ! PoisonPill
    }

    case DontShowAgainInteraction => {
      applicationStorageController ! StoreProperties(AppProperties(autoLogin = Some(actualConfiguration.get.autoLogin.map(al => if(al){false}else{true}).getOrElse(true))))
    }

    case props: AppProperties => {
      actualConfiguration = Some(props)
      log.info(actualConfiguration.toString)
      localizationController ! GetLocalization(props.defaultLocale.map(loc => loc).getOrElse("en"))
    }

    case msg: Localization => {
      localization = Some(msg)
      if (form.isEmpty) {
        showForm
      } else {
        form.map(f => {
          applicationStorageController ! StoreProperties(AppProperties(defaultLocale = actualConfiguration.get.defaultLocale))
          f.refreshLocalization(msg)
          if(ngrokService) {
            f.setNgrokStatus(msg << "on", "#2ECC71")
          } else {
            f.setNgrokStatus(msg << "off", "#C0392B")
          }

          if(noVNCService) {
            f.setNoVNCStatus(msg << "on", "#2ECC71")
          } else {
            f.setNoVNCStatus(msg << "off", "#C0392B")
          }
        })
      }
    }

    case CannotFindLocalization => {
      if (form.isEmpty) {
        showForm
       }
    }

    case ChangeLocaleInteraction(loc) => {
      actualConfiguration = Some(actualConfiguration.get.copy(defaultLocale = Some(loc)))
      localizationController ! GetLocalization(loc)
    }

    case ChangeDefaultVNCInteraction(vncPassword, port) => {
      form.map(f => f.stopBouncingIcon)
      if (vncPassword != "") {
        applicationStorageController ! StoreUserDefinedVNC(vncPassword, port)
        vncService = true
      } else {
        applicationStorageController ! RemoveUserDefinedVNCPassword
        vncService = false
      }
    }

    case StoredDefinedVNC(pass, port) => {
      if(pass.isDefined) {
        actualConfiguration = Some(actualConfiguration.get.copy(vncPassword = pass, vncPort = Some(port)))

        freePortInRange(52000 to 52500).map(nport => {
          servicesController ! RegisterServiceListener(ServiceEvents.STARTED_NO_VNC_SERVICE)
          servicesController ! RegisterServiceListener(ServiceEvents.STARTED_NGROK_SERVICE)
          servicesController ! RegisterServiceListener(ServiceEvents.STOPPED_NO_VNC_SERVICE)
          servicesController ! RegisterServiceListener(ServiceEvents.STOPPED_NGROK_SERVICE)
          
          servicesController ! StartNgrokService(ngrokDomain, ngrokToken, nport)
          servicesController ! StartNoVNCService(actualConfiguration.get.vncPort.get, nport)
        })
        form.map(f => {
          f.hideNoDefaultVnc
          f.enableContent
        })
      }
    }

    case ReceivedExtensionConnectionEvent(event, param) => {
      event match {
        case  ExtensionEvents.CONNECTED => form.map(f => f.hideNoExtension)
        case  ExtensionEvents.DISCONNECTED => form.map(f => f.showNoExtension)
      }
    }

    case ReceivedConnectionEvent(event, param) =>

      event match {
        case ConnectionEvent.CANNOT_CONNECT => form.map(f => f.showDisconnected)
        case ConnectionEvent.DISCONNECTED => form.map(f => {
          f.showDisconnected
          f.disableContent
        })
        case ConnectionEvent.CONNECTED => form.map(f => f.hideDisconnected)
        case ConnectionEvent.MESSAGE_RECEIVED =>
          param match {
            case msg: VirtualClassesUpdate =>
              form.map(f => f.refreshClasses(msg.classes))
            case msg: LoginResult => form.map(f => f.refreshClasses(msg.virtualClasses))
            case t =>
          }
      }

    case ReceivedAuthorizationEvent(event, param) =>
      log.info("Received authorization event")

      event match {
        case AuthorizationEvent.LOGIN_FAILED =>

        case AuthorizationEvent.LOGIN_SUCCESSFUL =>
          form.map(f => f.hideNoLogin)
          param match {
            case o: Option[LoginResult] =>
              o.map(r => {
                ngrokDomain = r.ngrokDomain
                ngrokToken = r.ngrokToken
                if(actualConfiguration.get.vncPort.isDefined && actualConfiguration.get.vncPassword.isDefined) {
                  form.map(f => f.setVNCDefaultPassword(actualConfiguration.get.vncPassword.get))
                  form.map(f => f.setVNCDefaultPort(actualConfiguration.get.vncPort.get.toString))

                  servicesController ! RegisterServiceListener(ServiceEvents.STARTED_NO_VNC_SERVICE)
                  servicesController ! RegisterServiceListener(ServiceEvents.STARTED_NGROK_SERVICE)
                  servicesController ! RegisterServiceListener(ServiceEvents.STOPPED_NO_VNC_SERVICE)
                  servicesController ! RegisterServiceListener(ServiceEvents.STOPPED_NGROK_SERVICE)

                  freePortInRange(52000 to 52500).map(port => {
                    servicesController ! StartNgrokService(r.ngrokDomain, r.ngrokToken, port)
                    servicesController ! StartNoVNCService(actualConfiguration.get.vncPort.get, port)
                  })
                  form.map(f => {
                    f.hideNoDefaultVnc
                    f.enableContent
                    f.hideLoading
                  })
                } else {
                  form.map(f => {
                    f.startBouncingIcon
                    f.enableContent
                    f.hideLoading
                  })
                }
              })
            case t=>
          }

        case AuthorizationEvent.LOG_OUTED =>
          form.map(f => f.showNoLogin)
      }

    case ReceivedServiceEvent(event, param) => {
      param match {
        case p: StartNgrokService =>
          actualNgrokPort = p.port
        case t =>
      }

      event match {
        case ServiceEvents.STARTED_NO_VNC_SERVICE =>
          noVNCService = true
          form.map(f => f.setNoVNCStatus(localization.get <<("on"), "#2ECC71"))
        case ServiceEvents.STARTED_NGROK_SERVICE =>
          ngrokService = true
          log.info("Setting on to ngrok service")
          form.map(f => f.setNgrokStatus(localization.get <<("on"), "#2ECC71"))
        case ServiceEvents.STOPPED_NO_VNC_SERVICE =>
          noVNCService = false
          form.map(f => f.setNoVNCStatus(localization.get <<("off"), "#C0392B"))
        case ServiceEvents.STOPPED_NGROK_SERVICE =>
          ngrokService = false
          log.info("Setting off to ngrok service")
          form.map(f => f.setNgrokStatus(localization.get <<("off"), "#C0392B"))
      }

      if(ngrokService && noVNCService) {
        wsController ! SendMessage(ServicesStatus(true, actualNgrokPort, true, true, actualConfiguration.get.vncPort.get, actualConfiguration.get.vncPassword.get))
      } else if (ngrokService) {
        wsController ! SendMessage(ServicesStatus(true, actualNgrokPort, false, true, actualConfiguration.get.vncPort.get, actualConfiguration.get.vncPassword.get))
      } else if (noVNCService) {
        wsController ! SendMessage(ServicesStatus(false, actualNgrokPort, true, true, actualConfiguration.get.vncPort.get, actualConfiguration.get.vncPassword.get))
      } else {
        wsController ! SendMessage(ServicesStatus(false, actualNgrokPort, false, true, actualConfiguration.get.vncPort.get, actualConfiguration.get.vncPassword.get))
      }



      if(stopping && !ngrokService && !noVNCService) {
      //  context.system.shutdown()
      }
    }
  }

  def showForm = {
    javafx.application.Platform.runLater(new Runnable {
      override def run(): Unit = {
        println(getClass.getResource("."))
        val resource = getClass().getClassLoader().getResource("assets/form.fxml")
        if (resource == null) {
          throw new IOException("Cannot load form.xml")
        }

        val loader = new FXMLLoader(resource)
        val pane = loader.load().asInstanceOf[Parent]
        form = Some(loader.getController[Form])

        form.map(f => {
          val s = new Stage()
          s.setScene(new Scene(pane))
          s.initStyle(StageStyle.UNDECORATED)
          s.show()
          f.postInitialize(self)
          f.disableContent

          actualConfiguration.map(c => {
            f.setSupportedLocales(c.supportedLocales)
            f.setDefaultLocale(c.defaultLocale.map(loc => loc).getOrElse("en"))
            localization.map(loc => {
              f.refreshLocalization(loc)

              if(ngrokService) {
                f.setNgrokStatus(loc << "on", "#2ECC71")
              } else {
                f.setNgrokStatus(loc << "off", "#C0392B")
              }

              if(noVNCService) {
                f.setNoVNCStatus(loc << "on", "#2ECC71")
              } else {
                f.setNoVNCStatus(loc << "off", "#C0392B")
              }

            })
          })

         if(actualConfiguration.get.autoLogin.exists(e => e)) {
          f.hideTutorial
         } else {
          f.showTutorial
         }

          f.showDisconnected
          f.showNoLogin
          f.showNoExtension

          wsExtensionController ! ExtensionConnect
          wsController ! Connect
        })
      }
    })
  }
}
