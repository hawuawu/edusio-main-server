package com.edusio.virtualclassconnector.client.controllers

import akka.actor.{ActorRef, FSM, Actor, ActorLogging}
import com.edusio.virtualclassconnector.client.controllers
import com.edusio.virtualclassconnector.messages.{Token, Login, LoginResult}

/**
 * Created by arnostkuchar on 10.12.14.
 */



case class UserWithToken(user: String, token: String)
case class AuthorizationData(connected: Boolean, queriedTokens: List[UserWithToken], authorizationHandlers: List[RegisterLoginHandler], authorizationListeners: List[Pair[ActorRef, AuthorizationEvent.Value]], authorizationData: Option[LoginResult] = None)

trait AuthorizationState

object AUTHORIZED extends AuthorizationState

object NOT_AUTHORIZED extends AuthorizationState

object AuthorizationEvent extends Enumeration {
  val LOGIN_SUCCESSFUL, LOGIN_FAILED, LOGIN_FAILED_CONNECTION, LOG_OUTED = Value
}

case class RegisterLoginHandler(sender: Any, handler: (Any) => Unit, event: AuthorizationEvent.Value)

case class DeRegisterLoginHandler(sender: Any, event: AuthorizationEvent.Value)

case class RegisterLoginListener(event: AuthorizationEvent.Value)

case class DeRegisterLoginListener(event: AuthorizationEvent.Value)

case class ReceivedAuthorizationEvent(event: AuthorizationEvent.Value, param: Any)

class LoginController(webSocketController: ActorRef, webSocketExtensionController: ActorRef) extends FSM[AuthorizationState, AuthorizationData] with ActorLogging {

  webSocketController ! RegisterConnectionListener(ConnectionEvent.CONNECTED)
  webSocketController ! RegisterConnectionListener(ConnectionEvent.DISCONNECTED)
  webSocketController ! RegisterConnectionListener(ConnectionEvent.MESSAGE_RECEIVED)

  webSocketExtensionController ! RegisterExtensionConnectionListener(ExtensionEvents.CONNECTED)
  webSocketExtensionController ! RegisterExtensionConnectionListener(ExtensionEvents.DISCONNECTED)
  webSocketExtensionController ! RegisterExtensionConnectionListener(ExtensionEvents.RECEIVED_KEYS)

  startWith(NOT_AUTHORIZED, AuthorizationData(false, List(), List(), List()))

  when(NOT_AUTHORIZED) {
    case Event(ReceivedExtensionConnectionEvent(event, param), data) => {
      log.debug("Received extension event " + event.toString + " with parameter " + param.toString)
      param match {
        case p: Token =>
          val uwt = (
            for( u <- 0 until p.user.size;  t <- 0 until p.token.size;
                 if!data.queriedTokens.exists(e => e.user == p.user(u))
            ) yield {
              if (data.connected) {
                log.debug("Connector is connected, sending login event.")
                webSocketController ! SendMessage(Login(p.user(u), p.token(t), ""))
              }
              UserWithToken(
                p.user(u),
                p.token(t)
              )
            }
            ).toList
          stay using data.copy(queriedTokens = uwt ::: data.queriedTokens)
        case o =>
          log.warning("Received unknown extension event " + o.toString)
          stay()
      }
    }

    case Event(ReceivedConnectionEvent(event, parameter), data) => {
      event match {
        case ConnectionEvent.CONNECTED =>
          //webSocketController ! SendMessage(Login(data.userName, data.password, data.salt))
          data.queriedTokens.map(qt => {
            webSocketController ! SendMessage(Login(qt.user, qt.token, ""))
          })
          stay using data.copy(connected = true)

        case ConnectionEvent.DISCONNECTED =>
          stay using data.copy(connected = false)

        case ConnectionEvent.MESSAGE_RECEIVED =>
          parameter match {
            case msg: LoginResult =>
              log.info("Received login result: " + msg)
              if (msg.logged) {
                goto(AUTHORIZED) using data.copy(authorizationData = Some(msg))
              } else {
                stay using data.copy(queriedTokens = data.queriedTokens.filterNot(t => t.user == msg.user))
              }
            case t =>
              stay()
          }
      }
    }
  }

  when(AUTHORIZED) {
    case Event(ReceivedConnectionEvent(event, parameter), data) => {
      event match {
        case ConnectionEvent.CONNECTED =>
          stay using data.copy(connected = true)

        case ConnectionEvent.DISCONNECTED =>
          goto(NOT_AUTHORIZED) using data.copy(connected = false)

        case ConnectionEvent.MESSAGE_RECEIVED =>
          parameter match {
            case msg: LoginResult =>
              if(data.queriedTokens.size > 1) {
                stay using data.copy(queriedTokens = data.queriedTokens.filterNot(f => f.user == msg.user))
              } else {
                if(data.queriedTokens.filterNot(f => f.user == msg.user).size == 0) {
                  goto(NOT_AUTHORIZED) using data.copy(queriedTokens = List())
                } else {
                  stay()
                }
              }
            case t =>
              stay()
          }
      }
    }
  }

  whenUnhandled {
    case Event(msg: RegisterLoginHandler, data) => {
      stay using data.copy(authorizationHandlers = msg :: data.authorizationHandlers)
    }

    case Event(DeRegisterLoginHandler(sender, event), data) => {
      stay using data.copy(authorizationHandlers = data.authorizationHandlers.filter(f => f.event != event && f.sender != sender))
    }

    case Event(RegisterLoginListener(event), data) => {
      stay using data.copy(authorizationListeners = sender -> event :: data.authorizationListeners)
    }

    case Event(DeRegisterLoginListener(event), data) => {
      stay using data.copy(authorizationListeners = data.authorizationListeners.filter(f => f._2 != event && f._1 != sender))
    }
  }

  onTransition {
    case NOT_AUTHORIZED -> AUTHORIZED =>
      stateData.authorizationHandlers.filter(f => f.event == AuthorizationEvent.LOGIN_SUCCESSFUL).map(m => m.handler())
      stateData.authorizationListeners.filter(f => f._2 == AuthorizationEvent.LOGIN_SUCCESSFUL).map(m => m._1 ! ReceivedAuthorizationEvent(AuthorizationEvent.LOGIN_SUCCESSFUL, nextStateData.authorizationData))

    case AUTHORIZED -> NOT_AUTHORIZED =>
      stateData.authorizationHandlers.filter(f => f.event == AuthorizationEvent.LOG_OUTED).map(m => m.handler())
      stateData.authorizationListeners.filter(f => f._2 == AuthorizationEvent.LOG_OUTED).map(m => m._1 ! ReceivedAuthorizationEvent(AuthorizationEvent.LOG_OUTED, None))
  }

}
