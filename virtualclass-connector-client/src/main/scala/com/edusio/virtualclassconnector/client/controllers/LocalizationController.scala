package com.edusio.virtualclassconnector.client.controllers

import java.io.InputStreamReader
import java.util.Properties

import akka.actor.{ActorLogging, Actor}
import org.apache.commons.io.Charsets

import scala.collection.immutable.HashMap
import scala.collection.JavaConverters._

/**
 * Created by arnostkuchar on 11.12.14.
 */

case class GetLocalization(locale: String)
case class Localization(values: List[Pair[String, String]]) {
  def <<(id: String): String = {
    values.find(v => v._1 == id).map(v => v._2).getOrElse(id)
  }
}
case object CannotFindLocalization

class LocalizationController extends Actor with ActorLogging {
  def receive = {
    case GetLocalization(locale) => {
      try {
        val prop = new Properties()
        val inputStream = getClass().getClassLoader().getResourceAsStream(locale + ".properties");

        if (inputStream != null) {
          prop.load(new InputStreamReader(inputStream, Charsets.UTF_8))
          sender ! Localization(prop.propertyNames.asScala.map(name => {
            name.asInstanceOf[String] -> prop.getProperty(name.asInstanceOf[String])
          }).toList)
        } else {
          log.error("Cannot find props file: " + locale + ".properties")
          sender ! CannotFindLocalization
        }
      } catch {
        case t: Throwable =>
          log.error("Exception while consumimng props: " + locale + ".properties | " + t.toString)
          sender ! CannotFindLocalization
      }
    }
  }
}
