package com.edusio.virtualclassconnector.client.controllers

import java.io.{File, FileOutputStream, InputStream}
import java.util.Properties

import akka.actor.{ActorLogging, Actor}
import com.edusio.virtualclassconnector.client.helpers.StringEncryption

import scala.util.Random

/**
 * Created by arnostkuchar on 11.12.14.
 */


case class StoreCredentials(userName: String, password: String)

case object GetStoredCredentials

case class StoredCredentials(userName: String, password: String)

case class AppProperties(autoLogin: Option[Boolean] = None, vncPort: Option[Int] = None, vncPassword: Option[String] = None, ngrokDefaultPort: Option[Int] = None, defaultLocale: Option[String] = None, supportedLocales: List[String] = List())

case object GetProperties

case class StoreProperties(properties: AppProperties)

case class StoreUserDefinedVNC(vncPassword: String, port: Int)

case object RemoveUserDefinedVNCPassword

case object GetStoredUserDefinedVNCPassword

case class StoredDefinedVNC(vncPassword: Option[String], port: Int)

class AppStorageController(propsFile: String) extends Actor with StringEncryption with ActorLogging {

  val pseudoPass = "XYZ192AKL"

  def receive = {
    case StoreCredentials(userName, password) =>
      try {
        val salt = Random.shuffle(Random.alphanumeric.take(8) ++ userName.getBytes).mkString
        val splitted = userName.splitAt(userName.size / 2)
        val phrase = splitted._1 + pseudoPass + splitted._2

        val encrypted = encryptString(password, phrase, salt)

        val prop = new Properties()
        val inputStream = getClass().getClassLoader().getResourceAsStream(propsFile);
        if (inputStream != null) {

          prop.load(inputStream);
          prop.setProperty("userName", userName)
          prop.setProperty("password", encrypted)
          prop.setProperty("password2", salt)

          val f = new File(getClass().getClassLoader().getResource(propsFile).getFile);
          val out = new FileOutputStream(f);
          prop.store(out, "");
        } else {
          log.error("Cannot find props file")
          //  throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }
      } catch {
        case t: Throwable =>
      }
    case GetStoredCredentials =>
      try {
        val prop = new Properties()
        val inputStream = getClass().getClassLoader().getResourceAsStream(propsFile);

        if (inputStream != null) {

          prop.load(inputStream);
          val userName = prop.getProperty("userName")
          val password = prop.getProperty("password")
          val salt = prop.getProperty("password2")

          val splitted = userName.splitAt(userName.size / 2)
          val phrase = splitted._1 + pseudoPass + splitted._2

          val decrypted = decryptString(password, phrase, salt)
          sender ! StoredCredentials(userName, decrypted)
        } else {
          //  throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
          sender ! StoredCredentials("", "")
        }
      } catch {
        case t: Throwable =>
          sender ! StoredCredentials("", "")

      }
    case StoreUserDefinedVNC(password, port) =>
      val salt = Random.shuffle(Random.alphanumeric.take(8)).mkString
      val phrase = pseudoPass

      val encrypted = encryptString(password, phrase, salt)

      val prop = new Properties()
      val inputStream = getClass().getClassLoader().getResourceAsStream(propsFile);
      if (inputStream != null) {

        prop.load(inputStream);
        prop.setProperty("vncPassword", encrypted)
        prop.setProperty("vncPassword2", salt)
        prop.setProperty("defaultVncPort", port.toString)

        val f = new File(getClass().getClassLoader().getResource(propsFile).getFile);
        val out = new FileOutputStream(f);
        prop.store(out, "");
        sender ! StoredDefinedVNC(Some(password), port)
      } else {
        log.error("Cannot find props file")
        //  throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        sender ! StoredDefinedVNC(None, port)
      }

    case RemoveUserDefinedVNCPassword =>

      val prop = new Properties()
      val inputStream = getClass().getClassLoader().getResourceAsStream(propsFile);
      if (inputStream != null) {

        prop.load(inputStream);
        prop.remove("vncPassword")
        prop.remove("vncPassword2")

        val f = new File(getClass().getClassLoader().getResource(propsFile).getFile);
        val out = new FileOutputStream(f);
        prop.store(out, "");
      } else {
        log.error("Cannot find props file")
        //  throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
      }

    case GetStoredUserDefinedVNCPassword =>
      val prop = new Properties()
      val inputStream = getClass().getClassLoader().getResourceAsStream(propsFile);

      if (inputStream != null) {

        prop.load(inputStream);
        val password = prop.getProperty("vncPassword")
        val salt = prop.getProperty("vncPassword2")

        val port = prop.getProperty("defaultVncPort").toInt

        val phrase = pseudoPass
        if (password != null && salt != null) {
          val decrypted = decryptString(password, phrase, salt)
          sender ! StoredDefinedVNC(Some(decrypted), port)
        } else {
          sender ! StoredDefinedVNC(None, 0)
        }
      } else {
        //  throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
      }

    case GetProperties =>
      val prop = new Properties()
      val inputStream = getClass().getClassLoader().getResourceAsStream(propsFile);

      if (inputStream != null) {
        prop.load(inputStream)

        sender ! AppProperties(
          try {
            val p = prop.getProperty("autoLogin").toBoolean
            if (p == null) throw new NullPointerException
            Some(p)
          } catch {
            case t: Throwable => None
          },
          try {
            val p = prop.getProperty("defaultVncPort").toInt
            if (p == null) throw new NullPointerException
            Some(p)
          } catch {
            case t: Throwable => None
          },
          try {
            val password = prop.getProperty("vncPassword")
            val salt = prop.getProperty("vncPassword2")
            if (password == null || salt == null) throw new NullPointerException
            val phrase = pseudoPass
            Some(decryptString(password, phrase, salt))
          } catch {
            case t: Throwable => None
          },
          try {
            val p = prop.getProperty("ngrokDefaultPort").toInt
            if (p == null) throw new NullPointerException
            Some(p)
          } catch {
            case t: Throwable => None
          },
          try {
            val p = prop.getProperty("defaultLocale")
            if (p == null) throw new NullPointerException
            Some(p)
          } catch {
            case t: Throwable => None
          },
          try {
            val p = prop.getProperty("supportedLocales")
            if (p == null) throw new NullPointerException
            p.split(',').toList
          } catch {
            case t: Throwable => List()
          }
        )

      } else {
        //  throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
      }

    case StoreProperties(props) =>
      val prop = new Properties()
      val inputStream = getClass().getClassLoader().getResourceAsStream(propsFile);

      if (inputStream != null) {

        prop.load(inputStream)
        props.autoLogin.map(al => {
          prop.setProperty("autoLogin", al.toString)
        })

        props.ngrokDefaultPort.map(ndp => {
          prop.setProperty("ngrokDefaultPort", ndp.toString)
        })

        props.vncPort.map(vnc => {
          prop.setProperty("defaultVncPort", vnc.toString)
        })

        props.defaultLocale.map(dl => {
          prop.setProperty("defaultLocale", dl.toString)
        })

        val f = new File(getClass().getClassLoader().getResource(propsFile).getFile);
        val out = new FileOutputStream(f)
        prop.store(out, "")
      } else {
        //  throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
      }
  }
}
