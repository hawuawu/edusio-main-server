package com.edusio.virtualclassconnector.client.controllers

import java.net.URI
import java.security.KeyStore

import akka.actor._
import org.eclipse.jetty.util.resource.Resource
import org.hawu.utils.websocket.client._
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

import com.edusio.virtualclassconnector.client.controllers
import com.edusio.virtualclassconnector.messages.{Ack, Ask, JsonDeserializable, JsonSerializable}

import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._

/**
 * Created by arnostkuchar on 10.12.14.
 */


//case class ConnectionData(connectionHandlers: List[RegisterConnectionHandler], connectionListeners: List[Pair[ActorRef, ConnectionEvent.Value]], client: HookupClient)
case class ConnectionData(connectionHandlers: List[RegisterConnectionHandler], connectionListeners: List[Pair[ActorRef, ConnectionEvent.Value]], client: WebSocketClientContext)

trait ConnectionState

object CONNECTED extends ConnectionState

object DISCONNECTED extends ConnectionState

object ConnectionEvent extends Enumeration {
  val CONNECTED, DISCONNECTED, MESSAGE_RECEIVED, CANNOT_CONNECT = Value
}

case class ReceivedConnectionEvent(event: ConnectionEvent.Value, params: Any)

case object Connect

case class SendMessage(message: JsonSerializable)

case class RegisterConnectionHandler(sender: Any, handler: (Any) => Unit, event: ConnectionEvent.Value)

case class DeRegisterConnectionHandler(sender: Any, event: ConnectionEvent.Value)

case class RegisterConnectionListener(event: ConnectionEvent.Value)

case class DeRegisterConnectionListener(event: ConnectionEvent.Value)

class WebSocketController(address: String, port: Int, secure: Boolean,  keyStore: Resource) extends FSM[ConnectionState, ConnectionData] {

  val uri = (if (secure) {
    "wss://"
  } else {
    "ws://"
  }) + address + ":" + port + ""

  println(uri)
  var reconnector: Option[ActorRef] = None
  var pinger: Option[ActorRef] = None
  var connectionTimeout: Option[ActorRef] = None

  //case object ClientConnected

  //case object ClientDisconnected

  case class MessageReceived(json: JsonDeserializable)

  startWith(
    DISCONNECTED,
    ConnectionData(
      List(),
      List(),
      WebSocketClientContextFactory(uri, self,  keyStore)
    )
  )
  /*
  startWith(DISCONNECTED, ConnectionData(
    List(),
    List(),
    new DefaultHookupClient(HookupClientConfig(new URI(uri))) {
      def receive = {

        case Connected =>
          log.info("Client connected in socket client")
          self ! ClientConnected

        case Disconnected(_) ⇒
          self ! ClientDisconnected

        case TextMessage(message) ⇒
          println("aadddww "  + message)

        case m: JsonMessage ⇒
          try {
            implicit lazy val formats = org.json4s.DefaultFormats
            val name = (m.content \ "Name").extract[String]
            val message = parse((m.content \ "Message").extract[String])
            log.info("Accepted" + message.toString)

            val c = Class.forName("com.edusio.virtualclassconnector.messages." + name)
            val from = c.getMethod("fromJson", classOf[JValue])

            val declaredConstructors = c.getDeclaredConstructors

            declaredConstructors.map(c => {
              c.setAccessible(true)
            })

            declaredConstructors.find(c => c.getParameterTypes.length == 0).map(bdinstance => {
              val builder = bdinstance.newInstance()
              val resultMessage = from.invoke(builder, message).asInstanceOf[JsonDeserializable]
              log.info(resultMessage.toString)
              println("sendiiing " + resultMessage)
              self ! MessageReceived(resultMessage)
            })
          } catch {
            case t: Throwable =>
              log.error("Error while parsing message: " + t)
          }
      }
    }
  ))


  */

  when(DISCONNECTED) {
    case Event(Connect, data) => {
      log.debug("Connect message received.")
      reconnector.map(rcn => context.stop(rcn))
      reconnector = None
      import ExecutionContext.Implicits.global
      try {

        data.client.connect()
      } catch {
        case t: Throwable =>
          log.error("Error while connection throught WebSocketController " + t.toString)
      }

      stay()
    }

    case Event(ClientCannotConnect, data) => {
      log.error("Cannot connect.")
      data.connectionHandlers.filter(f => f.event == ConnectionEvent.CANNOT_CONNECT).map(ch => ch.handler(_))
      data.connectionListeners.filter(f => f._2 == ConnectionEvent.CANNOT_CONNECT).map(f => f._1 ! ReceivedConnectionEvent(ConnectionEvent.CANNOT_CONNECT, Nil))
      reconnector = Some(context.actorOf(Props(classOf[ReconnectLaterHandler], Connect, 5000L, self)))
      stay()
    }

    case Event(ClientConnected, data) => {
      log.debug("ClientConnected message received.")
      data.connectionHandlers.filter(f => f.event == ConnectionEvent.CONNECTED).map(ch => ch.handler(_))
      data.connectionListeners.filter(f => f._2 == ConnectionEvent.CONNECTED).map(f => f._1 ! ReceivedConnectionEvent(ConnectionEvent.CONNECTED, Nil))
      goto(CONNECTED)
    }

    case Event(msg: RegisterConnectionHandler, data) => {
      stay using data.copy(connectionHandlers = msg :: data.connectionHandlers)
    }

    case Event(DeRegisterConnectionHandler(sender, event), data) => {
      stay using data.copy(connectionHandlers = data.connectionHandlers.filter(f => f.event != event && f.sender != sender))
    }

    case Event(RegisterConnectionListener(e), data) => {
      stay using data.copy(connectionListeners = (sender -> e) :: data.connectionListeners)
    }

    case Event(DeRegisterConnectionListener(event), data) => {
      stay using data.copy(connectionListeners = data.connectionListeners.filter(f => f._2 != event && f._1 != sender))
    }
  }

  when(CONNECTED) {

    case Event(ClientDisconnected, data) => {
      log.debug("ClientDisconnected message received.")
      data.connectionHandlers.filter(f => f.event == ConnectionEvent.DISCONNECTED).map(ch => ch.handler(_))
      data.connectionListeners.filter(f => f._2 == ConnectionEvent.DISCONNECTED).map(f => f._1 ! ReceivedConnectionEvent(ConnectionEvent.DISCONNECTED, Nil))
      reconnector = Some(context.actorOf(Props(classOf[ReconnectLaterHandler], Connect, 5000L, self)))
      goto(DISCONNECTED)
    }

    case Event(JsonMessage(m), data) => {
      try {
        implicit lazy val formats = org.json4s.DefaultFormats
        val name = (m \ "Name").extract[String]
        val message = parse((m \ "Message").extract[String])
        log.info("Accepted" + message.toString)

        val c = Class.forName("com.edusio.virtualclassconnector.messages." + name)
        val from = c.getMethod("fromJson", classOf[JValue])

        val declaredConstructors = c.getDeclaredConstructors

        declaredConstructors.map(c => {
          c.setAccessible(true)
        })

        declaredConstructors.find(c => c.getParameterTypes.length == 0).map(bdinstance => {
          val builder = bdinstance.newInstance()
          val resultMessage = from.invoke(builder, message).asInstanceOf[JsonDeserializable]
          log.info(resultMessage.toString)
          println("sendiiing " + resultMessage)
          self ! MessageReceived(resultMessage)
        })
      } catch {
        case t: Throwable =>
          log.error("Error while parsing message: " + t)
      }
      stay()
    }

    case Event(MessageReceived(msg), data) => {
      log.info("Received msg while connected")
      msg match {
        case Ask(v) =>
          self ! SendMessage(Ack(v))

        case Ack(v) =>
          connectionTimeout.map(t => context.stop(t))
          connectionTimeout = None

        case t =>
          data.connectionHandlers.filter(f => f.event == ConnectionEvent.MESSAGE_RECEIVED).map(ch => ch.handler(msg))
          data.connectionListeners.filter(f => f._2 == ConnectionEvent.MESSAGE_RECEIVED).map(f => f._1 ! ReceivedConnectionEvent(ConnectionEvent.MESSAGE_RECEIVED, msg))

      }
      stay()
    }

    case Event(SendMessage(message), data) => {
      message match {
        case m: Ask =>
          connectionTimeout = Some(connectionTimeout.map(t => t).getOrElse(context.actorOf(Props(classOf[WebSocketPingTimeout], 10 seconds, data.client))))

        case t =>
      }
      data.client.send(("Name" -> message.getClass.getSimpleName) ~ ("Message" -> pretty(message.toJson(message))))
      stay()
    }

    case Event(msg: RegisterConnectionHandler, data) => {
      stay using data.copy(connectionHandlers = msg :: data.connectionHandlers)
    }

    case Event(DeRegisterConnectionHandler(sender, event), data) => {

      stay using data.copy(connectionHandlers = data.connectionHandlers.filter(f => f.event != event && f.sender != sender))
    }

    case Event(RegisterConnectionListener(event), data) => {
      stay using data.copy(connectionListeners = sender -> event :: data.connectionListeners)
    }

    case Event(DeRegisterConnectionListener(event), data) => {
      stay using data.copy(connectionListeners = data.connectionListeners.filter(f => f._2 != event && f._1 != sender))
    }
  }

  whenUnhandled {
    case Event(msg, data) => {
      log.warning("Unhandled message: " + msg)
      stay()
    }
  }

  onTransition {
    case CONNECTED -> DISCONNECTED =>
      log.debug("Changing transition from CONNECTED to DISCONNECTED")
      pinger.map(p => context.stop(p))
      pinger = None
      connectionTimeout.map(c => context.stop(c))
      connectionTimeout = None

    case DISCONNECTED -> CONNECTED =>
      log.debug("Changing transition from DISCONNECTED to CONNECTED")
      pinger = Some(context.actorOf(Props(classOf[WebSocketPinger], self, 5 seconds)))
  }
}

class ReconnectLaterHandler(msg: Any, millis: Long, parent: ActorRef) extends Actor with ActorLogging {
  context.setReceiveTimeout(millis millis)

  def receive = {
    case akka.actor.ReceiveTimeout =>
      parent ! msg
  }
}

class WebSocketPinger(parent: ActorRef, duration: Duration) extends Actor with ActorLogging{
  context.setReceiveTimeout(duration)
  def receive = {
    case ReceiveTimeout =>
      log.debug("Sending ASK to server")
      parent ! SendMessage(Ask())
  }
}

class WebSocketPingTimeout(duration: Duration, client: WebSocketClientContext) extends Actor with ActorLogging {
  context.setReceiveTimeout(duration)
  def receive = {
    case ReceiveTimeout =>
      log.debug("Didnt receive ack, disconnecting.")
      client.disconnect()
      context.stop(self)
  }
}