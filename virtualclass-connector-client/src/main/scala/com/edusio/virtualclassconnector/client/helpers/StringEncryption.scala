package com.edusio.virtualclassconnector.client.helpers

import java.security.MessageDigest
import javax.crypto.{Cipher, SecretKey}
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}

import org.bouncycastle.util.Arrays
import org.bouncycastle.util.encoders.{Base64, Base64Encoder}

/**
 * Created by arnostkuchar on 11.12.14.
 */
trait StringEncryption {
  def encryptString(s: String, phrase: String, salt: String) = {
    val md = MessageDigest.getInstance("md5");
    val digestOfPassword = md.digest(phrase.getBytes("UTF-8"))
    val keyBytes = Arrays.copyOf(digestOfPassword, 24)
    val key = new SecretKeySpec(keyBytes, "DESede")

    val ivBytes = Arrays.copyOf(salt.getBytes("utf-8"), 8)
    val  iv = new IvParameterSpec(ivBytes)
    val  cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding")
    cipher.init(Cipher.ENCRYPT_MODE, key, iv)

    val plainTextBytes = s.getBytes("utf-8");
    val cipherText = cipher.doFinal(plainTextBytes)

    new String(Base64.encode(cipherText), "UTF-8")
  }

  def decryptString(cipher: String, phrase: String, salt: String) = {
    val md = MessageDigest.getInstance("md5");
    val digestOfPassword = md.digest(phrase.getBytes("UTF-8"))
    val keyBytes = Arrays.copyOf(digestOfPassword, 24)
    val key = new SecretKeySpec(keyBytes, "DESede")

    val ivBytes = Arrays.copyOf(salt.getBytes("utf-8"), 8)
    val  iv = new IvParameterSpec(ivBytes)
    val decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
    decipher.init(Cipher.DECRYPT_MODE, key, iv)

    val plainTextBytes = Base64.decode(cipher)
    val plainText = decipher.doFinal(plainTextBytes)

    new String(plainText, "UTF-8");
  }


}
