package com.edusio.virtualclassconnector.client.helpers

import javafx.scene.paint.Color

/**
 * Created by arnostkuchar on 11.12.14.
 */
object Colors {
  val EBONY = Color.web("#22313F")
  val BLOODY_RED =  Color.web("#c0392b")
}
