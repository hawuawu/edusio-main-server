package com.edusio.virtualclassconnector.client.controllers

import akka.actor._
import com.edusio.virtualclassconnector.client.controllers._
import com.edusio.virtualclassconnector.messages.{Ask, GetToken, Token, JsonDeserializable}
import io.backchat.hookup.HookupClient.Receive
import io.backchat.hookup._
import io.backchat.hookup.HookupServer.HookupServerClient

import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._
/**
 * Created by arnostkuchar on 10.02.15.
 */


object ExtensionEvents extends Enumeration {
  val CONNECTED, DISCONNECTED, RECEIVED_KEYS = Value
}

case class ExtensionConnectionData(connectionHandlers: List[RegisterExtensionConnectionHandler], connectionListeners: List[Pair[ActorRef, ExtensionEvents.Value]], keys: List[String], clients: List[ExtensionControllerContext], server: HookupServer)

trait ExtensionConnectionState

object EXTENSION_CONNECTED extends ExtensionConnectionState

object EXTENSION_DISCONNECTED extends ExtensionConnectionState

case class RegisterExtensionConnectionHandler(sender: Any, handler: (Any) => Unit, event: ExtensionEvents.Value)

case class DeRegisterExtensionConnectionHandler(sender: Any, event: ExtensionEvents.Value)

case class RegisterExtensionConnectionListener(event: ExtensionEvents.Value)

case class DeRegisterExtensionConnectionListener(event: ExtensionEvents.Value)

case object GetExtensionState

case class ExtensionClientConnected(context: ExtensionControllerContext )
case class ExtensionClientDisconnected(context: ExtensionControllerContext )

case class ReceivedExtensionConnectionEvent(event: ExtensionEvents.Value, param: Any)
class ExtensionControllerContext(ac: ActorSystem, parent: ActorRef) extends HookupServerClient {
  var needSyncTokens = true;
  override def receive: Receive =   {
    case Connected ⇒
      ac.log.info("Extension Connected")
      parent ! ExtensionClientConnected(this)

    case Disconnected(_) ⇒
      ac.log.info("Extension Disconnected")
      parent ! ExtensionClientDisconnected(this)

    case m @ Error(exOpt) ⇒

    case m: TextMessage ⇒
      ac.log.info("Extension Received text message: " + m)

    case m: JsonMessage =>
      try {
        implicit lazy val formats = org.json4s.DefaultFormats
        val name = (m.content \ "Name").extract[String]
        val message = parse((m.content \ "Message").extract[String])
        ac.log.info(message.toString)

        val c = Class.forName("com.edusio.virtualclassconnector.messages." + name)
        val from = c.getMethod("fromJson", classOf[JValue])

        val declaredConstructors = c.getDeclaredConstructors

        declaredConstructors.map(c => {
          c.setAccessible(true)
        })

        declaredConstructors.find(c => c.getParameterTypes.length == 0).map(bdinstance => {
          val builder = bdinstance.newInstance()
          val resultMessage = from.invoke(builder, message).asInstanceOf[JsonDeserializable]
          ac.log.info(resultMessage.toString)
          parent ! resultMessage
        })
      } catch {
        case t: Throwable =>
          ac.log.error("Extension Error while parsing message: " + t)
      }

    case t => throw new UnsupportedOperationException(t.toString)
  }
}

case object ExtensionConnect

class ExtensionController(address: String, port: Int) extends FSM[ExtensionConnectionState, ExtensionConnectionData] with ActorLogging {
  startWith(
    EXTENSION_DISCONNECTED,
    ExtensionConnectionData(
      List(),
      List(),
      List(),
      List(),
      HookupServer(address, port) {
        new ExtensionControllerContext(context.system, self)
      }
    )
  )

  when(EXTENSION_DISCONNECTED) {
    case Event(ExtensionConnect, data: ExtensionConnectionData) => {
      data.server.start
      stay()
    }

    case Event(GetExtensionState, data: ExtensionConnectionData) => {
      sender ! EXTENSION_DISCONNECTED
      stay()
    }

    case Event(msg: ExtensionClientConnected, data: ExtensionConnectionData) => {
      goto (EXTENSION_CONNECTED) using data.copy(clients = msg.context :: data.clients)
    }
  }

  when(EXTENSION_CONNECTED) {
    case Event(GetExtensionState, data: ExtensionConnectionData) => {
      sender ! EXTENSION_CONNECTED
      stay()
    }

    case Event(msg: ExtensionClientConnected, data: ExtensionConnectionData) => {
      stay using data.copy(clients = msg.context :: data.clients)
    }

    case Event(msg: ExtensionClientDisconnected, data: ExtensionConnectionData) => {
      if(data.clients.size == 1) {
        goto(EXTENSION_DISCONNECTED) using data.copy(clients =  List())
      } else {
        stay using data.copy(clients =  data.clients.filterNot(f => f == msg.context))
      }
    }

    case Event(msg: Token, data: ExtensionConnectionData) => {
      log.debug("Received token: " + msg.toString)

      stateData.connectionHandlers.map(m => {
        m.event match {
          case ExtensionEvents.RECEIVED_KEYS =>
           m.handler(msg)

          case t =>
        }
      })

      stateData.connectionListeners.map(l => {
        l._2 match {
          case ExtensionEvents.RECEIVED_KEYS =>
            l._1 !  ReceivedExtensionConnectionEvent(ExtensionEvents.RECEIVED_KEYS, msg)
          case t =>
        }
      })
      stay()
    }
  }

  whenUnhandled {
    case Event(msg: Ask, data: ExtensionConnectionData) => {
      val message = com.edusio.virtualclassconnector.messages.Ack()
      data.clients.map(c => c.send(("Name" -> message.getClass.getSimpleName) ~ ("Message" -> pretty(message.toJson(message)))))

      data.clients.filter(c => c.needSyncTokens).foreach(c => {
        c.needSyncTokens = false
        val message2 = com.edusio.virtualclassconnector.messages.GetToken()
        c.send(("Name" -> message2.getClass.getSimpleName) ~ ("Message" -> pretty(message2.toJson(message))))
      })

      stay()
    }

    case Event(msg: RegisterExtensionConnectionHandler, data: ExtensionConnectionData) => {
      stay using data.copy(connectionHandlers = msg :: data.connectionHandlers)
    }
    case Event(msg: DeRegisterExtensionConnectionHandler, data: ExtensionConnectionData) => {
      stay using data.copy(connectionHandlers = data.connectionHandlers.filterNot(f => f.sender == msg.sender && f.event == msg.event))
    }
    case Event(msg: RegisterExtensionConnectionListener, data: ExtensionConnectionData) => {
      stay using data.copy(connectionListeners = sender -> msg.event :: data.connectionListeners)
    }
    case Event(msg: DeRegisterExtensionConnectionListener, data: ExtensionConnectionData) => {
      stay using data.copy(connectionListeners = data.connectionListeners.filterNot(l => l._1 == sender && l._2 == msg.event))
    }
  }

  onTransition {
    case EXTENSION_DISCONNECTED -> EXTENSION_CONNECTED => {
      stateData.connectionHandlers.filter(f => f.event == ExtensionEvents.CONNECTED).map(h => h.handler())
      stateData.connectionListeners.map(m => {
        m._2 match {
          case ExtensionEvents.CONNECTED =>
            m._1 ! ReceivedExtensionConnectionEvent(ExtensionEvents.CONNECTED, None)
          case t =>
        }
      })
    }

    case EXTENSION_CONNECTED -> EXTENSION_CONNECTED => {
      stateData.connectionHandlers.filter(f => f.event == ExtensionEvents.CONNECTED).map(h => h.handler())
      stateData.connectionListeners.map(m => {
        m._2 match {
          case ExtensionEvents.CONNECTED =>
            m._1 ! ReceivedExtensionConnectionEvent(ExtensionEvents.CONNECTED, None)
          case t =>
        }
      })
    }

    case EXTENSION_CONNECTED -> EXTENSION_DISCONNECTED => {
      stateData.connectionHandlers.filter(f => f.event == ExtensionEvents.DISCONNECTED).map(h => h.handler())
      stateData.connectionListeners.map(m => {
        m._2 match {
          case ExtensionEvents.DISCONNECTED =>
            m._1 ! ReceivedExtensionConnectionEvent(ExtensionEvents.DISCONNECTED, None)
          case t =>
        }
      })
    }

    case EXTENSION_DISCONNECTED -> EXTENSION_DISCONNECTED => {
      stateData.connectionHandlers.filter(f => f.event == ExtensionEvents.DISCONNECTED).map(h => h.handler())
      stateData.connectionListeners.map(m => {
        m._2 match {
          case ExtensionEvents.DISCONNECTED =>
            m._1 ! ReceivedExtensionConnectionEvent(ExtensionEvents.DISCONNECTED, None)
          case t =>
        }
      })
    }
  }
}
