package com.edusio.virtualclassconnector

import akka.actor.ActorContext

/**
 * Created by arnostkuchar on 05.12.14.
 */
object VirtualConnectorServices extends Enumeration {
  val DATABASE_ADAPTER = Value
}

trait VirtualConnectorServicesActorSelector {
    def afs(ctx: ActorContext, service: VirtualConnectorServices.Value) = ctx.actorSelection("/user/" + service.toString)
}
