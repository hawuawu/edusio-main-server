package com.edusio.virtualclassconnector

import akka.actor.{Kill, Props, ActorSystem}
import com.edusio.virtualclassconnector.messages.{JsonDeserializable, JsonSerializable, Login}
import io.backchat.hookup._


import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._

/**
 * Created by arnostkuchar on 04.12.14.
 */


class SocketContext(val ac: ActorSystem) extends HookupServerClient  {

  val actor = ac.actorOf(Props(classOf[VirtualClassMember], this))

  override def receive = {
    case Connected ⇒
      ac.log.info("Connected")

    case Disconnected(_) ⇒
      ac.log.info("Disconnected")
      ac.stop(actor)

    case m @ Error(exOpt) ⇒

    case m: TextMessage ⇒
      ac.log.info("Received text message: " + m)

    case m: JsonMessage =>
      try {
        implicit lazy val formats = org.json4s.DefaultFormats
        val name = (m.content \ "Name").extract[String]
        val message = parse((m.content \ "Message").extract[String])
        ac.log.info(message.toString)

        val c = Class.forName("com.edusio.virtualclassconnector.messages." + name)
        val from = c.getMethod("fromJson", classOf[JValue])

        val declaredConstructors = c.getDeclaredConstructors

        declaredConstructors.map(c => {
          c.setAccessible(true)
        })

        declaredConstructors.find(c => c.getParameterTypes.length == 0).map(bdinstance => {
          val builder = bdinstance.newInstance()
         val resultMessage = from.invoke(builder, message).asInstanceOf[JsonDeserializable]
          ac.log.info(resultMessage.toString)
          actor ! resultMessage
        })
      } catch {
        case t: Throwable =>
          ac.log.error("Error while parsing message: " + t)
      }

    case t => throw new UnsupportedOperationException(t.toString)
  }

  def sendJsonSerializable(message: JsonSerializable): Unit = {

    ac.log.info("Sending to client: " + message)
    this.send(
      ("Name" -> message.getClass.getSimpleName) ~ ("Message" -> pretty(message.toJson(message))))
  }

}