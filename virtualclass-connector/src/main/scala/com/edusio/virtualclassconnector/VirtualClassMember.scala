package com.edusio.virtualclassconnector

import akka.actor._
import akka.util.Timeout
import com.edusio.virtualclassconnector.messages._

import scala.util.Random
import scala.concurrent.duration._

/**
 * Created by arnostkuchar on 04.12.14.
 */

trait State

object NotAuthorized extends State

object Authorized extends State

trait Data

case class VirtualClassMemberData(context: SocketContext, user: String = "", ngrokToken: String = "", ngrokDomain: String = "", vncPassword: String = "") extends Data

class VirtualClassMember(wsctx: SocketContext) extends FSM[State, Data] with ActorLogging with VirtualConnectorServicesActorSelector {

  val database = afs(context, VirtualConnectorServices.DATABASE_ADAPTER)

  var membershipChangeWatcher: Option[ActorRef] = None
  var lastNumberOfClassesForUser = 0

  var pinger  = context.actorOf(Props(classOf[WebSocketPinger], wsctx, 5 seconds))

  startWith(NotAuthorized, VirtualClassMemberData(wsctx))

  when(NotAuthorized) {
    case Event(msg: Login, data: VirtualClassMemberData) => {
      log.info("Login: " + msg)
      database ! CredentialsVerification(msg.userName, msg.hashedPassword, msg.salt)
      stay using(data.copy(user = msg.userName))
    }

    case Event(CredentialsVerificationFailed, data: VirtualClassMemberData) => {
      data.context.sendJsonSerializable(LoginResult(
        "loginError1",
        false,
        data.user
      ))
      stay()
    }

    case Event(CredentialsVerificationOk, data: VirtualClassMemberData) => {
      database ! GetTokenAndDomain(data.user)
      stay using data.copy(user = data.user)
    }

    case Event(msg: TokenAndDomain, data: VirtualClassMemberData) => {
      val vncPass = Random.alphanumeric.take(8).mkString
      database ! UpdateUserServices(data.user, ngrokDomain = Some(msg.domain), ngrokToken = Some(msg.token), vncPassword = Some(vncPass))
      database ! GetVirtualClasses(data.user)

      stay() using data.copy(
        ngrokDomain = msg.domain,
        ngrokToken = msg.token,
        vncPassword = vncPass
      )

    }

    case Event(msg: CannotFindDomainAndTokenForUser, data: VirtualClassMemberData) => {
      val vncPass = Random.alphanumeric.take(8).mkString
      database ! UpdateUserServices(data.user, ngrokDomain = None, ngrokToken = None, vncPassword = Some(vncPass))
      database ! GetVirtualClasses(data.user)

      stay() using data.copy(
        ngrokDomain = "",
        ngrokToken = "",
        vncPassword = vncPass
      )

    }


    case Event(msg: VirtualClasses, data: VirtualClassMemberData) => {
      data.context.sendJsonSerializable(LoginResult(
        "",
      true,
      data.user,
      data.ngrokToken,
      data.ngrokDomain,
      data.vncPassword,
      msg.classes
      ))

      database ! UpdateUserServices(data.user, clientOnline = Some(true))

      membershipChangeWatcher = Some(context.actorOf(Props(classOf[MembershipChangeWatcher], self, 5000L, data.user)))

      log.info("Authorized")
      goto(Authorized)
    }

    case Event(Ask(v), data) => {
      log.debug("Received ask, reply ack")
      wsctx.sendJsonSerializable(Ack(v))
      stay()
    }

    case Event(Ack(v), data: VirtualClassMemberData) => {
      pinger ! Ack(v)
      stay()
    }

    case Event(msg, data) => {
      log.error("Not authorized to accept other messages. " + msg.toString)
      stay()
    }
  }

  when(Authorized) {
    case Event(msg: ServicesStatus, data: VirtualClassMemberData) => {
      log.info("Service status " + msg)
      database ! UpdateUserServices(
        data.user,
        ngrokPort = Some(msg.ngrokPort),
        vncPort = Some(msg.vNCPort),
        ngrokStatus = Some(msg.ngrokRunning),
        vncProxyStatus = Some(msg.vNCProxyRunning),
        vncStatus = Some(msg.vNCRunning),
        vncPassword = Some(msg.vNCDefinedPassword)
      )
      stay()
    }

    case Event(msg: GetNumberOfClassesForUser, data: VirtualClassMemberData) => {
      database ! msg
      stay()
    }

    case  Event(msg: NumberOfClassesForUser, data: VirtualClassMemberData) => {
      if(msg.userName == data.user && msg.number != lastNumberOfClassesForUser ) {
        database ! GetVirtualClasses(msg.userName)
      }
      stay()
    }

     case Event(msg: VirtualClasses, data: VirtualClassMemberData) => {
       data.context.sendJsonSerializable(VirtualClassesUpdate(
         msg.classes
       ))
       stay()
    }
  }

  whenUnhandled {
    case Event(Kill, data: VirtualClassMemberData) => {
      stop()
    }

    case Event(Ask(v), data) => {
      log.debug("Received ask, reply ack")
      wsctx.sendJsonSerializable(Ack(v))
      stay()
    }

    case Event(Ack(v), data: VirtualClassMemberData) => {
      pinger ! Ack(v)
      stay()
    }
  }

  onTermination {
    case StopEvent(s, state, data: VirtualClassMemberData) => {
      log.info("Iam dying... goodbye")
      database ! UpdateUserServices(data.user, clientOnline = Some(false))
    }
  }
}



class MembershipChangeWatcher(parent: ActorRef, millis: Long, userName: String) extends Actor with ActorLogging {
  context.setReceiveTimeout(millis millis)

  def receive = {
    case ReceiveTimeout =>
      log.info("New test for class membership")
      parent ! GetNumberOfClassesForUser(userName)
  }
}

class WebSocketPinger(wsctx: SocketContext, duration: Duration) extends Actor with ActorLogging {
  var timeouter: Option[ActorRef] = None
  context.setReceiveTimeout(duration)

  def receive = {
    case Ack(v) =>
      timeouter.map(t => context.stop(t))
      timeouter = None

    case ReceiveTimeout =>
      log.debug("Sending ASK to client")
      timeouter = Some(timeouter.map(t => t)
        .getOrElse(context.actorOf(Props(classOf[WebSocketPingTimeouter], wsctx, 10 seconds))))
      wsctx.sendJsonSerializable(Ask())
  }
}

class WebSocketPingTimeouter(wsctx: SocketContext, duration: Duration) extends Actor with ActorLogging {
  context.setReceiveTimeout(duration)
  def receive = {
    case ReceiveTimeout =>
      log.debug("Didnt receive ack, disconnecting.")
      wsctx.disconnect()
      context.stop(self)
  }
}

