package com.edusio.virtualclassconnector

/**
 * Created by Wlsek on 4. 6. 2015.
 */
object Application extends App {
  override def main (args: Array[String]) {
    EntryPointService.start
  }
}
