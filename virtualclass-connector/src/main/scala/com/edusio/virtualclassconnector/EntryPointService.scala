package com.edusio.virtualclassconnector

import java.sql.{Connection, DriverManager}

import akka.actor.{Props, ActorSystem}
import io.backchat.hookup.{HookupServer, SslSupport}
import org.hawu.utils._
import org.hawu.utils.file.JarLocation
import org.slf4j.LoggerFactory

/**
 * Created by Wlsek on 4. 6. 2015.
 */

object EntryPointService extends services.EntryPointService with JarLocation {

  private var actorSystem: Option[ActorSystem] = None
  private var hookUpServer: Option[HookupServer] = None
  private var sqlConnection: Option[Connection] = None
  private var retry = true
  var logger = LoggerFactory.getLogger(this.getClass)

  def start = {
    try {
      logger.info("Starting service")
      actorSystem = Some(ActorSystem("VirtualClassConnector"))
      sqlConnection.map(connection => connection.close())
      sqlConnection = None

      actorSystem.map(as => {

        val virtualManagerAddress = as.settings.config getString ("virtual-class.vmAddress")
        val virtualManagerPort = as.settings.config getInt ("virtual-class.vmPort")
        val dbAdress = as.settings.config getString ("virtual-class.dbAddress")
        val dbName = as.settings.config getString ("virtual-class.dbName")
        val username = as.settings.config getString ("virtual-class.dbUser")
        val password = as.settings.config getString ("virtual-class.dbPassword")

        val driver = "com.mysql.jdbc.Driver"
        val url = "jdbc:mysql://" + dbAdress + "/" + dbName

        Class.forName(driver)
        val connection = DriverManager.getConnection(url, username, password)
        sqlConnection = Some(connection)

        as.actorOf(Props(classOf[VirtualClassMemberDatabaseAdapter], connection), VirtualConnectorServices.DATABASE_ADAPTER.toString)

        val sslSupport =
          SslSupport(
            keystorePath = actualPath + / + "keystore.jks",
            keystorePassword = "classconnector12214",
            algorithm = "SunX509"
          )

        val server = HookupServer(virtualManagerAddress, virtualManagerPort, sslSupport) {
          new SocketContext(as)
        }

        server onStop {
          as.log.info("Server stopped.")
        }

        server onStart {
          as.log.info("Server started.")
        }

        server.start

        hookUpServer = Some(server)
      })
    } catch {
      case t: Throwable =>
        if(retry) {
          logger.error("Error while starting service, trying in 5 seconds. [" + t.toString + "]")
          Thread.sleep(5000)
          start
        } else {
          sqlConnection.map(connection => connection.close())
          sqlConnection = None

          logger.error("Error while starting service. Stopped.")
        }
    }
  }

  def stop = {
    sqlConnection.map(connection => connection.close())
    sqlConnection = None

    actorSystem.map(as => {
      as.shutdown()
    })

    hookUpServer.map(s => {
      s.stop
    })

    retry = false
  }

  def restart = {
    logger.error("Restarting service.")
    stop
    retry = true
    start
  }
}
