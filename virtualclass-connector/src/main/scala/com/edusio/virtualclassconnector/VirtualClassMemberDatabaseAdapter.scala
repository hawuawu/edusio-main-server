package com.edusio.virtualclassconnector

import java.security.MessageDigest
import java.sql.{SQLNonTransientConnectionException, Connection}

import akka.actor.{ActorLogging, Actor}
import com.mysql.jdbc.exceptions.jdbc4.{MySQLNonTransientConnectionException, CommunicationsException}
import org.bouncycastle.util.encoders.Base64
import org.joda.time.DateTime

/**
 * Created by arnostkuchar on 05.12.14.
 */


case class CredentialsVerification(username: String, password: String, salt: String = "")

case object CredentialsVerificationOk

case object CredentialsVerificationFailed

case class GetVirtualClasses(username: String)

case class VirtualClasses(classes: List[String])

case class UpdateUserServices(
                               userName: String,
                               ngrokPort: Option[Int] = None,
                               vncPort: Option[Int] = None,
                               ngrokToken: Option[String] = None,
                               ngrokDomain: Option[String] = None,
                               ngrokStatus: Option[Boolean] = None,
                               vncProxyStatus: Option[Boolean] = None,
                               vncStatus: Option[Boolean] = None,
                               vncPassword: Option[String] = None,
                               clientOnline: Option[Boolean] = None
                               )

case class GetNumberOfClassesForUser(userName: String)

case class NumberOfClassesForUser(userName: String, number: Int)

case class GetTokenAndDomain(usr: String)

case class TokenAndDomain(user: String, token: String, domain: String)

case class CannotFindDomainAndTokenForUser(user: String)

class VirtualClassMemberDatabaseAdapter(sqlConnection: Connection) extends Actor with ActorLogging {

  def receive = {
    case GetTokenAndDomain(usr) => {
      try {
        // val sts = sqlConnection.createStatement
        // val result = sts.executeQuery("SELECT * FROM VirtualClassMembershipView WHERE user_name = \"" + usr + "\"")
        val md = MessageDigest.getInstance("SHA-256")
        md.update(usr.getBytes("UTF-8"))
        val finalized = new String(Base64.encode(md.digest), "UTF-8")
        var sum = 0
        finalized.toCharArray.map(s => sum = sum + s.toInt)

        val coded = usr + (new DateTime().getMillis + sum).toString
        sender ! TokenAndDomain(usr, coded, coded)
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole connector.")
          EntryPointService.restart

        case t: Throwable =>
          log.error("Error while getting token and domain for user: " + usr)
          sender ! CannotFindDomainAndTokenForUser(usr)
      }
    }

    case CredentialsVerification(usr, psw, salt) => {
      try {
        val sts = sqlConnection.createStatement
        val result = sts.executeQuery("SELECT * FROM VirtualClassMembers WHERE access_token = \"" + psw + "\" AND user_name=\"" + usr + "\" LIMIT 1;")

        if (result.next()) {
          sender ! CredentialsVerificationOk
        } else {
          sender ! CredentialsVerificationFailed
        }

        sts.close()
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole connector.")
          EntryPointService.restart

        case t: Throwable =>
          log.error("Error while CredentialsVerification: " + usr)
          sender ! CredentialsVerificationFailed
      }
    }

    case gvc: GetVirtualClasses => {
      try {
        val sts = sqlConnection.createStatement()
        val result = sts.executeQuery("SELECT posts.post_title from edusio.wp_posts as posts JOIN edusio_virtualclass.VirtualClassMembers as members ON posts.id = members.class_token WHERE user_name = \"" + gvc.username + "\"")
        var courses: List[String] = List()

        while (result.next()) {
          courses = result.getString("post_title") :: courses
        }

        sender ! VirtualClasses(courses)
        log.debug("Sending virtual classes: " + courses)
        sts.close()
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole connector.")
          EntryPointService.restart

        case t: Throwable =>
          log.error("Error while getting virtual classes.")
      }
    }

    case GetNumberOfClassesForUser(username) => {
      try {
        val sts = sqlConnection.createStatement
        val result = sts.executeQuery("SELECT * FROM VirtualClassMembershipView WHERE user_name = \"" + username + "\"")

        if (result.next()) {
          sender ! NumberOfClassesForUser(username, result.getInt("classes"))
        } else {
          sender ! NumberOfClassesForUser(username, 0)
        }

        sts.close()
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole connector.")
          EntryPointService.restart

        case t: Throwable =>
          log.error("Error while getting number of classes for user: " + username + "| error: " + t.toString)
          sender ! NumberOfClassesForUser(username, 0)
      }
    }

    case msg: UpdateUserServices => {
      var labels = ""
      var values = ""
      var duplicate = ""
      /*
      msg.clientOnline.map(s => set = set + ",client_online=" + s.toString)
      msg.ngrokDomain.map(s => set = set + ",ngrok_domain=" + s.toString)
      msg.ngrokPort.map(s => set = set + ",ngrok_port=" + s.toString)
      msg.ngrokStatus.map(s => set = set + ",ngrok_service_status=" + s.toString)
      msg.ngrokToken.map(s => set = set + ",ngrok_token=" + s.toString)
      msg.vncPassword.map(s => set = set + ",vnc_password=" + s.toString)
      msg.vncPort.map(s => set = set + ",vnc_port=" + s.toString)
      msg.vncProxyStatus.map(s => set = set + ",vnc_proxy_service_status=" + s.toString)
      msg.vncStatus.map(s => set = set + ",vnc_service_status=" + s.toString)
*/
      msg.clientOnline.map(s => {
        labels = labels + ",client_online"
        values = values + "," + s.toString
        duplicate = duplicate + ",client_online=VALUES(client_online)"
      })

      msg.ngrokDomain.map(s => {
        labels = labels + ",ngrok_domain"
        values = values + ",\"" + s.toString + "\""
        duplicate = duplicate + ",ngrok_domain=VALUES(ngrok_domain)"
      })

      msg.ngrokPort.map(s => {
        labels = labels + ",ngrok_port"
        values = values + "," + s.toString
        duplicate = duplicate + ",ngrok_port=VALUES(ngrok_port)"
      })

      msg.ngrokStatus.map(s => {
        labels = labels + ",ngrok_service_status"
        values = values + "," + s.toString
        duplicate = duplicate + ",ngrok_service_status=VALUES(ngrok_service_status)"
      })

      msg.ngrokToken.map(s => {
        labels = labels + ",ngrok_token"
        values = values + ",\"" + s.toString + "\""
        duplicate = duplicate + ",ngrok_token=VALUES(ngrok_token)"
      })

      msg.vncPassword.map(s => {
        labels = labels + ",vnc_password"
        values = values + ",\"" + s.toString + "\""
        duplicate = duplicate + ",vnc_password=VALUES(vnc_password)"
      })

      msg.vncPort.map(s => {
        labels = labels + ",vnc_port"
        values = values + "," + s.toString
        duplicate = duplicate + ",vnc_port=VALUES(vnc_port)"
      })

      msg.vncProxyStatus.map(s => {
        labels = labels + ",vnc_proxy_service_status"
        values = values + "," + s.toString
        duplicate = duplicate + ",vnc_proxy_service_status=VALUES(vnc_proxy_service_status)"
      })

      msg.vncStatus.map(s => {
        labels = labels + ",vnc_service_status"
        values = values + "," + s.toString
        duplicate = duplicate + ",vnc_service_status=VALUES(vnc_service_status)"
      })

      if (values != "" && labels != "") {
          try {
            values = "\"" + msg.userName + "\"" + values
            labels = "user_name" + labels
            duplicate = duplicate.substring(1)
            val statement = "INSERT INTO VirtualClassUserServices (" + labels + ") VALUES(" + values + ") ON DUPLICATE KEY UPDATE " + duplicate

            log.info("Statement: " + statement)

            val sts = sqlConnection.createStatement()
            sts.executeUpdate(statement)
            sts.close()
        } catch {
          case e: SQLNonTransientConnectionException =>
            log.error("Error in database, restarting whole connector.")
            EntryPointService.restart

          case t: Throwable =>
        }
      }

    }
  }
}
