package cz.noveit.elearning.vcmanager

import java.sql.{Connection, DriverManager}

import akka.actor.{ActorSystem, Props}
import org.hawu.utils._
import org.hawu.utils.file.JarLocation
import org.slf4j.LoggerFactory

/**
 * Created by Wlsek on 4. 6. 2015.
 */


object Services {
  val FOREVER_CONTROLLER = "foreverListController"
  val SCHEDULER_CONTROLLER = "schedulerController"
  val VIRTUALCLASS_CONTROLLER = "virtualclassController"
  val VIRTUALCLASS_MEMBER_ADAPTER_CONTROLLER = "membersController"
}


object EntryPointService extends services.EntryPointService with JarLocation {

  private var actorSystem: Option[ActorSystem] = None
  var edusioSqlConnection: Option[Connection] = None
  var virtualClassSqlConnection: Option[Connection] = None

  private var retry = true
  var logger = LoggerFactory.getLogger(this.getClass)

  def start = {
    try {
      logger.info("Starting service")
      edusioSqlConnection.map(connection => connection.close())
      edusioSqlConnection = None

      virtualClassSqlConnection.map(connection => connection.close())
      virtualClassSqlConnection = None

      actorSystem = Some(ActorSystem("VirtualClassManager-AS"))
      actorSystem.map(as => {
        logger.debug("Safe pause after class ends is sets to " + as.settings.config.getLong("virtual-class.classDurationAfterEnd").toString + " seconds.")
        val driver = "com.mysql.jdbc.Driver"
        val virtualManagerAddress = as.settings.config.getConfig("virtual-class").getString("vmAddress")
        val virtualManagerDomain = as.settings.config.getConfig("virtual-class").getString("vmDomain")

        Class.forName(driver)
        val connection = DriverManager.getConnection(
          "jdbc:mysql://" + as.settings.config.getConfig("virtual-class").getString("dbAddress") + "/" + as.settings.config.getConfig("virtual-class").getString("dbName"),
          as.settings.config.getConfig("virtual-class").getString("dbUser"),
          as.settings.config.getConfig("virtual-class").getString("dbPassword")
        )

        virtualClassSqlConnection = Some(connection)

        val connection2 = DriverManager.getConnection(
          "jdbc:mysql://" + as.settings.config.getConfig("edusio").getString("dbAddress") + "/" + as.settings.config.getConfig("edusio").getString("dbName"),
          as.settings.config.getConfig("edusio").getString("dbUser"),
          as.settings.config.getConfig("edusio").getString("dbPassword")
        )

        edusioSqlConnection = Some(connection2)


        as.actorOf(Props(classOf[BeforeStartCleanup], connection2, connection, () => {
          as.actorOf(Props[ForeverListController], Services.FOREVER_CONTROLLER)
          as.actorOf(Props(classOf[VirtualClassEdusioMemberAdapter], connection2), Services.VIRTUALCLASS_MEMBER_ADAPTER_CONTROLLER)
          as.actorOf(Props(classOf[ScheduleController], connection2), Services.SCHEDULER_CONTROLLER)
          as.actorOf(Props(classOf[VirtualClassController], connection, virtualManagerAddress, virtualManagerDomain), Services.VIRTUALCLASS_CONTROLLER)
        })) ! StartClean(virtualManagerAddress)
      })
    } catch {
      case t: Throwable =>
        if (retry) {
          logger.error("Error while starting service, trying in 5 seconds. [" + t.toString + "]")
          Thread.sleep(5000)
          start
        } else {
          edusioSqlConnection.map(connection => connection.close())
          edusioSqlConnection = None

          virtualClassSqlConnection.map(connection => connection.close())
          virtualClassSqlConnection = None

          logger.error("Error while starting service. Stopped.")
        }
    }
  }

  def stop = {
    edusioSqlConnection.map(connection => connection.close())
    edusioSqlConnection = None

    virtualClassSqlConnection.map(connection => connection.close())
    virtualClassSqlConnection = None

    actorSystem.map(as => {
      as.shutdown()
    })
    actorSystem = None
    retry = false
  }

  def restart = {
    logger.error("Restarting service.")
    stop
    retry = true
    start
  }
}

