package cz.noveit.elearning.vcmanager

import java.math.BigInteger
import java.net.ServerSocket
import java.sql.Connection
import java.security.SecureRandom

import akka.actor.{ActorLogging, Props, ActorRef, Actor}
import org.hawu.utils.port.GetFreePort

import scala.util.Random

/**
 * Created by arnostkuchar on 21.11.14.
 */


object TokenGenerator {
  private val random = new SecureRandom()

  def token: String = {
    new BigInteger(130, random).toString(45)
  }
}

class VirtualClassController(sqlConnection: Connection, address: String, domain: String) extends Actor with ActorLogging {

  private var startingClasses: List[String] = List()
  private var startClassesQueue: Array[String] = Array()

  private var endingClasses: List[String] = List()
  private var endClassesQueue: Array[String] = Array()

  val databaseController = context.actorOf(Props(classOf[VirtualClassDatabaseController], sqlConnection))
  val foreverController = context.actorSelection("/user/" + Services.FOREVER_CONTROLLER)

  def receive = {
    case msg: VirtualClassStarts =>
      if(!endingClasses.contains(msg.classToken)) {
        if(!startingClasses.contains(msg.classToken)) {
          startingClasses = msg.classToken :: startingClasses
          log.info("Virtual class started {}", msg.classToken)
          databaseController ! TakeClass(msg.classToken, address, domain)
        } else {
          log.info("Already starting.")
        }
      }  else {
        startClassesQueue = startClassesQueue :+ msg.classToken
      }

    case msg: ClassTaken =>
      log.info("All tokens generated for {}", msg.classToken)
    try {
        databaseController ! UpdateTakenClass(msg.classToken, address, GetFreePort(
          context.system.settings.config.getInt("virtual-class.portMin") to
          context.system.settings.config.getInt("virtual-class.portMax")
        ).get)
    } catch {
      case t: Throwable =>
        log.error("Error while trying to find socket.")
        self ! UpdateTakenClassFailed(msg.classToken, address, -1)
    }

    case msg: CannotTakeClass =>
      log.error("Cannot take class, letting some other controller to take it.")
      self ! CannotStartVirtualClass(msg.classToken, 0, address)


    case msg: UpdateTakenClassSuccessful =>
      foreverController ! StartVirtualClass(msg.classToken, msg.port, address)

    case msg: UpdateTakenClassFailed =>
      databaseController ! FreeTakenClass(msg.classToken)
      startingClasses = startingClasses.filterNot(c => c == msg.classToken)
      endClassesQueue = endClassesQueue.filterNot(c => c == msg.classToken)


    case msg: SuccessfullyStartedVirtualClass =>
      startingClasses = startingClasses.filterNot(c => c == msg.database)
      if(endClassesQueue.contains(msg.database)){
        endClassesQueue = endClassesQueue.filterNot(f => f == msg.database)
        self ! VirtualClassEnds(msg.database)
      }

    case msg: CannotStartVirtualClass =>
      startingClasses = startingClasses.filterNot(c => c == msg.database)
      if(endClassesQueue.contains(msg.database)){
        endClassesQueue = endClassesQueue.filterNot(f => f == msg.database)
        self ! VirtualClassEnds(msg.database)
      }


    case msg: VirtualClassEnds =>
       if(!startingClasses.contains(msg.classToken)) {
        if(!endingClasses.contains(msg.classToken)) {
          endingClasses = msg.classToken :: endingClasses
          log.info("Virtual class ends {}", msg.classToken)
          foreverController ! StopVirtualClass(msg.classToken)
        } else {
          log.info("Already ending.")
        }
      }  else {
        endClassesQueue = endClassesQueue :+ msg.classToken
      }

    case msg: SuccessfullyStoppedVirtualClass =>
      log.info("Freeing virtual class {}", msg.database)
      databaseController ! FreeTakenClass(msg.database)

    case msg: FreeTakenClassSuccessful =>
      endingClasses = endingClasses.filterNot(c => c == msg.classToken)
      if(startClassesQueue.contains(msg.classToken)){
        startClassesQueue = startClassesQueue.filterNot(f => f == msg.classToken)
        self ! VirtualClassStarts(msg.classToken)
      }

    case msg: FreeTakenClassFailed =>
      endingClasses = endingClasses.filterNot(c => c == msg.classToken)
      if(startClassesQueue.contains(msg.classToken)){
        startClassesQueue = startClassesQueue.filterNot(f => f == msg.classToken)
        self ! VirtualClassStarts(msg.classToken)
      }

    case msg: CannotStopVirtualClass =>
      log.info("Cannot stop virtual class {}")
      foreverController ! StopVirtualClass(msg.database)
  }
}

