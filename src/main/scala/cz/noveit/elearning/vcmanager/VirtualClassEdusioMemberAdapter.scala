package cz.noveit.elearning.vcmanager

import java.sql.{SQLNonTransientConnectionException, Connection}

import akka.actor.{Props, ActorLogging, Actor, ActorRef}
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 22. 5. 2015.
 */

case class SetCourseOnline(courseId: Int)

case class SetCourseOffline(courseId: Int)

/*
* Usermeta:
* course_online => course_id,
* course_online_role => (instructor || student)
* */
class VirtualClassEdusioMemberAdapter(edusioConnection: Connection) extends Actor with ActorLogging {
  context.actorOf(Props(classOf[RemoveVirtualClassOnlineCache], self, edusioConnection)) ! RemoveMembersOfPastVirtualClass

  var ready = false;
  var coursesToStart: Set[Int] = Set()
  var coursesToEnd: Set[Int] = Set()

  def receive = {
    case msg: SetCourseOnline =>
      log.debug("SetCourseOnline in ready?[{}] state {}", ready.toString, msg.toString)
      if (ready) {
        context.actorOf(Props(classOf[GetMembersOfCouserActor], self, edusioConnection)) ! GetCourseMembers(msg.courseId)
      } else {
        coursesToStart += msg.courseId
      }

    case msg: CourseMembers =>
      log.debug("CourseMembers")
      context.actorOf(Props(classOf[UpdateVirtualClassOnlineCache], self, edusioConnection)) ! UpdateMembersForVirtualClass(msg.users, msg.courseId, msg.eventBegins, msg.eventEnds)

    case msg: SetCourseOffline =>
      log.debug("SetCourseOffline in ready?[{}] state {}", ready.toString, msg.toString)
      if (ready) {
        context.actorOf(Props(classOf[RemoveVirtualClassOnlineCache], self, edusioConnection)) ! RemoveMembersByCourseId(msg.courseId)
      } else {
        coursesToEnd += msg.courseId
      }

    case RemoveMembersOfPastVirtualClassDone =>
      log.debug("RemoveMembersOfPastVirtualClassDone")
      coursesToStart.map(c => {
        self ! SetCourseOnline(c)
      })

      coursesToEnd.map(c => {
        self ! SetCourseOffline(c)
      })

      ready = true
  }
}


case class CourseMembers(users: List[CourseMember], eventBegins: DateTime, eventEnds: DateTime, courseId: Int)

object CourseMemberRole extends Enumeration {
  val student, teacher = Value
}

case class CourseMember(id: Int, role: CourseMemberRole.Value)

case class GetCourseMembers(courseId: Int)

class GetMembersOfCouserActor(parent: ActorRef, connection: Connection) extends Actor with ActorLogging {


  def receive = {
    case msg: GetCourseMembers =>
      log.debug("Removing course members first")
      context.actorOf(Props(classOf[RemoveVirtualClassOnlineCache], self, connection)) ! RemoveMembersByCourseId(msg.courseId)

    case msg: RemoveMembersByCourseIdDone => {
      log.debug("Courses members removed, creating new cache")

      var members: List[CourseMember] = List()
      var eventBegins: DateTime = DateTime.now()
      var eventEnds: DateTime = DateTime.now()
      val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");

      val selectSubstitutionInstructorQuery = "SELECT rooster.instructor_id as instructor_id, occurs.occur_begin, occurs.occur_end  FROM wp_posts as posts " +
        "JOIN wp_my_calendar_categories as categories ON categories.category_name = posts.post_title " +
        "JOIN wp_my_calendar as calendar ON calendar.event_category=categories.category_id " +
        "JOIN wp_my_calendar_events as occurs ON occurs.occur_event_id=calendar.event_id " +
        "JOIN edusio_events_susbstitution_rooster as rooster ON rooster.occur_id = occurs.occur_id " +
        "WHERE posts.id = " + msg.courseId + " AND occurs.occur_begin <= NOW() AND occurs.occur_end > NOW() " +
        "LIMIT 1"

      val selectAuthorInstructorQuery = "SELECT users.id  as instructor_id, posts.id, occurs.occur_begin, occurs.occur_end FROM `wp_posts` as posts " +
        "JOIN wp_users as users ON posts.post_author=users.id " +
        "JOIN wp_my_calendar_categories as categories ON categories.category_name=posts.post_title " +
        "JOIN wp_my_calendar as calendar ON calendar.event_category=categories.category_id " +
        "JOIN wp_my_calendar_events as occurs ON occurs.occur_event_id=calendar.event_id " +
        "WHERE occurs.occur_begin <= NOW() AND occurs.occur_end > NOW() AND posts.id=" + msg.courseId + " GROUP BY users.id " +
        "LIMIT 1;"

      val selectMembersQuery = "SELECT users.id as student_id, posts.id FROM `wp_posts` as posts " +
        "JOIN wp_usermeta as user_meta ON user_meta.meta_key=CONCAT('course_status', posts.id) " +
        "JOIN wp_users as users ON user_meta.user_id=users.id " +
        "JOIN wp_my_calendar_categories as categories ON categories.category_name=posts.post_title " +
        "JOIN wp_my_calendar as calendar ON calendar.event_category=categories.category_id " +
        "JOIN wp_my_calendar_events as occurs ON occurs.occur_event_id=calendar.event_id " +
        "WHERE user_meta.meta_value=2 AND occurs.occur_begin <= NOW() AND occurs.occur_end > NOW() AND posts.id=" + msg.courseId + " GROUP BY users.id;"

      try {
        val sts = connection.createStatement()
        var result = sts.executeQuery(selectSubstitutionInstructorQuery)
        log.debug("Running substitution query: " + selectSubstitutionInstructorQuery)
        if (!result.next()) {
          result = sts.executeQuery(selectAuthorInstructorQuery)
          log.debug("Substitution not found, running reagular teacher query: " + selectAuthorInstructorQuery)

          while (result.next()) {
            members = CourseMember(result.getInt("instructor_id"), CourseMemberRole.teacher) :: members
            eventBegins = DateTime.parse(result.getString("occur_begin"), formatter)
            eventEnds = DateTime.parse(result.getString("occur_end"), formatter)
          }
        } else {
          members = CourseMember(result.getInt("instructor_id"), CourseMemberRole.teacher) :: members
          eventBegins = DateTime.parse(result.getString("occur_begin"), formatter)
          eventEnds = DateTime.parse(result.getString("occur_end"), formatter)
        }

        log.debug("Running members query: " + selectMembersQuery)
        result = sts.executeQuery(selectMembersQuery)

        while (result.next()) {
          members = CourseMember(result.getInt("student_id"), CourseMemberRole.student) :: members
        }

        sts.close()
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole service.")
          EntryPointService.restart

        case t =>
          log.error("Error while GetMembersOfCouserActor " + t.toString)
      }

      parent ! CourseMembers(members, eventBegins, eventEnds, msg.courseId)
      context.stop(self)
    }
  }
}

case class UpdateMembersForVirtualClass(members: List[CourseMember], courseId: Int, eventBegin: DateTime, eventEnd: DateTime)

case class VirtualClassMembershipUpdated(courseId: Int)

class UpdateVirtualClassOnlineCache(parent: ActorRef, connection: Connection) extends Actor with ActorLogging {
  def receive = {
    case msg: UpdateMembersForVirtualClass => {
      val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");
      msg.members.map(m => {
        try {
          val query = "INSERT INTO edusio_virtualclass_membership_cache (course_id, event_begins, event_ends, user_id, role)" +
            "VALUES (" + msg.courseId + ",'" + msg.eventBegin.toString(formatter) + "','" + msg.eventEnd.toString(formatter) + "'," + m.id + ",'" + m.role.toString + "')"
          val sts = connection.createStatement()
          sts.executeUpdate(query)
          sts.close()
        } catch {
          case e: MySQLIntegrityConstraintViolationException =>
          case e: SQLNonTransientConnectionException =>
            log.error("Error in database, restarting whole service.")
            EntryPointService.restart
          case t =>
            log.error("Error while UpdateVirtualClassOnlineCache " + t.toString)
        }
      })

      parent ! VirtualClassMembershipUpdated(msg.courseId)
      context.stop(self)
    }
  }
}

case class RemoveMembersByCourseId(courseId: Int)

case class RemoveMembersByCourseIdDone(courseId: Int)

case object RemoveMembersOfPastVirtualClass

case object RemoveMembersOfPastVirtualClassDone

class RemoveVirtualClassOnlineCache(parent: ActorRef, connection: Connection) extends Actor with ActorLogging {
  def receive = {
    case msg: RemoveMembersByCourseId =>
      try {
        val query = "DELETE FROM edusio_virtualclass_membership_cache WHERE course_id=" + msg.courseId + ";"
        val sts = connection.createStatement()
        sts.executeUpdate(query)
        sts.close()
        parent ! RemoveMembersByCourseIdDone(msg.courseId)
        context.stop(self)
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole service.")
          EntryPointService.restart
        case t =>
          parent ! RemoveMembersByCourseIdDone(msg.courseId)
          context.stop(self)
      }

    case RemoveMembersOfPastVirtualClass =>
      try {
        val query = "DELETE FROM edusio_virtualclass_membership_cache WHERE event_ends<NOW();"
        val sts = connection.createStatement()
        sts.executeUpdate(query)
        sts.close()
        parent ! RemoveMembersOfPastVirtualClassDone
        context.stop(self)
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole service.")
          EntryPointService.restart
        case t =>
          parent ! RemoveMembersOfPastVirtualClassDone
          context.stop(self)
      }
  }
}