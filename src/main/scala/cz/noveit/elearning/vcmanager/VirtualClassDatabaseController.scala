package cz.noveit.elearning.vcmanager

import java.sql.{SQLNonTransientConnectionException, Connection}

import akka.actor.{Actor, ActorLogging}
import org.joda.time.DateTime

/**
 * Created by arnostkuchar on 24.11.14.
 */

case class GetMembers(classToken: String)

case class Members(classToken: String, members: List[String])

case class CreateAccessTokenForMember(member: String, token: String, classToken: String)

case class AccessTokenCreated(member: String, token: String)

case class CannotCreateAccessToken(member: String, token: String)

case class TakeClass(classToken: String, address: String, domain: String)

case class ClassTaken(classToken: String)

case class CannotTakeClass(classToken: String)

case class UpdateTakenClass(classToken: String, address: String, port: Int)

case class UpdateTakenClassSuccessful(classToken: String, address: String, port: Int)

case class UpdateTakenClassFailed(classToken: String, address: String, port: Int)

case class FreeTakenClass(classToken: String)

case class FreeTakenClassSuccessful(classToken: String)

case class FreeTakenClassFailed(classToken: String)

class VirtualClassDatabaseController(sqlConnection: Connection) extends Actor with ActorLogging {

  def receive = {
    case msg: GetMembers =>
      try {
        val sts = sqlConnection.createStatement()
        val members = sts.executeQuery("SELECT * FROM VirtualClassMembers WHERE class_token='" + msg.classToken + "'")


        var list: List[String] = List()
        while (members.next()) {
          list = members.getString("user_name") :: list
        }
        sts.close()
        sender ! Members(msg.classToken, list)
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole service.")
          EntryPointService.restart
        case t: Throwable =>
          sender ! Members(msg.classToken, List())
      }

    case msg: CreateAccessTokenForMember =>
      try {
        val sts = sqlConnection.createStatement()
        val update = sts.executeUpdate("UPDATE VirtualClassMembers SET access_token='" + msg.token + "' WHERE user_name='" + msg.member + "' && class_token='" + msg.classToken + "' ")
        sts.close()

        if (update > 0) {
          sender ! AccessTokenCreated(msg.member, msg.token)
        } else {
          sender ! CannotCreateAccessToken(msg.member, msg.token)
        }
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole service.")
          EntryPointService.restart
        case t: Throwable =>
          sender ! CannotCreateAccessToken(msg.member, msg.token)
      }

    case msg: TakeClass =>
      log.debug("Taking class {}", msg.toString)
      try {
        val dbQuery = "CREATE DATABASE IF NOT EXISTS vc_" + msg.classToken;
        val query = "INSERT INTO VirtualClassRooster(class_token, handler_address, handler_port, domain) VALUES ('" + msg.classToken + "', '" + msg.address + "', 0, '"+msg.domain+"')"
        val sts = sqlConnection.createStatement()
        sts.executeUpdate(dbQuery)
        val update = sts.executeUpdate(query)
        sts.close()

        if (update > 0) {
          log.debug("Class taken. {}", msg.classToken)

          sender ! ClassTaken(msg.classToken)
        } else {
          log.error("Cannot take {} class", msg.classToken)
          sender ! CannotTakeClass(msg.classToken)
        }
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole service.")
          EntryPointService.restart

        case t: Throwable =>
          log.error("Cannot take {} class with reason {}", msg.classToken, t.toString)
          sender ! CannotTakeClass(msg.classToken)
      }

    case msg: UpdateTakenClass =>
      try {
        val sts = sqlConnection.createStatement()
        val update = sts.executeUpdate("UPDATE VirtualClassRooster SET handler_address='" + msg.address + "', handler_port=" + msg.port + " WHERE class_token='" + msg.classToken + "'")
        sts.close()

        if (update > 0) {
          //   sender ! ClassTaken(msg.classToken)

          log.info("Successfully updated info for rooster {} class", msg.classToken)
          sender ! UpdateTakenClassSuccessful(msg.classToken, msg.address, msg.port)
        } else {
          log.error("Failed to update info for rooster {} class", msg.classToken)
          sender ! UpdateTakenClassFailed(msg.classToken, msg.address, msg.port)
        }
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole service.")
          EntryPointService.restart

        case t: Throwable =>
          log.error("Failed to update info for rooster {} class", msg.classToken)
          sender ! UpdateTakenClassFailed(msg.classToken, msg.address, msg.port)
      }

    case msg: FreeTakenClass =>
      try {
        val sts = sqlConnection.createStatement()
        val update = sts.executeUpdate("DELETE FROM VirtualClassRooster  WHERE class_token='" + msg.classToken + "'")
        sts.close()
        if (update > 0) {
          //   sender ! ClassTaken(msg.classToken)

          sender ! FreeTakenClassSuccessful(msg.classToken)
          log.info("Successfully removed info for rooster {} class", msg.classToken)
        } else {
          ()
          sender ! FreeTakenClassFailed(msg.classToken)
          log.error("Failed to removed info for rooster {} class", msg.classToken)
        }
      } catch {
        case e: SQLNonTransientConnectionException =>
          log.error("Error in database, restarting whole service.")
          EntryPointService.restart

        case t: Throwable =>
          sender ! FreeTakenClassFailed(msg.classToken)
          log.error("Failed to removed info for rooster {} class due to {}", msg.classToken, t)
      }
  }
}
