package cz.noveit.elearning.vcmanager

import java.sql.Connection

import akka.actor.{Actor, ActorLogging}

/**
 * Created by Wlsek on 28. 5. 2015.
 */

case class StartClean(nodeAddress: String)
class BeforeStartCleanup(edusioConnection: Connection, virtualClassConnection: Connection, doneHandler: () => Unit) extends Actor with ActorLogging {
  def receive = {
    case sc: StartClean => {
      try {
        log.debug("Started cleaning")
        val sts = virtualClassConnection.createStatement()
        val query = "DELETE FROM `VirtualClassRooster` WHERE `handler_address`=\"" + sc.nodeAddress + "\""
        sts.executeUpdate(query)
        sts.close()

        log.debug("Done cleaning")
        doneHandler()
      } catch {
        case t =>
          log.error("Error in cleanup, restarting whole service.")
          EntryPointService.restart

      }
    }
  }
}
