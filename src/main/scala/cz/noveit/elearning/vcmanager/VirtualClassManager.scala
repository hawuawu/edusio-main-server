package cz.noveit.elearning.vcmanager

import java.io.{InputStreamReader, BufferedReader}
import java.sql.DriverManager

import akka.actor.{Props, ActorSystem}

/**
 * Created by arnostkuchar on 21.11.14.
 */

object VirtualClassManager extends App {
  override def main(args: Array[String]): Unit = {
    EntryPointService.start
  }
}
