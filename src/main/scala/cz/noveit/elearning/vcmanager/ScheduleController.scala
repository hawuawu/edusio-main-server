package cz.noveit.elearning.vcmanager

import java.sql.{SQLNonTransientConnectionException, Connection}

import akka.actor._
import org.bouncycastle.util.encoders.Base64
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTimeZone, DateTime}
import scala.concurrent.duration._

/**
 * Created by arnostkuchar on 21.11.14.
 */

case class VirtualClassToSchedule(classToken: String, startDate: DateTime, endDate: DateTime)

case object TestChangeInDatabase

case class VirtualClassScheduling(virtualClass: VirtualClassToSchedule, planedStart: Option[ActorRef], planedEnd: Option[ActorRef])

trait VirtualClassStateChange

case class VirtualClassStarts(classToken: String) extends VirtualClassStateChange

case class VirtualClassEnds(classToken: String) extends VirtualClassStateChange

case class EdusioCourseEvent(courseId: Int, courseTitle: String, begin: String, end: String, occurId: Int) {
  def hash = new String(Base64.encode((courseId.toString + courseTitle + begin + end + occurId.toString).getBytes()))
}


case class GetScheduleTimeForClass(classToken: String)
case class CannotGetScheduleTimeForClass(classToken: String)
case class ScheduleTimeForClass(classToken: String, start: DateTime, end: DateTime)


class ScheduleController(sqlConnection: Connection) extends Actor with ActorLogging {

  val virtualClassController = context.actorSelection("/user/" + Services.VIRTUALCLASS_CONTROLLER)
  val membershipController = context.actorSelection("/user/" + Services.VIRTUALCLASS_MEMBER_ADAPTER_CONTROLLER)

  private var scheduling: List[VirtualClassScheduling] = List()
  private var lastChange = ""

  self ! TestChangeInDatabase

  def receive = {

    case msg: GetScheduleTimeForClass =>
     sender ! scheduling.find(s => s.virtualClass.classToken == msg.classToken)
        .map(f => ScheduleTimeForClass(f.virtualClass.classToken, f.virtualClass.startDate, f.virtualClass.endDate))
      .getOrElse(CannotGetScheduleTimeForClass(msg.classToken))

    case TestChangeInDatabase =>
        try {
          log.info("Checking change in database of virtual classes.")

          val sts = sqlConnection.createStatement()
          val result = sts.executeQuery("SELECT * FROM edusio_course_events_view;")


          var hash = "";
          var events: List[EdusioCourseEvent] = List()
          while (result.next()) {
            val event = EdusioCourseEvent(
              result.getInt("course_id"),
              result.getString("course_title"),
              result.getString("begin"),
              result.getString("end"),
              result.getInt("occur_id")
            )
            hash = event.hash + hash
            events = event :: events
          }
          sts.close()

          if (lastChange != hash) {
            lastChange = hash

            log.info("There is change, recreating handlers.")

            val vClasses: List[VirtualClassToSchedule] = events.map(e => {
              VirtualClassToSchedule(
                e.courseId.toString,
                DateTime.parse(e.begin, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS")),
                DateTime.parse(e.end, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS"))
                  .plusSeconds(context.system.settings.config.getInt("virtual-class.classDurationAfterEnd"))
              )
            })

            val now = new DateTime().getMillis

            scheduling.map(sch => {
              sch.planedStart.map(h => context.stop(h))
              sch.planedEnd.map(h => context.stop(h))
            })

            scheduling = List()

            vClasses.map(c => {

              var schedule = VirtualClassScheduling(c, None, None)

              val started = !(c.startDate.getMillis - now >= 0)
              val ended = !(c.endDate.getMillis - now >= 0)

              if (started && !ended) {
                self ! VirtualClassStarts(c.classToken)
                val handler = context.actorOf(Props(classOf[VirtualClassStateChangeHandler], VirtualClassEnds(c.classToken), c.endDate.getMillis - now, self))
                schedule = schedule.copy(planedEnd = Some(handler))
              } else if (!started && ended) {
                val handler = context.actorOf(Props(classOf[VirtualClassStateChangeHandler], VirtualClassStarts(c.classToken), c.startDate.getMillis - now, self))
                schedule = schedule.copy(planedStart = Some(handler))

                self ! VirtualClassEnds(c.classToken)
              } else if (!started && !ended) {
                val handler1 = context.actorOf(Props(classOf[VirtualClassStateChangeHandler], VirtualClassStarts(c.classToken), c.startDate.getMillis - now, self))
                schedule = schedule.copy(planedStart = Some(handler1))

                val handler2 = context.actorOf(Props(classOf[VirtualClassStateChangeHandler], VirtualClassEnds(c.classToken), c.endDate.getMillis - now, self))
                schedule = schedule.copy(planedEnd = Some(handler2))
              } else if (started && ended) {
                self ! VirtualClassEnds(c.classToken)
              }

              scheduling = (if (schedule.planedEnd.isDefined || schedule.planedStart.isDefined) {
                List(schedule)
              } else {
                List()
              }) ::: scheduling
            })
          }
          context.actorOf(Props(classOf[VirtualClassStateChangeHandler], TestChangeInDatabase, 10000L, self))
        } catch {
          case e: SQLNonTransientConnectionException =>
            log.error("Error in database, restarting whole service.")
            EntryPointService.restart

          case t =>
            log.error("Error while checking database changes " + t.toString)
            context.actorOf(Props(classOf[VirtualClassStateChangeHandler], TestChangeInDatabase, 10000L, self))
        }

    case vcs: VirtualClassStarts =>
   //   scheduling = scheduling.filter(sch => sch.virtualClass.classToken != vcs.classToken)
      membershipController ! SetCourseOnline(vcs.classToken.toInt)
      virtualClassController ! vcs

    case vce: VirtualClassEnds =>
   //   scheduling = scheduling.filter(sch => sch.virtualClass.classToken != vce.classToken)
      membershipController ! SetCourseOffline(vce.classToken.toInt)
      virtualClassController ! vce
  }
}


class VirtualClassStateChangeHandler(message: Any, millis: Long, actorRef: ActorRef) extends Actor {

  context.setReceiveTimeout(millis millis)

  def receive = {
    case ReceiveTimeout =>
      actorRef ! message
      context.stop(self)
  }
}
