package cz.noveit.elearning.vcmanager

import java.io._
import java.net.{URLDecoder, URLEncoder}
import java.security.MessageDigest
import java.util.Base64

import akka.actor.{Props, ActorRef, ActorLogging, Actor}
import org.bouncycastle.crypto.digests.MD5Digest
import org.bouncycastle.jcajce.provider.asymmetric.rsa.DigestSignatureSpi.MD5
import org.bouncycastle.util.encoders.Base64Encoder
import org.joda.time.Interval

/**
 * Created by arnostkuchar on 21.11.14.
 */


case object GetRunningVirtualClasses

trait VirtualClassUpTime

case object VirtualClassUpTimeStopped extends VirtualClassUpTime

case class VirtualClassUpTimeRunning(time: String) extends VirtualClassUpTime

case class RunningVirtualClass(row: Int, database: String, port: Int, address: String, upTime: VirtualClassUpTime)

case class RunningVirtualClasses(classes: List[RunningVirtualClass])

case class StartVirtualClass(database: String, port: Int, address: String)

case class CannotStartVirtualClass(database: String, port: Int, address: String)

case class SuccessfullyStartedVirtualClass(database: String, port: Int, address: String)

case class StopVirtualClass(database: String)

case class CannotStopVirtualClass(database: String)

case class SuccessfullyStoppedVirtualClass(database: String)

case object GetRawOutputOfForeverList
case class RawOutput(output: String)

class ForeverListController extends Actor with ActorLogging {
val actualPath = URLDecoder.decode( new File(this.getClass.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile.getPath, "UTF-8")
  def / = File.separator

  def receive = {

    case GetRawOutputOfForeverList => {
      log.debug("GetRawOutputOfForeverList received.")
      val procesListOfScripts = Runtime.getRuntime.exec(actualPath + / + "foreverlistscript.sh")
      val procesListOfScriptsReader = new BufferedReader(new InputStreamReader(procesListOfScripts.getInputStream))
      procesListOfScripts.waitFor()

      var output = ""
      while (procesListOfScriptsReader.ready()) {
        output = procesListOfScriptsReader.readLine() + output
      }

      log.debug("Replyiing raw output {}.", output)
      sender ! RawOutput(output)
    }

    case GetRunningVirtualClasses => {
      try {
        log.debug("GetRunningVirtualClasses received.")

        var virtualClasses: List[RunningVirtualClass] = List()

        val procesListOfScripts = Runtime.getRuntime.exec(actualPath + / + "foreverlistscript.sh")
        val procesListOfScriptsReader = new BufferedReader(new InputStreamReader(procesListOfScripts.getInputStream))
        procesListOfScripts.waitFor()

        var ommitingLines = 2

        while (procesListOfScriptsReader.ready()) {
          if (ommitingLines <= 0) {
            val splitted = procesListOfScriptsReader.readLine().split(" ").toList.filter(f => f.trim != "")


            virtualClasses = RunningVirtualClass(
              splitted.find(s => s.matches("\\[.*.\\]")).map(s => s.replace("[", "").replace("]", "").toInt).getOrElse(-1),
              splitted.find(s => s.matches("database=.*")).map(s => splitPair(s)).getOrElse(""),
              splitted.find(s => s.matches("port=.*")).map(s => splitPair(s).toInt).getOrElse(-1),
              splitted.find(s => s.matches("address=.*")).map(s => splitPair(s)).getOrElse(""),
              VirtualClassUpTimeStopped
            ) :: virtualClasses

          } else {
            procesListOfScriptsReader.readLine()
            ommitingLines -= 1
          }
        }


        val procesListOfUptimes = Runtime.getRuntime.exec(actualPath + / + "foreverlistuptime.sh")
        val procesListOfUptimesReader = new BufferedReader(new InputStreamReader(procesListOfUptimes.getInputStream))
        procesListOfUptimes.waitFor()

        ommitingLines = 2

        while (procesListOfUptimesReader.ready()) {
          if (ommitingLines <= 0) {
            val splitted = procesListOfUptimesReader.readLine().split(" ").toList.filter(f => f.trim != "")
            val tail = splitted.tail

            val row = tail(0).replace("[", "").replace("]", "").toInt
            val upTime = tail(1) match {
              case "STOPPED" => VirtualClassUpTimeStopped
              case t => VirtualClassUpTimeRunning(t)
            }

            virtualClasses = virtualClasses.find(c => c.row == row).map(c => c.copy(upTime = upTime)).toList ::: virtualClasses.filterNot(c => c.row == row)
          } else {
            procesListOfUptimesReader.readLine()
            ommitingLines -= 1
          }
        }

        sender ! RunningVirtualClasses(virtualClasses)
      } catch {
        case t: Throwable =>
          log.error("Exception thrown. " + t)
      }
    }

    case s: StartVirtualClass =>
      context.actorOf(Props(classOf[StartVirtualClassHandler], self, sender, s))

    case s: StopVirtualClass =>
      val responder = context.actorOf(Props(classOf[StopRunningVirtualClassQuery], s, sender))
      self.tell(GetRunningVirtualClasses, responder)
  }


  def splitPair(s: String): String = {
    s.split("=")(1)
  }


}

class StartVirtualClassHandler(parent: ActorRef, origin: ActorRef, originalMessage: StartVirtualClass) extends Actor with ActorLogging {
  val actualPath = URLDecoder.decode( new File(this.getClass.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile.getPath, "UTF-8")
  def / = File.separator

  val scheduler = context.actorSelection("/user/" + Services.SCHEDULER_CONTROLLER)
    scheduler ! GetScheduleTimeForClass(originalMessage.database)

  def receive = {
    case msg: ScheduleTimeForClass =>
      continuation((new Interval(msg.start, msg.end).toDurationMillis / 1000).toString)
    case msg: CannotGetScheduleTimeForClass =>
      continuation("")
    case msg: SuccessfullyStartedVirtualClass =>
      origin ! msg
      context.stop(self)
    case msg: CannotStartVirtualClass =>
      origin ! msg
      context.stop(self)
  }

  def continuation(durationParameter: String) = {
    val command = actualPath + / + "forever_start.sh " + originalMessage.database + " " + originalMessage.port + " " + originalMessage.address + " " + durationParameter
    log.debug("running command: ")
    log.debug(command)
    try{
      val out = new PrintWriter(new BufferedWriter(new FileWriter(actualPath + / + "virtualclass_commands.log", true)))
      out.println(command);
    } catch {
      case t => log.error("Cannot append to virtualclass_commands {}", t.toString)
    }
    val runClassScript = Runtime.getRuntime.exec(command)
    val runClassScriptReader = new BufferedReader(new InputStreamReader(runClassScript.getInputStream))

    var output = ""
    while (runClassScriptReader.ready()) {
      output = runClassScriptReader.readLine() + output
    }

    runClassScript.waitFor()

    val responder = context.actorOf(Props(classOf[StartRunningVirtualClassResponder], originalMessage, self))
    parent.tell(GetRunningVirtualClasses, responder)
    //context.stop(self)
  }
}


class StartRunningVirtualClassResponder(originalMsg: StartVirtualClass, respondTo: ActorRef) extends Actor {
  val actualPath = URLDecoder.decode( new File(this.getClass.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile.getPath, "UTF-8")
  def / = File.separator

  def receive = {
    case msg: RunningVirtualClasses =>
      msg.classes.find(c => c.database == originalMsg.database).map(found => {
        if (found.upTime.isInstanceOf[VirtualClassUpTimeRunning]) {
          respondTo ! SuccessfullyStartedVirtualClass(originalMsg.database, originalMsg.port, originalMsg.address)
          context.stop(self)
        } else {
          val command =  actualPath + / + "forever_stop.sh " + originalMsg.database
          val runClassScript = Runtime.getRuntime.exec(command)
          runClassScript.waitFor()

          respondTo ! CannotStartVirtualClass(originalMsg.database, originalMsg.port, originalMsg.address)
          context.stop(self)
        }
        found
      }).getOrElse({
        respondTo ! CannotStartVirtualClass(originalMsg.database, originalMsg.port, originalMsg.address)
        context.stop(self)
      })
  }
}

class StopRunningVirtualClassQuery(originalMsg: StopVirtualClass, respondTo: ActorRef) extends Actor with ActorLogging {
  val actualPath = URLDecoder.decode( new File(this.getClass.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile.getPath, "UTF-8")
  def / = File.separator

  def receive = {
    case msg: RunningVirtualClasses => {
      val vClass = msg.classes.find(c => c.database == originalMsg.database)
      if(vClass.isDefined) {
        val command =  actualPath + / + "forever_stop.sh " + originalMsg.database
        log.debug("Running forever stop script. [" +command+ "]")
        val runClassScript = Runtime.getRuntime.exec(command)
        runClassScript.waitFor()

        val responder = context.actorOf(Props(classOf[StopRunningVirtualClassResponder], originalMsg, respondTo))
        sender.tell(GetRunningVirtualClasses, responder)
      } else {
        log.debug("Class not found, already stopped?.")
        respondTo ! SuccessfullyStoppedVirtualClass(originalMsg.database)
        context.stop(self)
      }
    }

    case msg: CannotStopVirtualClass =>
      respondTo ! msg
      context.stop(self)

    case msg: SuccessfullyStoppedVirtualClass =>
      respondTo ! msg
      context.stop(self)
  }
}

class StopRunningVirtualClassResponder(originalMsg: StopVirtualClass, respondTo: ActorRef) extends Actor {
  def receive = {
    case msg: RunningVirtualClasses =>
      if(msg.classes.find(c => c.database == originalMsg.database).isEmpty){
        respondTo ! SuccessfullyStoppedVirtualClass(originalMsg.database)
      } else {
        respondTo ! CannotStopVirtualClass(originalMsg.database)
      }
      context.stop(self)
      //msg.classes.find(c => c.row == originalMsg.row)
  }
}