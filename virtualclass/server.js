var http = require('http');

var address = "";
var port = "";
var database = "";

process.argv.slice(2).forEach(function (val, index, array) {
    var pair = val.replace(/ /g, '').split("=");
    
    if (pair.length === 2) {
        console.log(pair[0] + ': ' + pair[1]);
        if(pair[0] === "address") {
            address = pair[1]
        } else if (pair[0] === "port") {
            port = pair[1]
        } else if (pair[0] === "database") {
            database = pair[1]
        }
    } else {
        console.log("Not acceptable parameter");
    }

});

if(address !== "" && port !== "" && database !== "") {
   var server = http.createServer(function (request, response) {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Hello World\n");
    });

    server.listen(port, address);
    console.log("Server running at " + address + ":" + port + " on database " + database); 
} else {
        console.log("Not enought parameters.");
}


