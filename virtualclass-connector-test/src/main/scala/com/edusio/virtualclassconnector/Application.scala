package com.edusio.virtualclassconnector

import javafx.application.Platform

import akka.actor.{Props, ActorSystem}
import com.edusio.virtualclassconnector.client.controllers.{StartNoVNCService, ServiceController}
import org.hawu.utils.websocket.client.WebSocketClientContextFactory

/**
 * Created by arnostkuchar on 12.12.14.
 */
object Application extends App {
  override def main(args: Array[String]) = {

    val as = ActorSystem("VirtuarclassConnector")
   // val server = com.edusio.virtualclassconnector.Etr(as)
   // com.edusio.virtualclassconnector.client.EntryPoint(as)
   // server.map(s => s.start)



    as.awaitTermination()
    Runtime.getRuntime().halt(0);
  }
}
