import akka.actor.{Props, ActorSystem}
import com.edusio.tests.dumb.database._
import com.edusio.tests.dumb.statistic.DumbStatisticManager
import com.edusio.tsp.database.tasks._
import com.edusio.tsp.transaction.TransactionManager
import org.scalatest._

import scala.concurrent.Await
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._

/**
 * Created by Wlsek on 18. 8. 2015.
 */
class TransactionManagerTests extends FlatSpec {
  "TransactionManager" should "query not completed transactions first." in {
    val as = ActorSystem("edusio-test")
    val database = as.actorOf(Props(classOf[DumbDatabaseManagerFirst]))
    val statistic = as.actorOf(Props(classOf[DumbStatisticManager]))


    implicit val timeout = Timeout(5 seconds)
    val future = database ? QueryNotCompletedTransactionsTest
    val tmanager = as.actorOf(Props(classOf[TransactionManager], database, statistic, 5000L, 0.25F))

    val result = Await.result(future, timeout.duration)
    result match {
      case QueryNotCompletedTransactions =>
        as.shutdown()
        assert(true)
      case t =>
        as.shutdown()
        fail()
    }
  }

  "TransactionManager" should "handle transactions with woocomerce \"complete\" and with woocomerce \"canceled\" status." in {
    val as = ActorSystem("edusio-test")
    val database = as.actorOf(Props(classOf[DumbDatabaseManagerSecond]))
    val statistic = as.actorOf(Props(classOf[DumbStatisticManager]))

    implicit val timeout = Timeout(5 seconds)
    val future = database ? QueryClearNotCompletedTransactionArtefactsTest
    val tmanager = as.actorOf(Props(classOf[TransactionManager], database, statistic, 5000L, 0.25F))
    val result = Await.result(future, timeout.duration).asInstanceOf[Int]

    as.shutdown()
    result === 0
  }



  "TransactionCancelCommissionHandler" should "complete canceled transaction." in {
    val as = ActorSystem("edusio-test")
    val database = as.actorOf(Props(classOf[DumbDatabaseManagerThird]))
    val statistic = as.actorOf(Props(classOf[DumbStatisticManager]))

    implicit val timeout = Timeout(5 seconds)
    val future = database ? QueryCompleteNotCompletedTransactionTest
    val tmanager = as.actorOf(Props(classOf[TransactionManager], database, statistic, 20000L, 0.25F))
    Await.result(future, timeout.duration) match {
      case NotCompletedTransactionCompleted =>
        assert(true)
        as.shutdown()
      case t =>
        as.shutdown()
        fail()
    }
  }

  "TransactionManager" should "wait until appearing uncomplete transaction." in {
    val as = ActorSystem("edusio-test")
    val database = as.actorOf(Props(classOf[DumbDatabaseManagerForth]))
    val statistic = as.actorOf(Props(classOf[DumbStatisticManager]))

    implicit val timeout = Timeout(10 seconds)
    val future = database ? QueryNotCompletedTransactionsTimeoutTest
    val tmanager = as.actorOf(Props(classOf[TransactionManager], database, statistic, 1000L, 0.25F))
    Await.result(future, timeout.duration) match {
      case QueryNotCompletedTransactions =>
        assert(true)
        as.shutdown()
      case t =>
        as.shutdown()
        fail()
    }
  }

  "TransactionCreateCommissionHandler" should "query instructor." in {
    val as = ActorSystem("edusio-test")
    val database = as.actorOf(Props(classOf[DumbDatabaseManagerFifth]))
    val statistic = as.actorOf(Props(classOf[DumbStatisticManager]))

    implicit val timeout = Timeout(5 seconds)
    val future = database ? QueryInstructorForCourseIdTest
    val tmanager = as.actorOf(Props(classOf[TransactionManager], database, statistic, 1000L, 0.25F))
    Await.result(future, timeout.duration) match {
      case q: QueryInstructorForCourseId =>
        assert(true)
        as.shutdown()
      case t =>
        as.shutdown()
        fail()
    }
  }

  "TransactionCreateCommissionHandler" should "query cooperation for instructor." in {
    val as = ActorSystem("edusio-test")
    val database = as.actorOf(Props(classOf[DumbDatabaseManagerSixth]))
    val statistic = as.actorOf(Props(classOf[DumbStatisticManager]))

    implicit val timeout = Timeout(5 seconds)
    val future = database ? QueryCooperationForInstructorIdTest
    val tmanager = as.actorOf(Props(classOf[TransactionManager], database, statistic, 1000L, 0.25F))
    Await.result(future, timeout.duration) match {
      case q: QueryCooperationForTransactionAndInstructor =>
        assert(true)
        as.shutdown()
      case t =>
        as.shutdown()
        fail()
    }
  }


  "TransactionCreateCommissionHandler" should "query events for course." in {
    val as = ActorSystem("edusio-test")
    val database = as.actorOf(Props(classOf[DumbDatabaseManagerSeventh]))
    val statistic = as.actorOf(Props(classOf[DumbStatisticManager]))

    implicit val timeout = Timeout(5 seconds)
    val future = database ? QueryEventsForCourseIdTest
    val tmanager = as.actorOf(Props(classOf[TransactionManager], database, statistic, 1000L, 0.25F))
    Await.result(future, timeout.duration) match {
      case q: QueryEventsForCourseId =>
        assert(true)
        as.shutdown()
      case t =>
        as.shutdown()
        fail()
    }
  }


  "TransactionCreateCommissionHandler" should "query substitution for event." in {
    val as = ActorSystem("edusio-test")
    val database = as.actorOf(Props(classOf[DumbDatabaseManagerEight]))
    val statistic = as.actorOf(Props(classOf[DumbStatisticManager]))

    implicit val timeout = Timeout(5 seconds)
    val future = database ? QuerySubstitutionInstructorForEventIdTest
    val tmanager = as.actorOf(Props(classOf[TransactionManager], database, statistic, 1000L, 0.25F))
    Await.result(future, timeout.duration) match {
      case q: QuerySubstitutionInstructorForEventId =>
        assert(true)
        as.shutdown()
      case t =>
        as.shutdown()
        fail()
    }

  }



}
