package com.edusio.tests.dumb.monthly

import akka.actor.Actor

/**
 * Created by Wlsek on 2. 11. 2015.
 */

case object GetHashForTest
case class HashForTest(hash: Option[String])
class HashProducerAndTester(var hashes: Array[String]) extends Actor {
  def receive = {
    case GetHashForTest =>
      sender ! HashForTest(hashes.headOption)
      if(hashes.size > 0) {
        hashes = hashes.tail
      }
  }
}
