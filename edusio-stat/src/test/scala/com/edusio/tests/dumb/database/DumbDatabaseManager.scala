package com.edusio.tests.dumb.database

import akka.actor.{ActorRef, Actor}
import com.edusio.tsp.database.tasks._
import org.joda.time.DateTime

/**
 * Created by Wlsek on 18. 8. 2015.
 */


case object QueryNotCompletedTransactionsTest

class DumbDatabaseManagerFirst extends Actor {
  private var queryNotCompletedTransactionsTestListener: ActorRef = _

  def receive = {
    case QueryNotCompletedTransactionsTest =>
      queryNotCompletedTransactionsTestListener = sender
    case QueryNotCompletedTransactions =>
      queryNotCompletedTransactionsTestListener ! QueryNotCompletedTransactions
  }
}

case object QueryClearNotCompletedTransactionArtefactsTest

class DumbDatabaseManagerSecond extends Actor {
  private var queryClearNotCompletedTransactionArtefactsTestListener: ActorRef = _

  private var transactions = List(
    NotCompletedTransaction(1, 1, 1, 1000, "completed", 20.20F),
    NotCompletedTransaction(2, 2, 2, 1000, "completed", 20.20F),
    NotCompletedTransaction(3, 3, 1, 1000, "completed", 20.20F),
    NotCompletedTransaction(4, 4, 2, 1000, "canceled", 20.20F),
    NotCompletedTransaction(5, 5, 1, 1000, "canceled", 20.20F),
    NotCompletedTransaction(6, 6, 2, 1000, "canceled", 20.20F)
  )

  def receive = {
    case QueryClearNotCompletedTransactionArtefactsTest =>
      queryClearNotCompletedTransactionArtefactsTestListener = sender

    case QueryNotCompletedTransactions =>
      sender ! NotCompletedTransactions(transactions)

    case q: QueryClearNotCompletedTransactionArtefacts =>
      transactions = transactions.filterNot(t => t.transactionId == q.transactionId)
      if (transactions.size == 0) queryClearNotCompletedTransactionArtefactsTestListener ! 0

  }
}

case object QueryCompleteNotCompletedTransactionTest

class DumbDatabaseManagerThird extends Actor {
  private var queryCompleteNotCompletedTransactionTest: ActorRef = _

  private var transactions = List(
    NotCompletedTransaction(4, 4, 2, 1000, "canceled", 20.20F),
    NotCompletedTransaction(5, 5, 1, 1000, "canceled", 20.20F),
    NotCompletedTransaction(6, 6, 2, 1000, "canceled", 20.20F)
  )

  def receive = {
    case QueryCompleteNotCompletedTransactionTest =>
      queryCompleteNotCompletedTransactionTest = sender
    case QueryNotCompletedTransactions =>
      sender ! NotCompletedTransactions(transactions)
    case q: QueryClearNotCompletedTransactionArtefacts =>
      sender ! ClearNotCompletedTransactionArtefactsComplete
    case q: QueryCompleteNotCompletedTransaction =>
      println("uii")
      sender ! NotCompletedTransactionCompleted
      transactions = transactions.filterNot(t => t.transactionId == q.transaction.transactionId)
      if (transactions.size == 0) queryCompleteNotCompletedTransactionTest ! NotCompletedTransactionCompleted

  }
}


case object QueryNotCompletedTransactionsTimeoutTest

class DumbDatabaseManagerForth extends Actor {
  private var queryNotCompletedTransactionsTimeoutTestListener: ActorRef = _
  private var counter = 0

  def receive = {
    case QueryNotCompletedTransactionsTimeoutTest =>
      queryNotCompletedTransactionsTimeoutTestListener = sender

    case QueryNotCompletedTransactions =>
      if (counter > 3) {
        queryNotCompletedTransactionsTimeoutTestListener ! QueryNotCompletedTransactions
      } else {
        counter += 1;
        sender ! NotCompletedTransactions(List())
      }
  }
}


case object QueryInstructorForCourseIdTest

class DumbDatabaseManagerFifth extends Actor {
  private var queryInstructorForCourseIdTestListener: ActorRef = _
  private var transactions = List(
    NotCompletedTransaction(1, 1, 1, 1000, "completed", 20.20F),
    NotCompletedTransaction(2, 2, 2, 1000, "completed", 20.20F),
    NotCompletedTransaction(3, 3, 1, 1000, "completed", 20.20F)
  )

  private val insructors = List(
    1 -> 25,
    2 -> 26
  )

  def receive = {
    case QueryInstructorForCourseIdTest =>
      queryInstructorForCourseIdTestListener = sender

    case QueryNotCompletedTransactions =>
      sender ! NotCompletedTransactions(transactions)

    case q: QueryClearNotCompletedTransactionArtefacts =>
      sender ! ClearNotCompletedTransactionArtefactsComplete

    case q: QueryInstructorForCourseId =>
      transactions = transactions.filterNot(t => t.courseId == q.courseId)
      insructors.find(i => i._1 == q.courseId).map(i => sender ! QueriedInstructor(i._2))

      if (transactions.size == 0) {
        queryInstructorForCourseIdTestListener ! q
      }

  }
}


case object QueryCooperationForInstructorIdTest

class DumbDatabaseManagerSixth extends Actor {
  private var queryCooperationForInstructorIdTestListener: ActorRef = _
  private var transactions = List(
    NotCompletedTransaction(1, 1, 1, 1000, "completed", 20.20F),
    NotCompletedTransaction(2, 2, 2, 1000, "completed", 20.20F),
    NotCompletedTransaction(3, 3, 1, 1000, "completed", 20.20F)
  )

  private val instructors = List(
    1 -> 25,
    2 -> 26
  )

  private var count: scala.collection.mutable.HashMap[Int, Int] = scala.collection.mutable.HashMap(
    25 -> 0,
    26 -> 0
  )

  def receive = {
    case QueryCooperationForInstructorIdTest =>
      queryCooperationForInstructorIdTestListener = sender

    case QueryNotCompletedTransactions =>
      sender ! NotCompletedTransactions(transactions)

    case q: QueryClearNotCompletedTransactionArtefacts =>
      sender ! ClearNotCompletedTransactionArtefactsComplete

    case q: QueryInstructorForCourseId =>
      transactions = transactions.filterNot(t => t.courseId == q.courseId)
      instructors.find(i => i._1 == q.courseId).map(i => sender ! QueriedInstructor(i._2))

    case q: QueryCooperationForTransactionAndInstructor =>
      count(q.instructorId) += 1
      if (count(25) == 2 && count(26) == 1) queryCooperationForInstructorIdTestListener ! q
  }
}

case object QueryEventsForCourseIdTest

class DumbDatabaseManagerSeventh extends Actor {
  private var queryEventsForCourseIdTestListener: ActorRef = _
  private var transactions = List(
    NotCompletedTransaction(1, 1, 1, 1000, "completed", 20.20F),
    NotCompletedTransaction(2, 2, 2, 1000, "completed", 20.20F),
    NotCompletedTransaction(3, 3, 1, 1000, "completed", 20.20F)
  )

  private val instructors = List(
    1 -> 25,
    2 -> 26
  )

  private var count: scala.collection.mutable.HashMap[Int, Int] = scala.collection.mutable.HashMap(
    1 -> 0,
    2 -> 0
  )

  def receive = {
    case QueryEventsForCourseIdTest =>
      queryEventsForCourseIdTestListener = sender

    case QueryNotCompletedTransactions =>
      sender ! NotCompletedTransactions(transactions)

    case q: QueryClearNotCompletedTransactionArtefacts =>
      sender ! ClearNotCompletedTransactionArtefactsComplete

    case q: QueryInstructorForCourseId =>
      transactions = transactions.filterNot(t => t.courseId == q.courseId)
      instructors.find(i => i._1 == q.courseId).map(i => sender ! QueriedInstructor(i._2))

    case q: QueryCooperationForTransactionAndInstructor =>
      sender ! Cooperations(List())

    case q: QueryEventsForCourseId =>
      count(q.courseId) += 1
      if (count(1) == 2 && count(2) == 1) queryEventsForCourseIdTestListener ! q
  }
}


case object QuerySubstitutionInstructorForEventIdTest

class DumbDatabaseManagerEight extends Actor {
  private var querySubstitutionInstructorForEventIdListener: ActorRef = _

  private var transactions = List(
    NotCompletedTransaction(1, 1, 1, 1000, "completed", 20.20F),
    NotCompletedTransaction(2, 2, 2, 1000, "completed", 20.20F),
    NotCompletedTransaction(3, 3, 1, 1000, "completed", 20.20F)
  )

  private val instructors = List(
    1 -> 25,
    2 -> 26
  )

  private var count: scala.collection.mutable.HashMap[Int, Int] = scala.collection.mutable.HashMap(
    1 -> 0,
    2 -> 0,
    3 -> 0
  )

  private var events: scala.collection.mutable.HashMap[Int, List[EventForCourse]] = scala.collection.mutable.HashMap(
    2 -> List(
      EventForCourse(1, DateTime.now().minusDays(1), DateTime.now().plusDays(1)),
      EventForCourse(2, DateTime.now().plusDays(1).plusMinutes(20), DateTime.now().plusDays(2)),
      EventForCourse(3, DateTime.now().plusDays(4), DateTime.now().plusDays(5))
    ),
    1 -> List()
  )


  def receive = {
    case QuerySubstitutionInstructorForEventIdTest =>
      querySubstitutionInstructorForEventIdListener = sender

    case QueryNotCompletedTransactions =>
      sender ! NotCompletedTransactions(transactions)

    case q: QueryClearNotCompletedTransactionArtefacts =>
      sender ! ClearNotCompletedTransactionArtefactsComplete

    case q: QueryInstructorForCourseId =>
      transactions = transactions.filterNot(t => t.courseId == q.courseId)
      instructors.find(i => i._1 == q.courseId).map(i => sender ! QueriedInstructor(i._2))

    case q: QueryCooperationForTransactionAndInstructor =>
      sender ! Cooperations(List())

    case q: QueryEventsForCourseId =>
      sender ! EventsForCourse(events(q.courseId))

    case q: QuerySubstitutionInstructorForEventId =>
      sender ! NoSubstitutionInstructorForEvent(q.eventId)
      println(q)
      count(q.eventId) += 1
      if (count(1) == 1 && count(2) == 1 && count(3) == 1) querySubstitutionInstructorForEventIdListener ! q
  }
}