import java.io._

import akka.actor.{ActorRef, Props, ActorSystem}
import com.edusio.tests.dumb.monthly.{HashForTest, GetHashForTest, HashProducerAndTester}
import com.edusio.tsp.tools._
import org.scalatest.FlatSpec
import akka.actor._
import akka.dispatch._
import akka.pattern.ask
import akka.util.Timeout
import org.scalatest.exceptions.TestFailedException
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Random

/**
 * Created by Wlsek on 2. 11. 2015.
 */
class HashTestSuiteTest extends FlatSpec {
  "HashTestSuite " should "properly return HashTestSuiteSame, HashTestSuiteDiffer, HashTestSuiteNext." in {
    val as = ActorSystem("HashTestSuiteTest")
    try {
      val hashFile = new File("hashTestFile.hash")
      if (!hashFile.exists()) {
        hashFile.createNewFile()
      } else {
        hashFile.delete()
        hashFile.createNewFile()
      }

      var generatedHashBatches: List[Array[String]] = List()
      var totalHashes = 0
      for (i <- 0 to 5) {
        var batch: Array[String] = Array()
        for (x <- 1 to Random.nextInt(5)) {
          batch = batch :+ (10000 + Random.nextInt(9000000)).toString
          totalHashes += 1
        }
        generatedHashBatches = batch :: generatedHashBatches
      }

      val bw = new BufferedWriter(new FileWriter(hashFile))
      var testers: Array[ActorRef] = Array()

      generatedHashBatches.map(b => {
        b.map(hash => {
          bw.write(hash)
          bw.newLine()
        })
        testers = testers :+ as.actorOf(Props(classOf[HashProducerAndTester], b))
      })
      bw.flush()
      bw.close()

      val br = new BufferedReader(new FileReader((hashFile)))
      val suiteTester = new HashTestSuite(
        () => {
          val hash = br.readLine()
          if (hash != null) {
            Some(hash)
          } else {
            None
          }
        }
      )




      var testing = true
      var safeCounter = 100
      var testersPosition = 0
      var tester = testers(testersPosition)

      var ok = 0
      while (safeCounter > 0 && testing) {
        implicit val timeout = Timeout(5 seconds)
        suiteTester(Await.result((tester ? GetHashForTest), timeout.duration).asInstanceOf[HashForTest].hash) match {
          case HashTestSuiteResultSame =>
            ok += 1
          case HashTestSuiteResultDiffer =>
            throw new TestFailedException("Hashes missmatch!", 1)
          case HashTestSuiteResultTestNext =>
            testersPosition += 1
            if(testersPosition < testers.size) {
              tester = testers(testersPosition)
            } else {
              if(suiteTester.isEmptyInFuture_?()) {
                testing = false
              } else {
                throw new TestFailedException("Testers done, but there are some hashes left in feeder", 1)
              }
            }
          case HashTestSuiteResultFeederDone =>
            if (Await.result((tester ? GetHashForTest), 5 seconds).asInstanceOf[HashForTest].hash.isEmpty && (testersPosition + 1 == testers.size)) {
              testing = false
            } else {
              throw new TestFailedException("Feeder done, but there are some hashes left", 1)
            }
        }

        safeCounter -= 1
      }

      if (safeCounter <= 0) throw new TestFailedException("Safe counter limit touched!", 1)
      assert(totalHashes == ok)
    } finally {
      as.shutdown()
    }
  }
}
