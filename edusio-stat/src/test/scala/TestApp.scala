
import akka.actor.{Props, ActorSystem}
import com.edusio.tsp.database.MysqlDatabaseManager
import com.edusio.tsp.database.tasks._
import com.edusio.tsp.io.{FileJsonRead, FileJsonWrite}
import com.edusio.tsp.log.Logger
import com.edusio.tsp.statistic.monthly.checksum._
import com.edusio.tsp.statistic.monthly.generator._
import com.edusio.tsp.statistic.monthly.{MonthlyBatchSettings, MonthlyReportManagerFactory, MonthlyReportMeta}
import com.edusio.tsp.tools.DateTimeMonthMax
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}

import scala.collection.immutable.HashMap

/**
 * Created by Wlsek on 27. 10. 2015.
 */
object TestApp extends App {
  override def main(args: Array[String]) {
    /*    val formatter =  DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
        val initialDate = formatter.parseDateTime("2015-09-30 14:00:00")

        println(DateTimeMonthMax(initialDate))
        println(DateTimeMonthMax(initialDate.plusMonths(1)))
        println(DateTimeMonthMax(initialDate.plusMonths(2)))
    */
    /*
        val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
        val initialDate = formatter.parseDateTime("2015-09-30 14:00:00")
        val timeAnchor = DateTimeMonthMax(initialDate)

        val as = ActorSystem("Edusio-TSP")

        Logger.addapter = Some(as.log)

        val databaseManager = as.actorOf(
          Props(
            classOf[MysqlDatabaseManager],
            as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getString("url"),
            as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getString("dbName"),
            as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getString("userName"),
            as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getString("password"),
            as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getInt("numberOfConnections")
          )
        )

        as.actorOf(
          Props(
            classOf[MonthlyReportManagerFactory],
            databaseManager,
            MonthlyBatchSettings(
              timeAnchor,
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getInt("numberOfMonthRecalculation"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("statisticFolder"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getInt("numberOfUsersProcessing"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getInt("chunkSize"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("paidDataFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("receivedPaymentDataFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("statisticsDataFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("checksumsDataFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("paymentReleaseRequestsChecksumsFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("paymentReleaseRequestsFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("metaDataFile"),
              HashMap(
                MonthlyReportRoles.Admin -> classOf[AdminGenerator],
                MonthlyReportRoles.Affiliate -> classOf[AffiliateGenerator],
                MonthlyReportRoles.Student -> classOf[StudentGenerator],
                MonthlyReportRoles.Instructor -> classOf[InstructorGenerator],
                MonthlyReportRoles.School -> classOf[SchoolGenerator]
              ),
              HashMap(
                MonthlyReportRoles.Admin -> classOf[AdminChecksumCalculator],
                MonthlyReportRoles.Affiliate -> classOf[AffiliateChecksumCalculator],
                MonthlyReportRoles.Student -> classOf[StudentChecksumCalculator],
                MonthlyReportRoles.Instructor -> classOf[InstructorChecksumCalculator],
                MonthlyReportRoles.School -> classOf[SchoolChecksumCalculator]
              ),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getLong("monthlyRecalculationTiming"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getLong("monthlyManagerTiming"),
              as.settings.config.getConfig("edusio-tsp").getLong("complaintLimit")
            )
          )
        )*/

    var values = Array(
      "A" -> Array("A" -> Array("A", "B", "C"), "B" -> Array("A", "B", "C"), "C" -> Array("1", "2", "3")),
      "B" -> Array("A" -> Array("A", "B", "C"), "B" -> Array("A", "B", "C"), "C" -> Array("1", "2", "3"))
    )

    var defaultOffset: OffsetSetting = PrimaryAffiliateOffsetSettings(1)
    var switch = true
    var counter = 100
    while (counter > 0 && switch) {
      val value = printValuesPrimalAffiliate(defaultOffset.asInstanceOf[PrimaryAffiliateOffsetSettings], values)
      if (value.isEmpty) {
        defaultOffset.ascendOffset match {
          case OffsetSettingDone => switch = false
          case nos: OffsetSetting =>
            defaultOffset = nos
        }
      } else {
        println(value.get)
        defaultOffset = defaultOffset.increaseOffset
      }
      counter -= 1
    }

    println("counter is " + counter)

    var values2 = Array(
      "A" -> Array("A", "B", "C"),
      "B" -> Array("A", "B", "C")
    )

    defaultOffset = SecondaryAffiliateOffsetSettings(1).default
    switch = true
    counter = 100
    while (counter > 0 && switch) {
      val value = printValuesSecondaryAffiliate(defaultOffset.asInstanceOf[SecondaryAffiliateOffsetSettings], values2)
      if (value.isEmpty) {
        defaultOffset.ascendOffset match {
          case OffsetSettingDone => switch = false
          case nos: OffsetSetting =>
            defaultOffset = nos
        }
      } else {
        println(value.get)
        defaultOffset = defaultOffset.increaseOffset
      }
      counter -= 1
    }

    println("counter is " + counter)

    var values3 = Array(
      "A" -> Array("A", "B", "C"),
      "B" -> Array("A", "B", "C")
    )
    defaultOffset = SchoolOffsetSettings(1).default
    switch = true
    counter = 100
    while (counter > 0 && switch) {
      val value = printValuesSchool(defaultOffset.asInstanceOf[SchoolOffsetSettings], values3)
      if (value.isEmpty) {
        defaultOffset.ascendOffset match {
          case OffsetSettingDone => switch = false
          case nos: OffsetSetting =>
            defaultOffset = nos
        }
      } else {
        println(value.get)
        defaultOffset = defaultOffset.increaseOffset
      }
      counter -= 1
    }

    println("counter is " + counter)

    val values4 = Array("A", "B", "C")
    defaultOffset = InstructorOffsetSettings(1).default
    switch = true
    counter = 100
    while (counter > 0 && switch) {
      val value = printValuesInstructor(defaultOffset.asInstanceOf[InstructorOffsetSettings], values4)
      if (value.isEmpty) {
        defaultOffset.ascendOffset match {
          case OffsetSettingDone => switch = false
          case nos: OffsetSetting =>
            defaultOffset = nos
        }
      } else {
        println(value.get)
        defaultOffset = defaultOffset.increaseOffset
      }
      counter -= 1
    }

    println("counter is " + counter)
  }

  def printValuesPrimalAffiliate(offset: PrimaryAffiliateOffsetSettings, values: Array[Pair[String, Array[Pair[String, Array[String]]]]]): Option[String] = {
    var value: Option[String] = None
    if (values.size > offset.sAOffsetPair._2) {
      if (values(offset.sAOffsetPair._2)._2.size > offset.sAInstructorOffsetPair._2) {
        if (values(offset.sAOffsetPair._2)._2(offset.sAInstructorOffsetPair._2)._2.size > offset.sAInstructorCoursesOffsetPair._2) {
          value = Some(values(offset.sAOffsetPair._2)._1 + "-" + values(offset.sAOffsetPair._2)._2(offset.sAInstructorOffsetPair._2)._1 + "-" + values(offset.sAOffsetPair._2)._2(offset.sAInstructorOffsetPair._2)._2(offset.sAInstructorCoursesOffsetPair._2))
        }
      }
    }
    value
  }

  def printValuesSecondaryAffiliate(offset: SecondaryAffiliateOffsetSettings, values: Array[Pair[String, Array[String]]]): Option[String] = {
    var value: Option[String] = None
    if (values.size > offset.directInstructorOffsetPair._2) {
      if(values(offset.directInstructorOffsetPair._2)._2.size > offset.directInstructorCoursesOffsetPair._2) {
        value = Some(values(offset.directInstructorOffsetPair._2)._1 + "-" + values(offset.directInstructorOffsetPair._2)._2(offset.directInstructorCoursesOffsetPair._2))
      }
    }
    value
  }

  def printValuesSchool(offset: SchoolOffsetSettings, values:  Array[Pair[String, Array[String]]]): Option[String] = {
    var value: Option[String] = None
    if (values.size > offset.directInstructorOffsetPair._2) {
      if(values(offset.directInstructorOffsetPair._2)._2.size > offset.directInstructorCoursesOffsetPair._2) {
        value = Some(values(offset.directInstructorOffsetPair._2)._1 + "-" + values(offset.directInstructorOffsetPair._2)._2(offset.directInstructorCoursesOffsetPair._2))
      }
    }
    value
  }

  def printValuesInstructor(offset: InstructorOffsetSettings, values: Array[String]): Option[String] = {
    var value: Option[String] = None
    if(values.size > offset.directInstructorCoursesOffsetPair._2) {
      value = Some(values(offset.directInstructorCoursesOffsetPair._2))
    }
    value
  }
}
