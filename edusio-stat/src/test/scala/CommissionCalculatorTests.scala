import com.edusio.tsp.database.tasks.{EventForCourse, TransactionCooperation, Cooperation, NotCompletedTransaction}
import com.edusio.tsp.tools.{CommissionCalculatorCommissionBoundTo, CommissionCalculatorCommissionRelation, CommissionCalculator}
import org.joda.time.DateTime
import org.scalatest.FlatSpec
import org.scalatest._
import org.scalatest.matchers.Matchers

import scala.collection.immutable.HashMap

/**
 * Created by Wlsek on 21. 8. 2015.
 */
class CommissionCalculatorTests extends FlatSpec with Matchers {
  "CommissionCalculator" should "split commission without substitution and cooperation in right way." in {
    //transaction: NotCompletedTransaction, instructorId: Int, cooperations: List[Cooperation], eventsWithSubstitution: HashMap[EventForCourse, Option[Int]]

    val transaction = NotCompletedTransaction(10, 2020, 9821, 0L, "completed", 48)
    val cooperations = List(
      Cooperation(25, 0, 0.25f, TransactionCooperation.EDUSIO.toString)
    )

    val commissions = CommissionCalculator(
      transaction,
      25,
      cooperations,
      HashMap(),
      0.2f
    )

    assert(commissions.size === 3)
    val edusioCommission = commissions.find(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO)
    assert(edusioCommission.isEmpty === false)
    edusioCommission.map(ec => {
      assert(ec.boundToId === None)
      assert(ec.commision.toDouble === 9.6f.toDouble)
      assert(ec.commisionBoundTo === "course")
      assert(ec.commissionRelation === TransactionCooperation.EDUSIO.toString)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })

    val edusioTaxesCommission = commissions.find(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO_TAXES)
    assert(edusioTaxesCommission.isEmpty === false)
    edusioTaxesCommission.map(ec => {
      assert(ec.boundToId === None)
      assert(ec.commision.toDouble === 2.4f.toDouble)
      assert(ec.commisionBoundTo === "course")
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.EDUSIO_TAXES.toString)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })

    val instructorCommission = commissions.find(c => c.userId == 25)
    assert(instructorCommission.isEmpty === false)
    instructorCommission.map(ec => {
      assert(ec.boundToId === None)
      assert(ec.commision === 36)
      assert(ec.commisionBoundTo === "course")
      assert(ec.commissionRelation === "instructing")
      assert(ec.courseId === 9821)
      assert(ec.userId === 25)
      assert(ec.fromUser === 25)
      assert(ec.transactionId === transaction.transactionId)
    })


  }

  "CommissionCalculator" should "split fixed commission in right way." in {
    val transaction = NotCompletedTransaction(10, 2020, 9821, 0L, "completed", 60)
    val cooperations = List(
      Cooperation(25, 0, 0.25f, CommissionCalculatorCommissionRelation.EDUSIO),
      Cooperation(25, 26, 20, CommissionCalculatorCommissionRelation.SCHOOL_FIXED)
    )

    val commissions = CommissionCalculator(
      transaction,
      25,
      cooperations,
      HashMap(),
      0.2f
    )

    assert(commissions.size === 4)
    val edusioCommission = commissions.find(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO)
    assert(edusioCommission.isEmpty === false)
    edusioCommission.map(ec => {
      assert(ec.boundToId === None)
      assert(ec.commision.toDouble === 12.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.COURSE)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.EDUSIO)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })

    val edusioTaxesCommission = commissions.find(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO_TAXES)
    assert(edusioTaxesCommission.isEmpty === false)
    edusioTaxesCommission.map(ec => {
      assert(ec.boundToId === None)
      assert(ec.commision.toDouble === 3.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.COURSE)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.EDUSIO_TAXES)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })

    val schoolCommission = commissions.find(c => c.userId == 26)
    assert(schoolCommission.isEmpty === false)
    schoolCommission.map(ec => {
      assert(ec.boundToId === None)
      assert(ec.commision === 20)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.COURSE)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.SCHOOL_FIXED)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 26)
      assert(ec.transactionId === transaction.transactionId)
    })

    val instructorCommission = commissions.find(c => c.userId == 25)
    assert(instructorCommission.isEmpty === false)
    instructorCommission.map(ec => {
      assert(ec.boundToId === None)
      assert(ec.commision === 25)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.COURSE)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.INSTRUCTING)
      assert(ec.courseId === 9821)
      assert(ec.userId === 25)
      assert(ec.fromUser === 25)
      assert(ec.transactionId === transaction.transactionId)
    })

  }

  "CommissionCalculator" should "split commission with substitution and cooperation with school in right way." in {
    val transaction = NotCompletedTransaction(10, 2020, 9821, 0L, "completed", 48)
    val cooperations = List(
      Cooperation(25, 0, 0.25f, CommissionCalculatorCommissionRelation.EDUSIO),
      Cooperation(25, 26, 0.25f, CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE)
    )

    val commissions = CommissionCalculator(
      transaction,
      25,
      cooperations,
      HashMap(
        EventForCourse(11, DateTime.now().plusDays(1), DateTime.now().plusDays(2)) -> None,
        EventForCourse(12, DateTime.now().plusDays(3), DateTime.now().plusDays(4)) -> None,
        EventForCourse(13, DateTime.now().plusDays(5), DateTime.now().plusDays(6)) -> Some(29)
      ),
      0.2f
    )

    assert(commissions.size === 12)

    val edusioCommission = commissions.filter(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO)
    assert(edusioCommission.size === 3)

    var edusioEventsHasToAppear = List(11, 12, 13)
    edusioCommission.map(ec => {
      edusioEventsHasToAppear = edusioEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision.toDouble === 3.2f.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.EDUSIO)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })

    var edusioTaxesEventsHasToAppear = List(11, 12, 13)

    val edusioTaxesCommission = commissions.filter(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO_TAXES)
    edusioTaxesCommission.map(ec => {
      edusioTaxesEventsHasToAppear = edusioTaxesEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision.toDouble === 0.8f.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.EDUSIO_TAXES)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(edusioTaxesEventsHasToAppear.isEmpty === true)

    val schoolCommission = commissions.filter(c => c.userId == 26)
    assert(schoolCommission.size === 3)


    var schoolEventsHasToAppear = List(11, 12, 13)
    schoolCommission.map(ec => {
      schoolEventsHasToAppear = schoolEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision === 3)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 26)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(schoolEventsHasToAppear.isEmpty === true)

    val instructorCommission = commissions.filter(c => c.commissionRelation match {
      case CommissionCalculatorCommissionRelation.INSTRUCTING => true
      case t => false
    })
    assert(instructorCommission.size === 2)

    var instructionEventsHasToAppear = List(11, 12)
    instructorCommission.map(ec => {
      instructionEventsHasToAppear = instructionEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision === 9)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.INSTRUCTING)
      assert(ec.courseId === 9821)
      assert(ec.userId === 25)
      assert(ec.fromUser === 25)
      assert(ec.transactionId === transaction.transactionId)
    })

    val substitutionCommission = commissions.find(c => c.commissionRelation match {
      case CommissionCalculatorCommissionRelation.SUBSTITUTION => true
      case t => false
    })
    assert(substitutionCommission.size === 1)
    substitutionCommission.map(ec => {
      assert(ec.boundToId.get ===
        13)
      assert(ec.commision === 9)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.SUBSTITUTION)
      assert(ec.courseId === 9821)
      assert(ec.userId === 29)
      assert(ec.fromUser === 25)
      assert(ec.transactionId === transaction.transactionId)
    })

  }


  "CommissionCalculator" should "split commission with substitution and cooperation with school and with referral in right way." in {
    val transaction = NotCompletedTransaction(10, 2020, 9821, 0L, "completed", 48)
    val cooperations = List(
      Cooperation(25, 0, 0.25f, CommissionCalculatorCommissionRelation.EDUSIO),
      Cooperation(25, 31, 0.05f, CommissionCalculatorCommissionRelation.REFERRAL),
      Cooperation(25, 26, 0.25f, CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE)
    )

    val commissions = CommissionCalculator(
      transaction,
      25,
      cooperations,
      HashMap(
        EventForCourse(11, DateTime.now().plusDays(1), DateTime.now().plusDays(2)) -> None,
        EventForCourse(12, DateTime.now().plusDays(3), DateTime.now().plusDays(4)) -> None,
        EventForCourse(13, DateTime.now().plusDays(5), DateTime.now().plusDays(6)) -> Some(29)
      ),
      0.2f
    )

    assert(commissions.size === 15)

    val edusioCommission = commissions.filter(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO)
    assert(edusioCommission.size === 3)

    var edusioEventsHasToAppear = List(11, 12, 13)
    edusioCommission.map(ec => {
      edusioEventsHasToAppear = edusioEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision.toDouble === 2.4f.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.EDUSIO)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(edusioEventsHasToAppear.isEmpty === true)

    val edusioTaxesCommission = commissions.filter(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO_TAXES)
    assert(edusioTaxesCommission.size === 3)

    var edusioTaxesEventsHasToAppear = List(11, 12, 13)
    edusioTaxesCommission.map(ec => {
      edusioTaxesEventsHasToAppear = edusioTaxesEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision.toDouble === 0.8f.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.EDUSIO_TAXES)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(edusioTaxesEventsHasToAppear.isEmpty === true)

    val referralCommission = commissions.filter(c => c.commissionRelation == CommissionCalculatorCommissionRelation.REFERRAL)
    assert(referralCommission.size === 3)

    var referralEventsHasToAppear = List(11, 12, 13)
    referralCommission.map(ec => {
      referralEventsHasToAppear = referralEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision === 0.8f)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.REFERRAL)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 31)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(referralEventsHasToAppear.isEmpty === true)

    val schoolCommission = commissions.filter(c => c.userId == 26)
    assert(schoolCommission.size === 3)


    var schoolEventsHasToAppear = List(11, 12, 13)
    schoolCommission.map(ec => {
      schoolEventsHasToAppear = schoolEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision === 3)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 26)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(schoolEventsHasToAppear.isEmpty === true)

    val instructorCommission = commissions.filter(c => c.commissionRelation match {
      case CommissionCalculatorCommissionRelation.INSTRUCTING => true
      case t => false
    })
    assert(instructorCommission.size === 2)

    var instructionEventsHasToAppear = List(11, 12)
    instructorCommission.map(ec => {
      instructionEventsHasToAppear = instructionEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision === 9)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.INSTRUCTING)
      assert(ec.courseId === 9821)
      assert(ec.userId === 25)
      assert(ec.fromUser === 25)
      assert(ec.transactionId === transaction.transactionId)
    })

    val substitutionCommission = commissions.find(c => c.commissionRelation match {
      case CommissionCalculatorCommissionRelation.SUBSTITUTION => true
      case t => false
    })
    assert(substitutionCommission.size === 1)
    substitutionCommission.map(ec => {
      assert(ec.boundToId.get ===
        13)
      assert(ec.commision === 9)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.SUBSTITUTION)
      assert(ec.courseId === 9821)
      assert(ec.userId === 29)
      assert(ec.fromUser === 25)
      assert(ec.transactionId === transaction.transactionId)
    })

  }


  "CommissionCalculator" should "split commission with substitution and cooperation with school and with referral and subreferral in right way." in {
    val transaction = NotCompletedTransaction(10, 2020, 9821, 0L, "completed", 48)
    val cooperations = List(
      Cooperation(25, 0, 0.25f, CommissionCalculatorCommissionRelation.EDUSIO),
      Cooperation(25, 31, 0.025f, CommissionCalculatorCommissionRelation.REFERRAL),
      Cooperation(31, 32, 0.05f, CommissionCalculatorCommissionRelation.REFERRAL),
      Cooperation(25, 26, 0.25f, CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE)
    )

    val commissions = CommissionCalculator(
      transaction,
      25,
      cooperations,
      HashMap(
        EventForCourse(11, DateTime.now().plusDays(1), DateTime.now().plusDays(2)) -> None,
        EventForCourse(12, DateTime.now().plusDays(3), DateTime.now().plusDays(4)) -> None,
        EventForCourse(13, DateTime.now().plusDays(5), DateTime.now().plusDays(6)) -> Some(29)
      ),
      0.2f
    )

    assert(commissions.size === 18)

    val edusioCommission = commissions.filter(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO)
    assert(edusioCommission.size === 3)

    var edusioEventsHasToAppear = List(11, 12, 13)
    edusioCommission.map(ec => {
      edusioEventsHasToAppear = edusioEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision.toDouble === 2.0f.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.EDUSIO)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(edusioEventsHasToAppear.isEmpty === true)

    val edusioTaxesCommission = commissions.filter(c => c.userId == 0 && c.commissionRelation == CommissionCalculatorCommissionRelation.EDUSIO_TAXES)
    assert(edusioTaxesCommission.size === 3)

    var edusioTaxesEventsHasToAppear = List(11, 12, 13)
    edusioTaxesCommission.map(ec => {
      edusioTaxesEventsHasToAppear = edusioTaxesEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision.toDouble === 0.8f.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.EDUSIO_TAXES)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 0)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(edusioTaxesEventsHasToAppear.isEmpty === true)

    val referralCommission = commissions.filter(c => c.commissionRelation == CommissionCalculatorCommissionRelation.REFERRAL && c.fromUser == 25 && c.userId == 31)
    assert(referralCommission.size === 3)

    var referralEventsHasToAppear = List(11, 12, 13)
    referralCommission.map(ec => {
      referralEventsHasToAppear = referralEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision.toDouble === 0.4f.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.REFERRAL)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 31)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(referralEventsHasToAppear.isEmpty === true)


    val referralSecondaryCommission = commissions.filter(c => c.commissionRelation == CommissionCalculatorCommissionRelation.REFERRAL && c.fromUser == 31 && c.userId == 32)
    assert(referralCommission.size === 3)

    var referralSecondaryEventsHasToAppear = List(11, 12, 13)
    referralSecondaryCommission.map(ec => {
      referralSecondaryEventsHasToAppear = referralSecondaryEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision === 0.8f.toDouble)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.REFERRAL)
      assert(ec.courseId === 9821)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(referralSecondaryEventsHasToAppear.isEmpty === true)

    val schoolCommission = commissions.filter(c => c.userId == 26)
    assert(schoolCommission.size === 3)


    var schoolEventsHasToAppear = List(11, 12, 13)
    schoolCommission.map(ec => {
      schoolEventsHasToAppear = schoolEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision === 3)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE)
      assert(ec.courseId === 9821)
      assert(ec.fromUser === 25)
      assert(ec.userId === 26)
      assert(ec.transactionId === transaction.transactionId)
    })
    assert(schoolEventsHasToAppear.isEmpty === true)

    val instructorCommission = commissions.filter(c => c.commissionRelation match {
      case CommissionCalculatorCommissionRelation.INSTRUCTING => true
      case t => false
    })
    assert(instructorCommission.size === 2)

    var instructionEventsHasToAppear = List(11, 12)
    instructorCommission.map(ec => {
      instructionEventsHasToAppear = instructionEventsHasToAppear.filterNot(f => f == ec.boundToId.get)
      assert(ec.commision === 9)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.INSTRUCTING)
      assert(ec.courseId === 9821)
      assert(ec.userId === 25)
      assert(ec.fromUser === 25)
      assert(ec.transactionId === transaction.transactionId)
    })

    val substitutionCommission = commissions.find(c => c.commissionRelation match {
      case CommissionCalculatorCommissionRelation.SUBSTITUTION => true
      case t => false
    })
    assert(substitutionCommission.size === 1)
    substitutionCommission.map(ec => {
      assert(ec.boundToId.get ===
        13)
      assert(ec.commision === 9)
      assert(ec.commisionBoundTo === CommissionCalculatorCommissionBoundTo.EVENT)
      assert(ec.commissionRelation === CommissionCalculatorCommissionRelation.SUBSTITUTION)
      assert(ec.courseId === 9821)
      assert(ec.userId === 29)
      assert(ec.fromUser === 25)
      assert(ec.transactionId === transaction.transactionId)
    })
  }

}
