import com.edusio.tsp.database.tasks.MonthlyReportRoles
import com.edusio.tsp.database.tasks.monthly.MetaUserInformations
import com.edusio.tsp.io.{FileJsonRead, FileJsonWrite}
import com.edusio.tsp.statistic.monthly.MonthlyReportMeta
import org.joda.time.DateTime
import org.scalatest.FlatSpec

/**
 * Created by Wlsek on 27. 10. 2015.
 */
class IOTests  extends FlatSpec {
  "FileJsonWrite and FileJsonRead" should "properly write and read MonthlyReportMeta." in {
    val time = new DateTime()

    val value =   MonthlyReportMeta(
      1, "novakd", "Dan Novák", MetaUserInformations(None), List(MonthlyReportRoles.Admin, MonthlyReportRoles.Affiliate, MonthlyReportRoles.Student, MonthlyReportRoles.Instructor, MonthlyReportRoles.School), time, time, 500L, "", "", ""
    )

    FileJsonWrite("test_meta.json", value serialize)
    val read = FileJsonRead("test_meta.json")
    assert(read.isDefined)
    read.map(v =>
      assert((value deserialize v) compare value)
    )
  }
}