package com.edusio.tsp.payment

import akka.actor.{ActorLogging, Props, Actor, ActorRef}
import com.edusio.tsp.database.DatabaseTaskError
import com.edusio.tsp.database.tasks._
import com.edusio.tsp.statistic.EnqueueUser
import org.joda.time.DateTime
import scala.concurrent.duration._
import com.edusio.tsp.tools.ActorWithTimeoutSupport

/**
 * Created by Wlsek on 8. 5. 2015.
 */

case class PaymentRequestProcessed(payment: PaymentRequest)

case class CannotProcessPaymentRequest(payment: PaymentRequest)

class PaymentManager(databaseManager: ActorRef, statisticManager: ActorRef, timeout: Long, chunkSize: Int) extends ActorWithTimeoutSupport with ActorLogging {
  private var requests: List[PaymentRequest] = List()
  log.debug("Payment manager started, querying payment requests.")
  databaseManager ! QueryAllUnprocessedPaymentRequests

  def receive = {
    case r: PaymentRequests =>
      log.debug("Payments requests come, i have to proceed with " + r.requests.size + " requests")
      requests = r.requests
      requests.map(r => {
        context.actorOf(Props(classOf[PaymentRequestProcessor], databaseManager, self, r, chunkSize))
      })
      process

    case pr: PaymentRequestProcessed =>
      log.debug("Payment request processed " + pr)
      context.stop(sender)
      requests = requests.filter(f => f != pr.payment)
      statisticManager ! EnqueueUser(pr.payment.userId)
      process

    case cp: CannotProcessPaymentRequest =>
      log.error("Cannot process payment! " + cp)
      context.stop(sender)
      requests = requests.filter(f => f != cp.payment)
      process

    case DatabaseTaskError =>
      log.error("Database error come to PaymentManager!")
      process
  }

  def process = if (requests.size == 0) {
    startTimeout(timeout millis)
  }

  def onTimeout = {
    databaseManager ! QueryAllUnprocessedPaymentRequests
  }
}

case class TransactionCommissionsProcessed(amount: Float)

case object CannotProcessTransactionCommissions

case class FixedTransactionCommissionsProcessed(amount: Float)

case object CannotProcessFixedTransactionCommissions

class PaymentRequestProcessor(databaseManager: ActorRef, parent: ActorRef, request: PaymentRequest, chunkSize: Int) extends Actor  with ActorLogging {
  log.debug("PaymentRequestProcessor started for request: " + request + ", starting with reset temporary data.")
  private var amount: Float = 0
  val timeStarted = new DateTime()
  databaseManager ! QueryResetPaymentRequest(request)

  def receive = {
    case r: ResetPaymentRequestCompleted =>
      log.debug("PaymentRequestProcessor - Reset complete. Asking for TransactionCommissionProcessor")
      context.actorOf(Props(classOf[TransactionCommissionProcessor], databaseManager, self, request, chunkSize))
    case tcp: TransactionCommissionsProcessed =>
      log.debug("TransactionCommissionProcessor is done. Asking for FixedTransactionCommissionProcessor")
      amount += tcp.amount
      context.stop(sender)
      context.actorOf(Props(classOf[FixedTransactionCommissionProcessor], databaseManager, self, request, chunkSize))
    case ftcp: FixedTransactionCommissionsProcessed =>
      log.debug("FixedTransactionCommissionProcessor is done. Asking for finalizing payment request with: " + amount)
      amount += ftcp.amount
      context.stop(sender)
      databaseManager ! QueryFinalizePaymentRequest(request, new DateTime().getMillis - timeStarted.getMillis, amount)
    case PaymetRequestFinalized =>
      log.debug("Finalizing payment is done. Sending information to parent.")
      parent ! PaymentRequestProcessed(request)
    case DatabaseTaskError =>
      log.debug("DatabaseTaskError. Sending information to parent.")
      context.stop(sender)
      parent ! CannotProcessPaymentRequest(request)
    case CannotProcessTransactionCommissions =>
      log.debug("Cannot process transaction commissions. Sending information to parent.")
      context.stop(sender)
      parent ! CannotProcessPaymentRequest(request)
    case CannotProcessFixedTransactionCommissions =>
      log.debug("Cannot process fixed transaction commissions. Sending information to parent.")
      context.stop(sender)
      parent ! CannotProcessPaymentRequest(request)
  }
}

class TransactionCommissionProcessor(databaseManager: ActorRef, parent: ActorRef, request: PaymentRequest, chunkSize: Int) extends Actor with ActorLogging {
  log.debug("TransactionCommissionProcessor started, querying for TransactionCommissions.")
  private var processingTransactions: List[TransactionCommission] = List()
  private var transactionToWriteIntoDb: List[TransactionCommission] = List()
  private var actualTransactionOffset = 0
  private var amount: Float = 0

  databaseManager ! QueryTransactionCommissions(request, actualTransactionOffset, chunkSize)

  def receive = {
    case t: TransactionCommissions =>
      log.debug("TransactionCommissions come, processing them.")
      processingTransactions = t.transactions
      processPaidTransactions

    case t: AlreadyPaidTransactionCommissions =>
      log.debug("Filtering already paid transactions. {}", t.transactions)
      t.transactions.map(t => {
        processingTransactions = processingTransactions.filter(f => f.id != t.id)
      })
      transactionToWriteIntoDb = processingTransactions
      processingTransactions = List()
      transactionToWriteIntoDb.map(t => {
        amount += t.commision
        log.debug("Connecting commission and payment request.")
        databaseManager ! QueryInsertTransactionCommissionToPayment(t, request)
      })
      processInsertIntoPayment

    case t: InsertedTransactionCommissionToPayment =>
      log.debug("Inserted transaction commission to payment done.")
      transactionToWriteIntoDb = transactionToWriteIntoDb.filter(ti => ti.id != t.transaction.id)
      processInsertIntoPayment

    case DatabaseTaskError =>
      log.debug("There was database error, sending to parent.")
      parent ! CannotProcessTransactionCommissions
  }

  def processPaidTransactions = {
    if(processingTransactions.size > 0) {
      log.debug("There are transactions {}, query if they are not already in other request.", processingTransactions)
      databaseManager ! QueryAlreadyPaidTransactionCommissions(processingTransactions)
    } else {
      log.debug("No transactions to query for their usage, sending to parent.")
      parent ! TransactionCommissionsProcessed(amount)
    }
  }

  def processInsertIntoPayment = if(transactionToWriteIntoDb.size == 0) {
      log.debug("Increasing offset and query another commissions.")
      actualTransactionOffset += chunkSize
      databaseManager ! QueryTransactionCommissions(request, actualTransactionOffset, chunkSize)
  }
}

class FixedTransactionCommissionProcessor(databaseManager: ActorRef, parent: ActorRef, request: PaymentRequest, chunkSize: Int) extends Actor with ActorLogging {
  log.debug("FixedTransactionCommissionProcessor started, querying for TransactionCommissions.")
  private var processingFixedTransactions: List[FixedTransactionCommission] = List()
  private var fixedTransactionToWriteIntoDb: List[FixedTransactionCommission] = List()
  private var actualFixedTransactionOffset = 0
  private var amount: Float = 0

  databaseManager ! QueryFixedTransactionCommissions(request, actualFixedTransactionOffset, chunkSize)

  def receive = {
    case t: FixedTransactionCommissions =>
      log.debug("FixedTransactionCommissions come, processing them.")
      processingFixedTransactions = t.transactions
      processPaidTransactions

    case t: AlreadyPaidFixedTransactionCommissions =>
      log.debug("Filtering already paid transactions.")
      t.transactions.map(t => {
        processingFixedTransactions = processingFixedTransactions.filter(f => f.id != t.id)
      })
      fixedTransactionToWriteIntoDb = processingFixedTransactions
      processingFixedTransactions = List()
      fixedTransactionToWriteIntoDb.map(t => {
        amount += t.commission
        log.debug("Connecting commission and payment request.")
        databaseManager ! QueryInsertFixedTransactionCommissionToPayment(t, request)
      })
      processInsertIntoPayment

    case t: InsertedFixedTransactionCommissionToPayment =>
      log.debug("Inserted fixed transaction commission to payment done.")
      fixedTransactionToWriteIntoDb = fixedTransactionToWriteIntoDb.filter(ti => ti.id != t.transaction.id)
      processInsertIntoPayment

    case DatabaseTaskError =>
      log.debug("There was database error, sending to parent.")
      parent ! CannotProcessFixedTransactionCommissions
  }

  def processPaidTransactions = {
    if(processingFixedTransactions.size > 0) {
      log.debug("There are fixed transactions, query if they are not already in other request.")
      databaseManager ! QueryAlreadyPaidFixedTransactionCommissions(processingFixedTransactions)
    } else {
      log.debug("No fixed transactions to query for their usage, sending to parent.")
      parent ! FixedTransactionCommissionsProcessed(amount)
    }
  }

  def processInsertIntoPayment = if(fixedTransactionToWriteIntoDb.size == 0) {
    log.debug("Increasing offset and query another fixed commissions.")
    actualFixedTransactionOffset += chunkSize
    databaseManager ! QueryFixedTransactionCommissions(request, actualFixedTransactionOffset, chunkSize)
  }
}