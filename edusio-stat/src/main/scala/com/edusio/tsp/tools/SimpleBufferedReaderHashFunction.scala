package com.edusio.tsp.tools

import java.io.BufferedReader

/**
 * Created by Wlsek on 2. 11. 2015.
 */
object SimpleBufferedReaderHashFunction {
  def apply(buffer: Option[BufferedReader]) = {
    () => {
      buffer.map(br => {
        val value = br.readLine()
        if(value != null) {
          Some(value)
        } else {
          None
        }
      }).getOrElse(None)
    }
  }
}
