package com.edusio.tsp.tools

import akka.actor.SupervisorStrategy.{Restart, Stop}
import akka.actor._

import scala.concurrent.duration.Duration

/**
 * Created by Wlsek on 5. 5. 2015.
 */

class NoveitExpectedTimeoutException extends java.util.concurrent.TimeoutException

trait ActorWithTimeoutSupport extends Actor {
  private var timeouter: Option[ActorRef] = None

  def startTimeout(timeout: Duration) = {
    timeouter.map(t => context.stop(t))
    timeouter = Some(context.actorOf(Props(classOf[TimeOutActor], timeout)))
  }
  def stopTimeout = {
    timeouter.map(t => context.stop(t))
  }

  def onTimeout


  override val supervisorStrategy : SupervisorStrategy = {
    OneForOneStrategy(){
      case e: NoveitExpectedTimeoutException =>
        onTimeout
        Stop
      case e: Exception =>
        Restart
      case t =>
        Restart
    }
  }
}

class TimeOutActor(timeout: Duration) extends Actor {

  context.setReceiveTimeout(timeout)

  def receive = {
    case ReceiveTimeout =>
      throw new NoveitExpectedTimeoutException()
  }
}
