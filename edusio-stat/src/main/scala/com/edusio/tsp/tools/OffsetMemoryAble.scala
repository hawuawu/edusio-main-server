package com.edusio.tsp.tools

/**
 * Created by Wlsek on 3. 11. 2015.
 */
trait OffsetMemoryAble {
  trait GeneratorValuesOffsetType

  var offsets: List[Pair[GeneratorValuesOffsetType, Int]] = List()

  def resetOffset(offsetType: GeneratorValuesOffsetType): Unit = {
    offsets = offsetType -> 0 :: offsets.filterNot(off => off._1 match {
      case `offsetType` => true
      case t => false
    })
  }

  def increaseOffset(offsetType: GeneratorValuesOffsetType, value: Int): Unit = {
    offsets = offsets.find(off => off._1 match {
      case `offsetType` => true
      case t => false
    }).map(off => off.copy(_2 = off._2 + value))
      .getOrElse(offsetType -> value) :: offsets.filterNot(off => off._1 match {
      case `offsetType` => true
      case t => false
    })
  }

  def offset(offsetType: GeneratorValuesOffsetType): Int = {
    offsets.find(off => {
      off._1 match {
        case `offsetType` => true
        case t => false
      }
    }).map(f => f._2).getOrElse(Int.MaxValue)
  }
}
