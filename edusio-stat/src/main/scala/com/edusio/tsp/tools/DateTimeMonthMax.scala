package com.edusio.tsp.tools

import org.joda.time.DateTime

/**
 * Created by Wlsek on 30. 10. 2015.
 */
object DateTimeMonthMax {
  def apply(date: DateTime): DateTime = {
    date.millisOfDay().withMaximumValue().dayOfMonth().withMaximumValue()
  }
}
