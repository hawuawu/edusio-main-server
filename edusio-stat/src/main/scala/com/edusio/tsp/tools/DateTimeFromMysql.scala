package com.edusio.tsp.tools

import akka.event.LoggingAdapter
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 29. 10. 2015.
 */
object DateTimeFromMysql {
  def apply(mysql: String, logger: Option[LoggingAdapter] = None): Option[DateTime] ={

    try {
      val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S")
      Some(formatter.parseDateTime(mysql))
    } catch {
      case i: IllegalArgumentException =>
        try {
          logger.map(log => log.error("Cannot parse time trying another format."  + i.toString))
          val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
          Some(formatter.parseDateTime(mysql))
        } catch {
          case i2: IllegalArgumentException =>
            logger.map(log => log.error("Cannot parse time trying another format."  + i2.toString))
            None
        }
    }
  }
}
