package com.edusio.tsp.tools

import com.edusio.tsp.database.tasks._

import scala.collection.immutable.HashMap

/**
 * Created by Wlsek on 20. 8. 2015.
 */

object CommissionCalculatorCommissionRelation {
  val EDUSIO = TransactionCooperation.EDUSIO.toString
  val EDUSIO_TAXES = "edusio_taxes"
  val REFERRAL = TransactionCooperation.REFERRAL.toString
  val SCHOOL_PERCENTAGE = TransactionCooperation.SCHOOL_PERCENTAGE.toString
  val SCHOOL_FIXED = TransactionCooperation.SCHOOL_FIXED.toString
  val INSTRUCTING = "instructing"
  val SUBSTITUTION = "substitution"
}

object CommissionCalculatorCommissionBoundTo {
  val COURSE = "course"
  val EVENT = "event"
}

object CommissionCalculator {
  def apply(transaction: NotCompletedTransaction, instructorId: Int, cooperations: List[Cooperation], eventsWithSubstitution: HashMap[EventForCourse, Option[Int]], taxesRatio: Float): List[Commision] = {
    var commissions: List[Commision] = List()

    val edusio = cooperations.find(c => c.cType == CommissionCalculatorCommissionRelation.EDUSIO).get
    val school = cooperations.find(c => c.cType match {
      case CommissionCalculatorCommissionRelation.SCHOOL_FIXED => true
      case CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE => true
      case t => false
    })

    val primaryReferral = cooperations.find(c => c.cType == CommissionCalculatorCommissionRelation.REFERRAL && c.user == instructorId)
    val secondaryReferral = cooperations.find(c => {
      c.cType == CommissionCalculatorCommissionRelation.REFERRAL && primaryReferral.map(pr => pr.withUser == c.user).getOrElse(false)
    })

    var edusioShare = transaction.value * edusio.commission
    var taxes = edusioShare * taxesRatio
    edusioShare = edusioShare - taxes

    var instructorShare = transaction.value - edusioShare - taxes

    var totalReferralCommission = transaction.value * (primaryReferral.map(pr => pr.commission).getOrElse(0f) + secondaryReferral.map(sr => sr.commission).getOrElse(0f))

    secondaryReferral.map(r => {
      val value = transaction.value * secondaryReferral.map(sr => sr.commission).getOrElse(0f)
      commissions = Commision(transaction.courseId, r.withUser, transaction.transactionId, r.user, value, CommissionCalculatorCommissionBoundTo.COURSE, None, TransactionCooperation.REFERRAL.toString) :: commissions
      edusioShare -= value
      totalReferralCommission -= value
    })

    primaryReferral.map(r => {
      commissions = Commision(transaction.courseId, r.withUser, transaction.transactionId, r.user, totalReferralCommission, CommissionCalculatorCommissionBoundTo.COURSE, None, TransactionCooperation.REFERRAL.toString) :: commissions
      edusioShare -= totalReferralCommission
    })

    if (edusioShare > 0) {
      commissions = Commision(transaction.courseId, 0, transaction.transactionId, instructorId, edusioShare, CommissionCalculatorCommissionBoundTo.COURSE, None, TransactionCooperation.EDUSIO.toString) :: commissions
      commissions = Commision(transaction.courseId, 0, transaction.transactionId, instructorId, taxes, CommissionCalculatorCommissionBoundTo.COURSE, None, CommissionCalculatorCommissionRelation.EDUSIO_TAXES.toString) :: commissions
    } else if (edusioShare < 0) {
      instructorShare -= edusioShare
    }

    school.map(s => {
      s.cType match {
        case CommissionCalculatorCommissionRelation.SCHOOL_FIXED =>
          instructorShare -= s.commission
          commissions = Commision(transaction.courseId, s.withUser, transaction.transactionId, instructorId, s.commission, CommissionCalculatorCommissionBoundTo.COURSE, None, CommissionCalculatorCommissionRelation.SCHOOL_FIXED) :: commissions

        case CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE =>
          commissions = Commision(transaction.courseId, s.withUser, transaction.transactionId, instructorId, s.commission * instructorShare, CommissionCalculatorCommissionBoundTo.COURSE, None, CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE) :: commissions
          instructorShare -= instructorShare * s.commission
      }
    })

    if (instructorShare > 0) {
      commissions = Commision(transaction.courseId, instructorId, transaction.transactionId, instructorId, instructorShare, CommissionCalculatorCommissionBoundTo.COURSE, None, CommissionCalculatorCommissionRelation.INSTRUCTING) :: commissions
    }


    if (eventsWithSubstitution.size > 0) {
      commissions.map(c => {
        c.commissionRelation match {
          case CommissionCalculatorCommissionRelation.EDUSIO =>
            eventsWithSubstitution.map(e => {
              Commision(c.courseId, c.userId, c.transactionId, c.fromUser, c.commision / eventsWithSubstitution.size, CommissionCalculatorCommissionBoundTo.EVENT, Some(e._1.id), c.commissionRelation)
            })

          case CommissionCalculatorCommissionRelation.EDUSIO_TAXES =>
            eventsWithSubstitution.map(e => {
              Commision(c.courseId, c.userId, c.transactionId, c.fromUser, c.commision / eventsWithSubstitution.size, CommissionCalculatorCommissionBoundTo.EVENT, Some(e._1.id), c.commissionRelation)
            })
/*
          case CommissionCalculatorCommissionRelation.REFERRAL =>
            eventsWithSubstitution.map(e => {
              Commision(c.courseId, c.userId, c.transactionId, c.fromUser, c.commision / eventsWithSubstitution.size, CommissionCalculatorCommissionBoundTo.EVENT, Some(e._1.id), c.commissionRelation)
            })*/

          case CommissionCalculatorCommissionRelation.SCHOOL_FIXED =>
            eventsWithSubstitution.map(e => {
              Commision(c.courseId, c.userId, c.transactionId, c.fromUser, c.commision / eventsWithSubstitution.size, CommissionCalculatorCommissionBoundTo.EVENT, Some(e._1.id), c.commissionRelation)
            })

          case CommissionCalculatorCommissionRelation.SCHOOL_PERCENTAGE =>
            eventsWithSubstitution.map(e => {
              Commision(c.courseId, c.userId, c.transactionId, c.fromUser, c.commision / eventsWithSubstitution.size, CommissionCalculatorCommissionBoundTo.EVENT, Some(e._1.id), c.commissionRelation)
            })

          case CommissionCalculatorCommissionRelation.INSTRUCTING =>
            eventsWithSubstitution.map(e => {
              Commision(c.courseId, e._2.getOrElse(instructorId), c.transactionId, c.fromUser, c.commision / eventsWithSubstitution.size, CommissionCalculatorCommissionBoundTo.EVENT, Some(e._1.id), e._2.map(_ => CommissionCalculatorCommissionRelation.SUBSTITUTION).getOrElse(CommissionCalculatorCommissionRelation.INSTRUCTING))
            })

          case CommissionCalculatorCommissionRelation.REFERRAL =>
            if(c.fromUser == instructorId) {
              eventsWithSubstitution.map(e => {
                Commision(c.courseId, c.userId, c.transactionId, c.fromUser, c.commision / eventsWithSubstitution.size, CommissionCalculatorCommissionBoundTo.EVENT, Some(e._1.id), c.commissionRelation)
              })
            } else {
              eventsWithSubstitution.map(e => {
                Commision(c.courseId, c.userId, c.transactionId, c.fromUser, c.commision / eventsWithSubstitution.size, CommissionCalculatorCommissionBoundTo.EVENT, Some(e._1.id), c.commissionRelation)
              })
            }
        }
      }).flatten
    } else {
      commissions
    }
  }
}
