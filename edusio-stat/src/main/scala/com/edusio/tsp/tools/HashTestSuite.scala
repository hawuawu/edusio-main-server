package com.edusio.tsp.tools

/**
 * Created by Wlsek on 2. 11. 2015.
 */

trait HashTestSuiteResult
case object HashTestSuiteResultSame extends HashTestSuiteResult
case object HashTestSuiteResultDiffer extends HashTestSuiteResult
case object HashTestSuiteResultTestNext extends HashTestSuiteResult
case object HashTestSuiteResultFeederDone extends HashTestSuiteResult

class HashTestSuite(
                   val feedFunction: () => Option[String],
                   val peekFunction: Option[() => Boolean] = None,
                   val resetFunction: Option[() => Unit] = None
                     ) {
  var lastFedValue: Option[String] = None

  def apply(hashToTest: Option[String]): HashTestSuiteResult = {
    hashToTest.map(h => {
      lastFedValue = feedFunction()
        lastFedValue.map(hash => {
        if( hash == h) {
          HashTestSuiteResultSame
        }else {
          HashTestSuiteResultDiffer
        }
      }).getOrElse(HashTestSuiteResultFeederDone)
    }).getOrElse({
      HashTestSuiteResultTestNext
    })
  }

  def isEmptyInFuture_?(): Boolean = {
    val result = peekFunction.map(f => f()).getOrElse(feedFunction().isEmpty)
    resetFunction.map(f => f())
    result
  }
}
