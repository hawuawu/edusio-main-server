package com.edusio.tsp

import akka.actor.{Props, ActorSystem}
import com.edusio.tsp.database.MysqlDatabaseManager
import com.edusio.tsp.database.tasks.MonthlyReportRoles
import com.edusio.tsp.log.Logger
import com.edusio.tsp.payment.PaymentManager
import com.edusio.tsp.statistic.StatisticManager
import com.edusio.tsp.statistic.monthly.checksum._
import com.edusio.tsp.statistic.monthly.generator._
import com.edusio.tsp.statistic.monthly.{MonthlyBatchSettings, MonthlyReportManagerFactory}
import com.edusio.tsp.tools.DateTimeMonthMax
import com.edusio.tsp.transaction.{CancelCourseManager, TransactionManager, FixedCooperationTransactionManager}
import org.hawu.utils._
import org.hawu.utils.file.JarLocation
import org.joda.time.DateTime
import org.slf4j.LoggerFactory

import scala.collection.immutable.HashMap

/**
 * Created by Wlsek on 4. 6. 2015.
 */

object EntryPointService extends services.EntryPointService with JarLocation {

  private var actorSystem: Option[ActorSystem] = None
  private var retry = true
  var logger = LoggerFactory.getLogger(this.getClass)

  def start = {
    try {
      actorSystem = Some(ActorSystem("Edusio-TSP"))
      actorSystem.map(as => {
        Logger.addapter = Some(as.log)

        val url = as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getString("url")
        val dbName = as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getString("dbName")
        val userName = as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getString("userName")
        val password = as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getString("password")
        val numberOfDbConnections = as.settings.config.getConfig("edusio-tsp").getConfig("mysql").getInt("numberOfConnections")
        val timing = as.settings.config.getConfig("edusio-tsp").getLong("timing")
        val simultaneousProcessing = as.settings.config.getConfig("edusio-tsp").getInt("simultaneousProcessing")
        val complaintLimit = as.settings.config.getConfig("edusio-tsp").getLong("complaintLimit")
        val ourCommision = as.settings.config.getConfig("edusio-tsp").getDouble("ourCommision").toFloat
        val taxes = as.settings.config.getConfig("edusio-tsp").getDouble("taxesFromCommission").toFloat
        val numberOfMonthsInMonthlyStatistic = as.settings.config.getConfig("edusio-tsp").getInt("numberOfMonthsInMonthlyStatistic")
        val chunkSize = as.settings.config.getConfig("edusio-tsp").getInt("chunkSize")

        val databaseManager = as.actorOf(
          Props(
            classOf[MysqlDatabaseManager],
            url,
            dbName,
            userName,
            password,
            numberOfDbConnections
          )
        )

        val serviceDatabaseManager = as.actorOf(
          Props(
            classOf[MysqlDatabaseManager],
            as.settings.config.getConfig("edusio-tsp").getConfig("service-mysql").getString("url"),
            as.settings.config.getConfig("edusio-tsp").getConfig("service-mysql").getString("dbName"),
            as.settings.config.getConfig("edusio-tsp").getConfig("service-mysql").getString("userName"),
            as.settings.config.getConfig("edusio-tsp").getConfig("service-mysql").getString("password"),
            as.settings.config.getConfig("edusio-tsp").getConfig("service-mysql").getInt("numberOfConnections")
          )
        )

        val statisticManager = as.actorOf(
          Props(
            classOf[StatisticManager],
            databaseManager,
            timing,
            simultaneousProcessing,
            complaintLimit,
            numberOfMonthsInMonthlyStatistic,
            chunkSize
          )
        )
/*
        as.actorOf(
          Props(
            classOf[TransactionManager],
            databaseManager,
            statisticManager,
            timing,
            ourCommision,
            taxes
          )
        )

        as.actorOf(
          Props(
            classOf[PaymentManager],
            databaseManager,
            statisticManager,
            timing,
            20
          )
        )

        as.actorOf(
          Props(
            classOf[CancelCourseManager],
            databaseManager,
            serviceDatabaseManager,
            statisticManager,
            timing
          )
        )

        as.actorOf(
          Props(
            classOf[MonthlyReportManagerFactory],
            databaseManager,
            MonthlyBatchSettings(
              DateTimeMonthMax(new DateTime()),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getInt("numberOfMonthRecalculation"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("statisticFolder"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getInt("numberOfUsersProcessing"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getInt("chunkSize"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("paidDataFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("receivedPaymentDataFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("statisticsDataFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("checksumsDataFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("paymentReleaseRequestsChecksumsFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("paymentReleaseRequestsFile"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getString("metaDataFile"),
              HashMap(
                MonthlyReportRoles.Admin -> classOf[AdminGenerator],
                MonthlyReportRoles.Affiliate -> classOf[AffiliateGenerator],
                MonthlyReportRoles.Student -> classOf[StudentGenerator],
                MonthlyReportRoles.Instructor -> classOf[InstructorGenerator],
                MonthlyReportRoles.School -> classOf[SchoolGenerator]
              ),
              HashMap(
                MonthlyReportRoles.Admin -> classOf[AdminChecksumCalculator],
                MonthlyReportRoles.Affiliate -> classOf[AffiliateChecksumCalculator],
                MonthlyReportRoles.Student -> classOf[StudentChecksumCalculator],
                MonthlyReportRoles.Instructor -> classOf[InstructorChecksumCalculator],
                MonthlyReportRoles.School -> classOf[SchoolChecksumCalculator]
              ),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getLong("monthlyRecalculationTiming"),
              as.settings.config.getConfig("edusio-tsp").getConfig("monthly-report").getLong("monthlyManagerTiming"),
              as.settings.config.getConfig("edusio-tsp").getLong("complaintLimit")
            )
          )
        )
*/
      })

    } catch {
      case t: Throwable =>
        if (retry) {
          logger.error("Error while starting service, trying in 5 seconds. [" + t.toString + "]")
          Thread.sleep(5000)
          start
        } else {
          logger.error("Error while starting service. Stopped.")
        }
    }
  }

  def stop = {
    actorSystem.map(as => {
      as.shutdown()
    })
    actorSystem = None
    retry = false
  }

  def restart = {
    logger.error("Restarting service.")
    stop
    retry = true
    start
  }
}
