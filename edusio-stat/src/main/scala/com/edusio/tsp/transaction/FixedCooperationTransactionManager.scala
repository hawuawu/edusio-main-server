package com.edusio.tsp.transaction

import akka.actor.{ActorLogging, Props, Actor, ActorRef}
import com.edusio.tsp.database.DatabaseTaskError
import com.edusio.tsp.database.tasks._
import com.edusio.tsp.statistic.EnqueueUser
import com.edusio.tsp.tools.ActorWithTimeoutSupport
import org.joda.time.{Months, DateTime}
import scala.concurrent.duration._

/**
 * Created by Wlsek on 7. 5. 2015.
 */

case class CooperationProcessed(cooperationContract: CooperationContract)

case class CooperationCannotProcessed(cooperationContract: CooperationContract)


class FixedCooperationTransactionManager(databaseManager: ActorRef, statisticManager: ActorRef, timeout: Long) extends ActorWithTimeoutSupport with ActorLogging {
  private var cooperationContracts: List[CooperationContract] = List()
  databaseManager ! QueryAllContracts

  def receive = {
    case dtr: CooperationContracts =>
      cooperationContracts = dtr.contracts
      cooperationContracts.map(c => {
        context.actorOf(Props(classOf[FixedCooperationTransactionProcessor], databaseManager, self, c))
      })
      processContracts
    case cp: CooperationProcessed =>
      context.stop(sender)
      cooperationContracts = cooperationContracts.filter(c => c.id != cp.cooperationContract.id)
      statisticManager ! EnqueueUser(cp.cooperationContract.fromUser)
      statisticManager ! EnqueueUser(cp.cooperationContract.toUser)
      processContracts
    case cp: CooperationCannotProcessed =>
      context.stop(sender)
      cooperationContracts = cooperationContracts.filter(c => c.id != cp.cooperationContract.id)
      processContracts
    case DatabaseTaskError =>
      processContracts
  }

  def onTimeout = {
    databaseManager ! QueryAllContracts
  }

  def processContracts = if (cooperationContracts.size == 0)  {
    log.debug("All proceeded, waiting for next time.")
    startTimeout(timeout millis)
  }
}

class FixedCooperationTransactionProcessor(databaseManager: ActorRef, parent: ActorRef, contract: CooperationContract) extends Actor with ActorLogging {
  private var transactions: List[FixedContractTransaction] = List()
  private val now = new DateTime()
  private var lastStep = false

  databaseManager ! QueryLastFixedContractTransaction(contract.toUser)

  def receive = {
    case dtr: FixedContractCommissions =>
      if (!lastStep) {
        val timeHook = if (dtr.transactions.size > 0) {
          dtr.transactions.head.date
        } else {
          contract.time
        }

        val months = Months.monthsBetween(timeHook, now)

        transactions = (for (i <- 1 to months.getMonths) yield {
          val fc = FixedContractTransaction(0, contract.id, contract.fromUser, contract.toUser, contract.commission, timeHook.plusMonths(i))
          databaseManager ! QueryInsertFixedContractTransaction(fc)
          fc
        }).toList
        if (transactions.size > 0) {
          log.debug("Processing transactions: " + transactions)
        } else {
          waitOrProcess
        }
      } else {
        val months = Months.monthsBetween(contract.time, now).getMonths
        if (dtr.transactions.size != months) {
          log.error("Error in fixed contract transactions, trying to recover from it.")
          for (i <- 1 to months) {
            val actualDate = contract.time.plusMonths(i)
            if (!dtr.transactions.exists(e => e.date == actualDate)) {
              val fc = FixedContractTransaction(0, contract.id, contract.fromUser, contract.toUser, contract.commission, actualDate)
              databaseManager ! QueryInsertFixedContractTransaction(fc)
              transactions = fc :: transactions
            }
          }
        } else {
          parent ! CooperationProcessed(contract)
        }
      }

    case i: FixedContractTransactionInserted =>
      log.debug("Inserted fixed contract transaction: " + i.transaction)
      transactions = transactions.filter(f => f != i.transaction)
      waitOrProcess

    case i: CannotInsertFixedContractTransaction =>
      log.error("Cannot insert fixed contract transaction: " + i.transaction)
      transactions = transactions.filter(f => f != i.transaction)
      waitOrProcess

    case DatabaseTaskError =>
      parent ! CooperationCannotProcessed(contract)
  }

  def waitOrProcess = {
    if (transactions.size == 0) {
      if (!lastStep) {
        lastStep = true
        databaseManager ! QueryFixedContractTransactionsForInstructor(contract.toUser)
      } else {
        parent ! CooperationProcessed(contract)
      }
    }
  }
}