package com.edusio.tsp.transaction

import akka.actor.{ActorLogging, Props, Actor, ActorRef}
import com.edusio.tsp.database.DatabaseTaskError
import com.edusio.tsp.database.tasks._
import com.edusio.tsp.statistic.EnqueueUser
import com.edusio.tsp.tools.ActorWithTimeoutSupport

import scala.collection.immutable.HashMap
import scala.concurrent.duration._

/**
 * Created by Wlsek on 13. 10. 2015.
 */


case object QueryCancelledCoursesWithTransactions

class CancelCourseManager(databaseManager: ActorRef, serviceDatabaseManager: ActorRef, statisticManager: ActorRef, timing: Long) extends ActorWithTimeoutSupport with ActorLogging {

  private var cancelledCoursesBuffer: HashMap[CancelledTransaction, ActorRef] = HashMap()
  self ! QueryCancelledCoursesWithTransactions

  log.debug("CancelCourseManager started.")

  def receive = {
    case QueryCancelledCoursesWithTransactions =>
      log.debug("Querying cancelled courses with transactions.")
      databaseManager ! QueryNotProcessedCancelledCourses

    case ct: CancelledCoursesWithTransactions =>
      if (ct.transactions.isEmpty) {
        startTimeout(timing millis)
        log.debug("No cancelled courses, trying again in {} millis", timing)
      } else {
        log.debug("There are {} cancelled courses.", ct.transactions.size)
        ct.transactions.map(t => {
          cancelledCoursesBuffer += t -> context.actorOf(Props(classOf[CancelTransactionProcessor], t, self, databaseManager))
        })
      }

    case t: TransactionWasCancelled =>
      log.debug("Transaction {} and order {} was cancelled", t.transaction.transactionId, t.transaction.orderId)
      context.stop(sender)
      cancelledCoursesBuffer = cancelledCoursesBuffer.filter(f => f._1.transactionId != t.transaction.transactionId)
      if (cancelledCoursesBuffer.find(f => f._1.courseId == t.transaction.courseId).isEmpty) {
        log.debug("Marking course like processed.")
        serviceDatabaseManager ! QueryInsertProcessedTimeMarkForCancelledCourse(t.transaction.courseId, t.transaction.timeMark)
      }

      if (cancelledCoursesBuffer.isEmpty) {
        log.debug("Process buffer is empty, checking again in {} millis.", timing)
        startTimeout(timing millis)
      }

    case DatabaseTaskError =>
      log.debug("Error in database, checking again in {} millis.", timing)
      cancelledCoursesBuffer.map(b => context.stop(b._2))
      cancelledCoursesBuffer = HashMap()
      startTimeout(timing millis)
  }

  def onTimeout = {
    self ! QueryCancelledCoursesWithTransactions
  }
}

case class TransactionWasCancelled(transaction: CancelledTransaction)

class CancelTransactionProcessor(transaction: CancelledTransaction, parent: ActorRef, databaseManager: ActorRef) extends Actor with ActorLogging {
  databaseManager ! QueryCancelTransaction(transaction.transactionId, transaction.orderId)
  log.debug("CancelTransactionProcessor started. Sending cancel transaction to database manager.")

  def receive = {
    case t: TransactionCancelled =>
      log.debug("Transaction cancelled, sending information to parent.")
      parent ! TransactionWasCancelled(transaction)
    case DatabaseTaskError =>
      log.debug("There were database error, sending information to parent.")
      parent ! DatabaseTaskError
  }
}