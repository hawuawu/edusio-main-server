package com.edusio.tsp.transaction

import akka.actor._
import com.edusio.tsp.database.DatabaseTaskError
import com.edusio.tsp.database.tasks._
import com.edusio.tsp.statistic.EnqueueUser
import com.edusio.tsp.tools.{CommissionCalculator, ActorWithTimeoutSupport}
import scala.collection.immutable.HashMap
import scala.concurrent.duration._

/**
 * Created by Wlsek on 4. 5. 2015.
 */

case class TransactionActionDone(transaction: NotCompletedTransaction)

class TransactionManager(databaseManager: ActorRef, statisticManager: ActorRef, timing: Long, ourCommission: Float, taxes: Float) extends ActorWithTimeoutSupport {
  private var transactions: List[NotCompletedTransaction] = List()
  private var handlers: List[ActorRef] = List()
  private var timer: Option[ActorRef] = None

  databaseManager ! QueryNotCompletedTransactions

  def receive = {
    case dtr: NotCompletedTransactions =>
      transactions = dtr.transactions
      handlers = dtr.transactions.map(t => {
        t.status match {
          case "completed" =>
            context.actorOf(Props(classOf[TransactionCreateCommissionHandler], databaseManager, self, statisticManager, t, ourCommission, taxes))
          case "canceled" =>
            context.actorOf(Props(classOf[TransactionCancelCommissionHandler], databaseManager, self, t))

        }
      })
      if (transactions.size == 0) startTimeout(timing millis)

    case DatabaseTaskError =>
      handlers = handlers.filter(h => {
        if(h == sender) {
          context.stop(h)
          false
        } else {
          true
        }
      })

  if(handlers.isEmpty)startTimeout(timing millis)
     //

    case td: TransactionActionDone =>
      transactions = transactions.filter(t => t != td.transaction)
      handlers = handlers.filter(h => {
        if(h == sender) {
          context.stop(h)
          false
        } else {
          true
        }
      })

      if (transactions.length == 0) {
        startTimeout(timing millis)
      }
  }

  def onTimeout = {
    databaseManager ! QueryNotCompletedTransactions
  }
}

class TransactionCreateCommissionHandler(databaseManager: ActorRef, parent: ActorRef, statisticManager: ActorRef, transaction: NotCompletedTransaction, ourCommission: Float, taxes: Float) extends Actor with ActorLogging {
  private var cooperations: List[Cooperation] = List()
  private var eventsWithSubstitution: HashMap[EventForCourse, Option[Int]] = HashMap()
  private var instructorId: Option[Int] = None
  private var commissions: List[Commision] = List()

  private var eventsToAskForSubstitution: List[EventForCourse] = List()

  databaseManager ! QueryClearNotCompletedTransactionArtefacts(transaction.transactionId)
  log.info("Querying to clear transaction artifacts")

  def receive = {
    case ClearNotCompletedTransactionArtefactsComplete =>
      log.info("Clear done, querying instructor for course.")
      databaseManager ! QueryInstructorForCourseId(transaction.courseId)

    case dtr: QueriedInstructor =>
      log.info("Got instructor, querying cooperations")
      instructorId = Some(dtr.instructorId)
        databaseManager ! QueryCooperationForTransactionAndInstructor(transaction.transactionId, dtr.instructorId)

    case dtr: Cooperations =>
      log.info("Got cooperations, querying events for course.")
      cooperations = dtr.cooperations
      databaseManager ! QueryEventsForCourseId(transaction.courseId)

    case dtr: EventsForCourse =>
      log.info("Got events info, asking for substitution or split.")
      eventsToAskForSubstitution = dtr.events
      askForSubstitutionOrSplitCommissions

    case dtr: SubstitutionInstructorForEvent =>
      log.info("Got some substitution for event. Asking another substitution or split.")
      var found: Option[EventForCourse] = None
      eventsToAskForSubstitution = eventsToAskForSubstitution.filter(f => {
        if(f.id == dtr.eventId) {
          found = Some(f)
          false
        } else {
          true
        }
      })

      found.map(f => eventsWithSubstitution += f -> Some(dtr.instructorId))
      askForSubstitutionOrSplitCommissions

    case dtr: NoSubstitutionInstructorForEvent =>
      log.info("Got none substitution for event. Asking another substitution or split.")
      var found: Option[EventForCourse] = None
      eventsToAskForSubstitution = eventsToAskForSubstitution.filter(f => {
        if(f.id == dtr.eventId) {
          found = Some(f)
          false
        } else {
          true
        }
      })

      found.map(f => eventsWithSubstitution += f -> None)
      askForSubstitutionOrSplitCommissions

    case dtr: InsertCommisionToPayDone =>
      commissions = commissions.filter(f => f != dtr.commision)
      if (commissions.size == 0) databaseManager ! QueryCompleteNotCompletedTransaction(transaction)

    case NotCompletedTransactionCompleted =>
      parent ! TransactionActionDone(transaction)

    case DatabaseTaskError =>
      parent ! DatabaseTaskError

    case t =>
      log.error("Unknown message " + t.toString)
  }

  def askForSubstitutionOrSplitCommissions =  if (eventsToAskForSubstitution.size > 0) {
    log.info("Asking another substitution.")
      databaseManager ! QuerySubstitutionInstructorForEventId(eventsToAskForSubstitution.head.id)
  } else {
log.info("Splitting")

/*
    val ourValue = transaction.value * ourCommission
    val toDivide = transaction.value - ourValue

    commissions = List(Commision(transaction.courseId, 0, transaction.transactionId, instructorId.get, ourValue, "course", None, "edusio"))

    if (cooperations.size > 0) {
      var weight: Float = 1
      cooperations.map(co => weight = weight + co.percent)
      weight = 1 / weight

      /* Teachers commission */
      if (eventsWithSubstitution.size > 0) {
        eventsWithSubstitution.map(e => {
          commissions = Commision(transaction.courseId, e._2.getOrElse(instructorId.get), transaction.transactionId, instructorId.get, (1 * (weight * toDivide)) / eventsWithSubstitution.size, "event", Some(e._1.id), e._2.map(_=>"substitution").getOrElse("instructing")) :: commissions
        })
      } else {
        commissions = Commision(transaction.courseId, instructorId.get, transaction.transactionId, instructorId.get, (1 * weight * toDivide), "course", None, "instructing") :: commissions
      }

      /* Percentage cooperation */
      cooperations.map(co => {
        if (eventsWithSubstitution.size > 0) {
          eventsWithSubstitution.map(e => {
            commissions = Commision(transaction.courseId, co.withUser, transaction.transactionId, e._2.getOrElse(instructorId.get), (co.percent * weight * toDivide) / eventsWithSubstitution.size, "event", Some(e._1.id), co.cType) :: commissions
          })
        } else {
          commissions = Commision(transaction.courseId, co.withUser, transaction.transactionId, instructorId.get, (co.percent * weight * toDivide), "course", None, co.cType) :: commissions
        }
      })

    } else {
      /* Teachers commission */
      if (eventsWithSubstitution.size > 0) {
        eventsWithSubstitution.map(e => {
          commissions = Commision(transaction.courseId, e._2.getOrElse(instructorId.get), transaction.transactionId, instructorId.get, toDivide / eventsWithSubstitution.size, "event", Some(e._1.id), e._2.map(_=>"substitution").getOrElse("instructing")) :: commissions
        })
      } else {
        commissions = Commision(transaction.courseId, instructorId.get, transaction.transactionId, instructorId.get, toDivide, "course", None, "instructing") :: commissions
      }
    }*/

    try {
      CommissionCalculator(transaction, instructorId.get, cooperations, eventsWithSubstitution, taxes).map(co => {
        statisticManager ! EnqueueUser(co.fromUser)
        statisticManager ! EnqueueUser(co.userId)
        databaseManager ! QueryInsertCommisionToPay(co)
      })
    } catch {
      case t: Throwable =>
        parent ! DatabaseTaskError
    }
  }

}

class TransactionCancelCommissionHandler(databaseManager: ActorRef, parent: ActorRef, transaction: NotCompletedTransaction) extends Actor {
  databaseManager ! QueryClearNotCompletedTransactionArtefacts(transaction.transactionId)

  def receive = {
    case ClearNotCompletedTransactionArtefactsComplete =>
      databaseManager ! QueryCompleteNotCompletedTransaction(transaction)
    case NotCompletedTransactionCompleted =>
      parent ! TransactionActionDone(transaction)
    case DatabaseTaskError =>
      parent ! DatabaseTaskError
  }
}