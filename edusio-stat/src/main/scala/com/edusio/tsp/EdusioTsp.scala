package com.edusio.tsp

import akka.actor._
import com.edusio.tsp.database.MysqlDatabaseManager
import com.edusio.tsp.log.Logger
import com.edusio.tsp.payment.PaymentManager
import com.edusio.tsp.statistic.StatisticManager
import com.edusio.tsp.transaction.{FixedCooperationTransactionManager, TransactionManager}

/**
 * Created by Wlsek on 4. 5. 2015.
 */
object EdusioTsp extends App {
  override def main(args: Array[String]) {
    EntryPointService.start
  }
}


