package com.edusio.tsp.io

import java.io.{FileWriter, BufferedWriter}

import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.native.Serialization.write

/**
 * Created by Wlsek on 27. 10. 2015.
 */
object FileJsonWrite {
  def apply(file: String, json: JValue): Boolean = {
    implicit lazy val formats = org.json4s.DefaultFormats
    try {
      val wr = new BufferedWriter(new FileWriter(file))
      wr.write(write(json))
      wr.close()
      true
    } catch {
      case t: Throwable =>
        false
    }
  }
}