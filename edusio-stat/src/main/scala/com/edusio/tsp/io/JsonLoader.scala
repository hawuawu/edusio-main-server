package com.edusio.tsp.io

import java.io.{FileReader, BufferedReader, File}
import org.json4s.JsonAST._
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.native.JsonMethods._
/**
 * Created by Wlsek on 27. 10. 2015.
 */
object FileJsonRead {
  def apply(file: String): Option[JValue] = {
    implicit lazy val formats = org.json4s.DefaultFormats

    val f = new File(file)
    try {
      if (f.canRead) {
        val br = new BufferedReader(new FileReader(file))
        var line = br.readLine()
        var total = line
        while (line != null) {
          total += line
          line = br.readLine()
        }
        br.close()
        Some(parse(total))
      } else {
        None
      }
    } catch {
      case e: Exception =>
        println(e)
        None
    }
  }
}
