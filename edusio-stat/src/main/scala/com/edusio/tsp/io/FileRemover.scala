package com.edusio.tsp.io

import java.io.File

/**
 * Created by Wlsek on 27. 10. 2015.
 */
object FileRemover {
  def apply(files: List[String]): Unit = {
    files.map(f => {
      new File(f) delete()
    })
  }
}
