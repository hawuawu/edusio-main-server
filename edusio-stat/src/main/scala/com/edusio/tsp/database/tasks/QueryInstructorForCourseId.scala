package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTask, DatabaseConfigs, DatabaseTaskError, DatabaseTaskResult}

/**
 * Created by Wlsek on 4. 5. 2015.
 */
case class QueryInstructorForCourseId(courseId: Int) extends DatabaseTask {
  override def doTask(conn: Connection): DatabaseTaskResult = {
    try {
      val stmt = conn.createStatement()
      val sql = "SELECT post_author FROM " + DatabaseConfigs.postsTable + " WHERE ID="+courseId+" LIMIT 1;"
      val rs = stmt.executeQuery(sql)


      var result: Option[Int] = None
      while (rs.next()) {
        result = Some(rs.getInt("post_author"))
      }

      rs.close()
      stmt.close()

      result.map(r => QueriedInstructor(r)).getOrElse(DatabaseTaskError)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryInstructorForCourseId: " + t)
        DatabaseTaskError
    }
  }
}

case class QueriedInstructor(instructorId: Int) extends DatabaseTaskResult