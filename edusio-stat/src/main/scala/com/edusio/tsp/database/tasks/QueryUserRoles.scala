package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.monthly.CommissionRelation
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult, DatabaseTask}


/**
 * Created by Wlsek on 26. 10. 2015.
 */

object MonthlyReportRoles extends Enumeration {
  val Admin, Affiliate, Student, Instructor, School = Value
}

case class QueryUserRoles(userId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {

      val stmt =connection.createStatement()

      val isAdminQuery = "SELECT meta.* FROM `wp_users` as users " +
        "JOIN `wp_usermeta` as meta ON users.id = meta.user_id " +
        "WHERE users.id = "+userId+" AND meta.meta_key = 'wp_capabilities' AND meta.meta_value REGEXP ('admin')"

      val isAffiliateQuery = "SELECT * FROM edusio_referral_commisions WHERE parent_user_id = " + userId

      val isAffiliateSecondQuery = "SELECT * FROM edusio_commissions_to_pay WHERE user_id = " + userId + " AND commission_relation = '" + CommissionRelation.REFERRAL.toString + "' LIMIT 1;"

      val isInstructorQuery = "SELECT user_id FROM `wp_bp_xprofile_groups` as groups " +
        "JOIN `wp_bp_xprofile_fields` as fields ON fields.group_id = groups.id " +
        "JOIN `wp_bp_xprofile_data` as profile ON profile.field_id = fields.id " +
        "WHERE groups.name = \"Instructor\" AND profile.user_id = "+userId+" LIMIT 1"

      val isSchoolQuery = "SELECT user_id FROM `wp_bp_xprofile_groups` as groups " +
        "JOIN `wp_bp_xprofile_fields` as fields ON fields.group_id = groups.id " +
        "JOIN `wp_bp_xprofile_data` as profile ON profile.field_id = fields.id " +
        "WHERE groups.name = \"School\" AND profile.user_id = "+userId+" LIMIT 1"

      var result: List[MonthlyReportRoles.Value] = List(MonthlyReportRoles.Student)

      var r = stmt.executeQuery(isAdminQuery)
      if (r.next()) {
        result = MonthlyReportRoles.Admin :: result
      }
      r.close()

      r = stmt.executeQuery(isAffiliateQuery)
      if (r.next()) {
        result = MonthlyReportRoles.Affiliate :: result
      } else {
        val r2 = stmt.executeQuery(isAffiliateSecondQuery)
        if (r2.next()) {
          result = MonthlyReportRoles.Affiliate :: result
        }
        r2.close()
      }
      r.close()

      r = stmt.executeQuery(isInstructorQuery)
      if (r.next()) {
        result = MonthlyReportRoles.Instructor :: result
      }
      r.close()

      r = stmt.executeQuery(isSchoolQuery)
      if (r.next()) {
        result = MonthlyReportRoles.School :: result
      }
      r.close()

      stmt.close()
      QueriedUserRoles(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError

      case t: Throwable =>
        log.error("Error QueryUserRoles: " + t)
        DatabaseTaskError
    }
  }
}


case class QueriedUserRoles(roles: List[MonthlyReportRoles.Value]) extends DatabaseTaskResult