package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 11. 5. 2015.
 */
case class QueryInsertFixedTransactionCommissionToPayment(transaction: FixedTransactionCommission, request: PaymentRequest)  extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "INSERT INTO `"+DatabaseConfigs.paymentRequestMembership+"`(`transaction_id`, `transaction_type`, `request_id`) VALUES ("+transaction.id+",'fixed_commission',"+request.id+")"
      val rs = stmt.executeUpdate(sql)

      stmt.close()
      if(rs > 0) {
        InsertedFixedTransactionCommissionToPayment(transaction)
      } else {
        DatabaseTaskError
      }

    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryInsertFixedTransactionCommissionToPayment: " + t)
        DatabaseTaskError
    }
  }
}

case class InsertedFixedTransactionCommissionToPayment(transaction: FixedTransactionCommission) extends DatabaseTaskResult