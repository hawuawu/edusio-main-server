package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 5. 5. 2015.
 */
case class QueryPaidCommissionsForUserId(userId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT * FROM `edusio_commissions_to_pay` WHERE `from_user`=" + userId

      val rs = stmt.executeQuery(sql)

      var result: List[Commision] = List()

      while (rs.next()) {
        result = Commision(
        rs.getInt("course_id"),
        rs.getInt("user_id"),
        rs.getInt("transaction_id"),
        userId,
        rs.getFloat("commission"),
        rs.getString("commission_bond_to"),
        if(rs.getString("commission_bond_to") == "event") {
          Some(rs.getInt("bound_to_id"))
        } else {
          None
        },
        rs.getString("commission_relation")) :: result
      }

      rs.close()
      stmt.close()

      PaidCommissions(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryPaidCommissionsForUserId: " + t)
        DatabaseTaskError
    }
  }
}

case class PaidCommissions(commissions: List[Commision]) extends DatabaseTaskResult