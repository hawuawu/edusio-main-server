package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}
/**
 * Created by Wlsek on 12. 11. 2015.
 */

case class QueryRemoveStatisticsForUser(userId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      val sql = "DELETE FROM edusio_statistics WHERE user_id = " + userId
      val rs = stmt.executeUpdate(sql)
      stmt.close()
      QueryRemoveStatisticsForUserDone
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError

      case t: Throwable =>
        log.error("Error QueryRemoveStatisticsForUser: " + t)
        DatabaseTaskError
    }
  }
}

case object QueryRemoveStatisticsForUserDone extends DatabaseTaskResult