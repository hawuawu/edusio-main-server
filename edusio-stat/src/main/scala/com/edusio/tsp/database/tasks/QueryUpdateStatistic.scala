package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 5. 5. 2015.
 */

case class Statistic(userId: Int, subjectId: Int, statisticName: String, statisticLabel: String, value: Float, time: DateTime)

case class QueryUpdateStatistic(statistic: Statistic) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");


      val sql = "INSERT INTO `edusio_statistics` (`user_id`, `subject_id`, `statistic_name`, `statistic_label`, `value`, `time`) " +
        "VALUES ("+statistic.userId+", "+statistic.subjectId+", '"+statistic.statisticName+"', '"+statistic.statisticLabel+"', "+statistic.value+", '"+ statistic.time.toString(formatter)+"') " +
        "on duplicate key update statistic_label = values(statistic_label), value = values(value), time = values(time);"
      val rs = stmt.executeUpdate(sql)
      stmt.close()
      if (rs > 0) {
        StatisticUpdated(statistic)
      } else {
        DatabaseTaskError
      }
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError

      case t: Throwable =>
        log.error("Error QueryUpdateStatistic: " + t)
        DatabaseTaskError
    }
  }
}

case class StatisticUpdated(statistic: Statistic) extends DatabaseTaskResult