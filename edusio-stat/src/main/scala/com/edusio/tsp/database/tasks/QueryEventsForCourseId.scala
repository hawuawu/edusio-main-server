package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTask, DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 4. 5. 2015.
 */
case class QueryEventsForCourseId(courseId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT posts.id as course_id, posts.post_title as course_title, occurs.occur_begin as begin, occurs.occur_end as end, occurs.occur_id as occur_id " +
        "FROM wp_my_calendar_events occurs INNER JOIN wp_my_calendar calendar on occurs.occur_event_id=calendar.event_id " +
        "INNER JOIN wp_my_calendar_categories all_categories ON calendar.event_category=all_categories.category_id "+
        "INNER JOIN wp_posts posts ON posts.post_title=all_categories.category_name WHERE posts.id="+courseId+" GROUP BY occur_id"

      val rs = stmt.executeQuery(sql)

      var result: List[EventForCourse] = List();

      val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");

      while (rs.next()) {
        result = EventForCourse(rs.getInt("occur_id"), formatter.parseDateTime(rs.getString("begin")), formatter.parseDateTime(rs.getString("end"))) :: result
      }

      rs.close()
      stmt.close()

      EventsForCourse(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryEventsForCourseId: " + t)
        DatabaseTaskError
    }
  }
}

case class EventForCourse(id: Int, begin: DateTime, end: DateTime)
case class EventsForCourse(events: List[EventForCourse]) extends DatabaseTaskResult

