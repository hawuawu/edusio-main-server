package com.edusio.tsp.database.tasks.monthly

import java.sql.{Connection, SQLNonTransientConnectionException}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 17. 11. 2015.
 */
case class QueryRemoveOlderMonthlyCommissionStatistic(userId: Int, numberOfMonths: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "DELETE FROM `edusio_statistics` WHERE user_id = "+userId+" " +
        "AND statistic_name REGEXP(\"monthly.commission.gain.*\") "+
        "AND time < (NOW() - INTERVAL "+numberOfMonths+" MONTH)"

      stmt.executeUpdate(sql)
      stmt.close()
      RemovedOlderMonthlyCommissionStatistics
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryAffiliateLevel: " + t)
        DatabaseTaskError
    }
  }
}

case object RemovedOlderMonthlyCommissionStatistics extends DatabaseTaskResult