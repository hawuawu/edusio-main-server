package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 8. 5. 2015.
 */
case class QueryFinalizePaymentRequest(request: PaymentRequest, duration: Long, amount: Float) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      if(amount > 0) {
        val sql = "UPDATE `"+DatabaseConfigs.paymentRequest+"` SET `processed`=1,`process_duration`="+duration+", amount="+amount+" WHERE `id`=" + request.id
        stmt.executeUpdate(sql)
      } else {
        val sql = "UPDATE `"+DatabaseConfigs.paymentRequest+"` SET `processed`=1,`process_duration`="+duration+", amount="+amount+" WHERE `id`=" + request.id
        val result = stmt.executeQuery(sql)
        if(!result.next()) {
          val sql = "DELETE FROM `"+DatabaseConfigs.paymentRequest+"` WHERE `id`=" + request.id
          stmt.executeUpdate(sql)
        }
      }
      stmt.close()
      PaymetRequestFinalized
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryFinalizePaymentRequest: " + t)
        DatabaseTaskError
    }
  }
}

case object PaymetRequestFinalized extends DatabaseTaskResult
