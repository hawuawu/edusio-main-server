package com.edusio.tsp.database.tasks

import java.sql.{Connection, SQLNonTransientConnectionException}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 13. 10. 2015.
 */

case class QueryCancelTransaction(transactionId: Int, orderId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {

      connection.setAutoCommit(false)
      val stmt = connection.createStatement()
      val sql = "UPDATE `edusio_transactions` SET `status`='canceled' WHERE id = " + transactionId
      val rs = stmt.executeUpdate(sql)

      if (rs > 0) {
        val sql2 = "UPDATE `wp_posts` SET post_status = 'wc-cancelled' WHERE ID = " + orderId
        val rs2 = stmt.executeUpdate(sql2)
        if (rs2 > 0) {
          connection.commit()
          stmt.close()
          TransactionCancelled(transactionId)
        } else {
          connection.rollback()
          stmt.close()
          connection.setAutoCommit(true)
          DatabaseTaskError
        }
      } else {
        connection.rollback()
        stmt.close()
        connection.setAutoCommit(true)
        DatabaseTaskError
      }
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        connection.setAutoCommit(true)
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryCancelTransaction: " + t)
        connection.setAutoCommit(true)
        DatabaseTaskError
    }
  }
}

case class TransactionCancelled(transactionId: Int) extends DatabaseTaskResult