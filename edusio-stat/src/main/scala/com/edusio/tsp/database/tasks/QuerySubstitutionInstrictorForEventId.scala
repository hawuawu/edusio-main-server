package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseConfigs, DatabaseTaskError, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 4. 5. 2015.
 */
case class QuerySubstitutionInstructorForEventId(eventId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT * from " + DatabaseConfigs.substitutionTable + " WHERE occur_id=" + eventId

      val rs = stmt.executeQuery(sql)

      var result: Option[Int] = None

      while (rs.next()) {
        result = Some(rs.getInt("instructor_id"))
      }

      rs.close()
      stmt.close()

      result.map(r =>
        SubstitutionInstructorForEvent(eventId, r)
      ).getOrElse(NoSubstitutionInstructorForEvent(eventId))
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError

      case t: Throwable =>
        log.error("Error QuerySubstitutionInstructorForEventId: " + t)
        DatabaseTaskError
    }
  }
}
case class NoSubstitutionInstructorForEvent(eventId: Int) extends DatabaseTaskResult
case class SubstitutionInstructorForEvent(eventId: Int, instructorId: Int) extends DatabaseTaskResult