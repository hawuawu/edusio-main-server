package com.edusio.tsp.database.tasks.monthly

import java.security.MessageDigest
import java.sql.{SQLNonTransientConnectionException, Connection}
import java.util.Base64

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}
import com.edusio.tsp.statistic.monthly.generator.{MonthlyStatisticGenerateAbleMessage, MonthlyStatisticJsonSerializeable}
import com.edusio.tsp.tools.DateTimeFromMysql
import org.joda.time.DateTime


import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._


/**
 * Created by Wlsek on 30. 10. 2015.
 */

case class CommissionForCourse(courseTitle: String, courseId: Int, courseAuthorId: Int, commission: Float, commissionRelation:  CommissionRelation.Value, fromUserId: Int, studentId: Int, orderDate: DateTime) extends MonthlyStatisticGenerateAbleMessage with MonthlyStatisticJsonSerializeable {
  def serialize = {
    ("courseTitle" -> courseTitle) ~
      ("courseId" -> courseId) ~
      ("courseAuthorId" -> courseAuthorId) ~
      ("commission" -> commission) ~
      ("commissionRelation" -> commissionRelation.toString) ~
      ("fromUserId" -> fromUserId) ~
      ("studentId" -> studentId) ~
      ("orderDate" -> orderDate.getMillis)
  }
}

case class EarningsForCourseByUserAndRelation(earnings: List[CommissionForCourse], hash: Option[String]) extends DatabaseTaskResult

case class QueryEarningsForCourseByUserAndRelation(userId: Int, limit: Int, offset: Int, timeAnchor: DateTime, relation: CommissionRelation.Value*) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      var relationWhere = ""
      relation.map(r => relationWhere = relationWhere + "'" + r.toString + "',")
      relationWhere = "AND commissions.commission_relation IN (" + (if (relation.size > 0) {
        relationWhere.substring(0, relationWhere.size - 1)
      } else {
        relationWhere
      }) + ") "

      val sql = "SELECT courses.post_title as course_title, " +
        "courses.id as course_id, " +
        "courses.post_author as course_author, " +
        "commissions.commission, " +
        "commissions.commission_relation, " +
        "fromUsers.ID as from_user, " +
        "customer_meta.meta_value as student_id, " +
        "orders.post_date as order_date, " +
        "MD5(CONCAT(courses.post_title, courses.id, courses.post_author, commissions.commission, commissions.commission_relation, fromUsers.ID, customer_meta.meta_value, orders.post_date)) as hash " +
        "FROM `edusio_commissions_to_pay`  as commissions " +
        "JOIN edusio_transactions as transactions ON transactions.id = commissions.transaction_id " +
        "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
        "JOIN wp_users as fromUsers ON fromUsers.id = from_user " +
        "JOIN wp_posts as courses ON courses.ID = commissions.course_id " +
        "JOIN wp_postmeta as customer_meta ON customer_meta.post_id = transactions.order_id " +
        "WHERE user_id = " + userId + " " +
        "AND commission_bond_to = 'course' " +
        "AND transactions.transaction_done = 1 " +
        "AND customer_meta.meta_key = '_customer_user' " +
        "AND MONTH(orders.post_date) = " + timeAnchor.getMonthOfYear + " " +
        relationWhere +
        "ORDER BY order_date, orders.ID,commissions.id,commissions.user_id  LIMIT " + limit + " OFFSET " + offset

      val rs = stmt.executeQuery(sql)

      var result: List[CommissionForCourse] = List()
      var hash: Option[String] = None

      while (rs.next()) {

        val orderCreated: Option[DateTime] = DateTimeFromMysql(rs.getString("order_date"), Some(log))

        result = CommissionForCourse(
          rs.getString("course_title"),
          rs.getInt("course_id"),
          rs.getInt("course_author"),
          rs.getFloat("commission"),
          CommissionRelation.withName(rs.getString("commission_relation")),
          rs.getInt("from_user"),
          rs.getInt("student_id"),
          orderCreated.getOrElse(new DateTime(0L))
        ) :: result
        if (hash.isEmpty) {
          hash = Some(rs.getString("hash"))
        } else {
          hash = Some(hash.get + rs.getString("hash"))
        }
      }
      hash = hash.map(h => {
        Base64.getEncoder.encodeToString(MessageDigest.getInstance("MD5").digest(h.getBytes("UTF-8")))
      })
      rs.close()
      stmt.close()
      EarningsForCourseByUserAndRelation(result, hash)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryEarningsForCourseByUserAndRelation: " + t)
        DatabaseTaskError
    }
  }
}

case class EarningsForCourseByUserAndRelationHash(hash: Option[String]) extends DatabaseTaskResult
case class QueryEarningsForCourseByUserAndRelationHash(userId: Int, limit: Int, offset: Int, timeAnchor: DateTime, relation: CommissionRelation.Value*) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      var relationWhere = ""
      relation.map(r => relationWhere = relationWhere + "'" + r.toString + "',")
      relationWhere = "AND commissions.commission_relation IN (" + (if (relation.size > 0) {
        relationWhere.substring(0, relationWhere.size - 1)
      } else {
        relationWhere
      }) + ") "

      val sql = "SELECT " +
        "MD5(CONCAT(courses.post_title, courses.id, courses.post_author, commissions.commission, commissions.commission_relation, fromUsers.ID, customer_meta.meta_value, orders.post_date)) as hash " +
        "FROM `edusio_commissions_to_pay` as commissions " +
        "JOIN edusio_transactions as transactions ON transactions.id = commissions.transaction_id " +
        "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
        "JOIN wp_users as fromUsers ON fromUsers.id = from_user " +
        "JOIN wp_posts as courses ON courses.ID = commissions.course_id " +
        "JOIN wp_postmeta as customer_meta ON customer_meta.post_id = transactions.order_id " +
        "WHERE user_id = " + userId + " " +
        "AND commission_bond_to = 'course' " +
        "AND transactions.transaction_done = 1 " +
        "AND customer_meta.meta_key = '_customer_user' " +
        "AND MONTH(orders.post_date) = " + timeAnchor.getMonthOfYear + " " +
        relationWhere +
        "ORDER BY orders.post_date, orders.ID,commissions.id,commissions.user_id  LIMIT " + limit + " OFFSET " + offset

      val rs = stmt.executeQuery(sql)

      var hash: Option[String] = None

      while (rs.next()) {
        if (hash.isEmpty) {
          hash = Some(rs.getString("hash"))
        } else {
          hash = Some(hash.get + rs.getString("hash"))
        }
      }
      hash = hash.map(h => {
        Base64.getEncoder.encodeToString(MessageDigest.getInstance("MD5").digest(h.getBytes("UTF-8")))
      })
      rs.close()
      stmt.close()
      EarningsForCourseByUserAndRelationHash(hash)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryEarningsForCourseByUserAndRelationHash: " + t)
        DatabaseTaskError
    }
  }
}