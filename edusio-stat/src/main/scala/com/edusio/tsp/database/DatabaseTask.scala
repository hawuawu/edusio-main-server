package com.edusio.tsp.database

import java.sql.Connection

import com.edusio.tsp.log.EdusioLoggable

/**
 * Created by Wlsek on 4. 5. 2015.
 */
trait DatabaseTask extends EdusioLoggable {
  def doTask(onConnection: Connection): DatabaseTaskResult
}

trait DatabaseTaskResult

object DatabaseTaskError extends DatabaseTaskResult