package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseConfigs, DatabaseTaskError, DatabaseTaskResult, DatabaseTask}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 11. 5. 2015.
 */
case class QueryFixedTransactionCommissions(paymentRequest: PaymentRequest, offset: Int, limit: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT * FROM `" + DatabaseConfigs.fixedCommissionsTable + "` WHERE `to_user`=" + paymentRequest.userId + " ORDER BY `created` LIMIT " + limit + " OFFSET " + offset + ";"

      val rs = stmt.executeQuery(sql)

      var result: List[FixedTransactionCommission] = List()

      val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");

      while (rs.next()) {


        result = FixedTransactionCommission(
          rs.getInt("id"),
          rs.getInt("contract_id"),
          rs.getFloat("commission"),
          rs.getInt("from_user"),
          rs.getInt("to_user"),
          formatter.parseDateTime(rs.getString("created"))
        ) :: result
      }

      rs.close()
      stmt.close()

      FixedTransactionCommissions(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryFixedTransactionCommissions: " + t)
        DatabaseTaskError
    }
  }
}

case class FixedTransactionCommission(id: Int, contractId: Int, commission: Float, fromUser: Int, toUser: Int, created: DateTime)

case class FixedTransactionCommissions(transactions: List[FixedTransactionCommission]) extends DatabaseTaskResult