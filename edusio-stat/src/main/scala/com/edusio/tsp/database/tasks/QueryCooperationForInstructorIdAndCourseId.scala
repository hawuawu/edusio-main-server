package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTask, DatabaseTaskResult, DatabaseConfigs, DatabaseTaskError}
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._


/**
 * Created by Wlsek on 4. 5. 2015.
 */


object TransactionCooperation extends Enumeration {
  val REFERRAL, SCHOOL_FIXED, SCHOOL_PERCENTAGE, EDUSIO = Value
}

case class QueryCooperationForTransactionAndInstructor(transactionId: Int, instructorId: Int) extends DatabaseTask {
  override def doTask(conn: Connection): DatabaseTaskResult = {
    try {
      val stmt = conn.createStatement()

      val selectSchoolCooperationSelect = "SELECT * FROM " + DatabaseConfigs.transactionMetaTable + " WHERE meta_key in ('SchoolCooperationType', 'SchoolCooperationSchool', 'SchoolCooperationCommision') AND edusio_transaction_id =" + transactionId
      val selectReferralCooperationSelect = "SELECT * FROM " + DatabaseConfigs.transactionMetaTable + " WHERE meta_key = 'Referrals' AND edusio_transaction_id=" + transactionId
      val selectEdusioCooperationSelect = "SELECT * FROM " + DatabaseConfigs.transactionMetaTable + " WHERE meta_key = 'EdusioCommision' AND edusio_transaction_id=" + transactionId

      var result: List[Cooperation] = List()

      val schoolCooperationResult = stmt.executeQuery(selectSchoolCooperationSelect)
      var schoolCooperationBuilder = Cooperation(instructorId, 0, 0, "")

      while (schoolCooperationResult.next()) {
        schoolCooperationResult.getString("meta_key") match {
          case "SchoolCooperationType" =>
            schoolCooperationResult.getString("meta_value") match {
              case "commision" => schoolCooperationBuilder = schoolCooperationBuilder.copy(cType = TransactionCooperation.SCHOOL_PERCENTAGE.toString)
              case "fixed" => schoolCooperationBuilder = schoolCooperationBuilder.copy(cType = TransactionCooperation.SCHOOL_FIXED.toString)
            }

          case "SchoolCooperationSchool" =>
            schoolCooperationBuilder = schoolCooperationBuilder.copy(withUser = schoolCooperationResult.getString("meta_value").toInt)

          case "SchoolCooperationCommision" =>
            schoolCooperationBuilder = schoolCooperationBuilder.copy(commission = schoolCooperationResult.getString("meta_value").toFloat)
        }
      }

      if (schoolCooperationBuilder.commission != 0 && schoolCooperationBuilder.cType != "" && schoolCooperationBuilder.withUser != 0) {
        result = schoolCooperationBuilder :: result
      }

      val referralCooperationResult = stmt.executeQuery(selectReferralCooperationSelect)
      while (referralCooperationResult.next()) {
        val refferalsRaw = referralCooperationResult.getString("meta_value")
        val json = parse(refferalsRaw)
        implicit lazy val formats = org.json4s.DefaultFormats

        var primaryReferal: Option[Cooperation] = None
        var secondaryReferal: Option[Cooperation] = None

        json.extract[List[JObject]].map(v => {
          val affiliateId = (v \ "ReferralUser").extract[String].toInt
          val commission = (v \ "ReferralCommision").extract[String].toFloat

          (v \ "type").extract[String] match {
            case "primary" =>
              primaryReferal =
                Some(Cooperation(-1, affiliateId, commission, TransactionCooperation.REFERRAL.toString))
            case "secondary" =>
              secondaryReferal =
                Some(Cooperation(instructorId, affiliateId, commission, TransactionCooperation.REFERRAL.toString))
          }
        })
        result = primaryReferal.map(pr => pr.copy(user = secondaryReferal.map(sr => sr.withUser).getOrElse(pr.user))).toList ::: secondaryReferal.toList ::: result
      }

      val selectEdusioCooperationResult = stmt.executeQuery(selectEdusioCooperationSelect)
      while (selectEdusioCooperationResult.next()) {
        result = Cooperation(instructorId, 0, selectEdusioCooperationResult.getString("meta_value").toFloat, TransactionCooperation.EDUSIO.toString) :: result
      }

      stmt.close()
      Cooperations(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryCooperationForTransaction: " + t)
        DatabaseTaskError
    }
  }
}


case class QueryCooperationForInstructorId(instructorId: Int, timeHook: Long) extends DatabaseTask {
  override def doTask(conn: Connection): DatabaseTaskResult = {
    try {

      val stmt = conn.createStatement()
      val sql = "SELECT *  FROM " + DatabaseConfigs.cooperationTable + " WHERE contract_type='commision' AND contract_flow='instructor-to-school' AND instructor_id=" + instructorId
      val rs = stmt.executeQuery(sql)


      val stmt2 = conn.createStatement()
      val sql2 = "SELECT *  FROM " + DatabaseConfigs.referralTable + " WHERE FROM_UNIXTIME(" + timeHook + ") >= started AND FROM_UNIXTIME(" + timeHook + ") <= ended AND child_user_id=" + instructorId
      val rs2 = stmt2.executeQuery(sql2)


      var result: List[Cooperation] = List()

      while (rs.next()) {
        result = Cooperation(rs.getInt("instructor_id"), rs.getInt("school_id"), rs.getFloat("provision"), "cooperation") :: result
      }

      while (rs2.next()) {
        result = Cooperation(rs.getInt("child_user_id"), rs.getInt("parent_user_id"), rs.getFloat("percentage"), "referral") :: result
      }
      rs.close()
      stmt.close()

      rs2.close()
      stmt2.close()
      Cooperations(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryCooperationForInstructorId: " + t)
        DatabaseTaskError
    }
  }
}

case class Cooperation(user: Int, withUser: Int, commission: Float, cType: String)

case class Cooperations(cooperations: List[Cooperation]) extends DatabaseTaskResult

/*
SELECT * FROM `edusio_referral_commisions` WHERE child_user_id = 2 AND (ended IS NULL or ended >= (SELECT orders.post_date FROM `edusio_transactions` as transactions
JOIN wp_posts as orders ON transactions.order_id = orders.ID
WHERE orders.post_type = "shop_order" AND transactions.id = 1 LIMIT 1))
 */