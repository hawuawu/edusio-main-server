package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseConfigs, DatabaseTaskError, DatabaseTaskResult, DatabaseTask}
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 8. 5. 2015.
 */
case class QueryAlreadyPaidTransactionCommissions(transactions: List[TransactionCommission])  extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val arrayQuery = "(" + transactions.map(t => t.id).mkString(",") + ")"
      val sql = "SELECT * FROM `"+DatabaseConfigs.paymentRequestMembership+"` WHERE `transaction_id` in " + arrayQuery + " AND transaction_type='commission';"

      val rs = stmt.executeQuery(sql)

      var result: List[Int] = List()

      val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");
      while (rs.next()) {
        result = rs.getInt("transaction_id") :: result
      }

      rs.close()
      stmt.close()
      AlreadyPaidTransactionCommissions(
        transactions.filter(t => result.exists(r => r == t.id))
      )
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryPaidCommissionsForUserId: " + t)
        DatabaseTaskError
    }
  }
}
case class AlreadyPaidTransactionCommissions(transactions: List[TransactionCommission]) extends DatabaseTaskResult