package com.edusio.tsp.database.tasks.monthly

import java.security.MessageDigest
import java.sql.{SQLNonTransientConnectionException, Connection}
import java.util.Base64

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}
import com.edusio.tsp.statistic.monthly.generator.MonthlyStatisticJsonSerializeable
import com.edusio.tsp.tools.DateTimeFromMysql
import org.joda.time.DateTime
import org.json4s.JsonAST.JValue

import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._

/**
 * Created by Wlsek on 12. 11. 2015.
 */

object PaymentRequestMeta extends Enumeration {
  val EDUSIO_PAYMENT_RELEASE_FIRSTNAME = Value("first_name")
  val EDUSIO_PAYMENT_RELEASE_MIDDLENAME = Value("middle_name")
  val EDUSIO_PAYMENT_RELEASE_LASTNAME = Value("last_name")
  val EDUSIO_PAYMENT_RELEASE_DATE_OF_BIRTH = Value("date_of_birth")
  val EDUSIO_PAYMENT_RELEASE_PHONE = Value("phone")
  val EDUSIO_PAYMENT_RELEASE_ADDRESS = Value("address")
  val EDUSIO_PAYMENT_RELEASE_COUNTRY = Value("country")
  val EDUSIO_PAYMENT_RELEASE_COMPANY_INDENTIFICATION_NUMBER = Value("company_identification_number")
  val EDUSIO_PAYMENT_RELEASE_LEGAL_RELATIONSHIP = Value("legal_relationship")
  val EDUSIO_PAYMENT_RELEASE_ACCOUNT_TYPE = Value("type")
  val EDUSIO_PAYMENT_RELEASE_ACCOUNT = Value("account")
  val EDUSIO_PAYMENT_RELEASE_CURRENCY = Value("currency")
  val EDUSIO_PAYMENT_RELEASE_PRICE = Value("price")
  val EDUSIO_PAYMENT_RELEASE_CHARGES = Value("charges")
}

case class UserPaymentRequest(
                               id: Int,
                               amount: Float,
                               created: DateTime,
                               firstName: String,
                               middleName: String,
                               lastName: String,
                               dateOfBirth: String,
                               phone: String,
                               address: String,
                               country: String,
                               companyIdentificationNumber: String,
                               legalRelationship: String,
                               accountType: String,
                               account: String,
                               currency: String,
                               price: Float,
                               charges: Float
                               ) extends  MonthlyStatisticJsonSerializeable {
  def serialize = {
    ("id" -> id) ~
      ("amount" -> amount) ~
      ("created" -> created.getMillis) ~
      ("firstName" -> firstName) ~
      ("middleName" -> middleName) ~
      ("lastName" -> lastName) ~
      ("dateOfBirth" -> dateOfBirth) ~
      ("phone" -> phone) ~
      ("address" -> address) ~
      ("country" -> country) ~
      ("companyIdentificationNumber" -> companyIdentificationNumber) ~
      ("legalRelationship" -> legalRelationship) ~
      ("accountType" -> accountType) ~
      ("account" -> account) ~
      ("currency" -> currency) ~
      ("price" -> price) ~
      ("charges" -> charges)
  }
}

case class QueryUserPaymentRequestForUser(userId: Int, limit: Int, offset: Int, timeAnchor: DateTime) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      val sql = "SELECT *, MD5(concat(id, created, user_id, status, processed, process_duration, amount)) as hash " +
        "FROM edusio_payment_requests as requests " +
        "WHERE requests.processed = 1 AND requests.user_id = " + userId + " " +
        "ORDER BY requests.created  LIMIT " + limit + " OFFSET " + offset

      val rs = stmt.executeQuery(sql)

      var result: List[UserPaymentRequest] = List()
      var hash: Option[String] = None

      while (rs.next()) {
        val id = rs.getInt("request_id")

        val sql2 = "SELECT *, MD5((SELECT GROUP_CONCAT(meta_value) FROM edusio_payment_requests_meta WHERE request_id = 30 ORDER BY request_id)) as hash FROM edusio_payment_requests_meta   WHERE request_id = 30  ORDER BY request_id";
        val rs2 = stmt.executeQuery(sql2)

        var tempPaymentRequest = UserPaymentRequest(id, rs.getFloat("amount"), DateTimeFromMysql(rs.getString("created")).getOrElse(new DateTime(0l)), "", "", "", "", "", "", "", "", "", "", "", "", 0, 0)
        var hash = Some(rs.getString("hash"))
        while (rs2.next()) {
          hash = Some(hash.map(h => h + rs2.getString("hash")).getOrElse(rs2.getString("hash")))

          PaymentRequestMeta.withName(rs2.getString("meta_key")) match {
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_ACCOUNT => tempPaymentRequest.copy(account = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_ACCOUNT_TYPE => tempPaymentRequest.copy(accountType = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_ADDRESS => tempPaymentRequest.copy(address = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_CHARGES => tempPaymentRequest.copy(charges = rs2.getFloat("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_COMPANY_INDENTIFICATION_NUMBER => tempPaymentRequest.copy(companyIdentificationNumber = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_COUNTRY => tempPaymentRequest.copy(country = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_CURRENCY => tempPaymentRequest.copy(currency = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_DATE_OF_BIRTH => tempPaymentRequest.copy(dateOfBirth = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_FIRSTNAME => tempPaymentRequest.copy(firstName = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_LASTNAME => tempPaymentRequest.copy(lastName = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_MIDDLENAME => tempPaymentRequest.copy(middleName = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_LEGAL_RELATIONSHIP => tempPaymentRequest.copy(legalRelationship = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_PHONE => tempPaymentRequest.copy(phone = rs2.getString("meta_value"))
            case PaymentRequestMeta.EDUSIO_PAYMENT_RELEASE_PRICE => tempPaymentRequest.copy(price = rs2.getFloat("meta_value"))
          }
        }
        rs2.close()
      }
      rs.close()
      stmt.close()
      QueriedUserPaymentsRequestsForUser(result, hash.map(h => {
        Base64.getEncoder.encodeToString(MessageDigest.getInstance("MD5").digest(h.getBytes("UTF-8")))
      }))
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryUserPaymentRequestForUser: " + t)
        DatabaseTaskError
    }
  }
}

case class QueriedUserPaymentsRequestsForUser(requests: List[UserPaymentRequest], hash: Option[String]) extends DatabaseTaskResult


case class QueryUserPaymentRequestForUserHash(userId: Int, limit: Int, offset: Int, timeAnchor: DateTime) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      val sql = "SELECT MD5(concat(id, created, user_id, status, processed, process_duration, amount)) as hash " +
        "FROM edusio_payment_requests as requests " +
        "WHERE requests.processed = 1 AND requests.user_id = " + userId + " " +
        "ORDER BY requests.created  LIMIT " + limit + " OFFSET " + offset

      val rs = stmt.executeQuery(sql)

      var result: List[UserPaymentRequest] = List()
      var hash: Option[String] = None

      while (rs.next()) {
        var hash = Some(rs.getString("hash"))

        val sql2 = "SELECT MD5((SELECT GROUP_CONCAT(meta_value) FROM edusio_payment_requests_meta WHERE request_id = 30 ORDER BY request_id)) as hash FROM edusio_payment_requests_meta   WHERE request_id = 30  ORDER BY request_id";
        val rs2 = stmt.executeQuery(sql2)
        while (rs2.next()) {
          hash = Some(hash.map(h => h + rs2.getString("hash")).getOrElse(rs2.getString("hash")))
          rs2.close()
        }
        rs.close()
        stmt.close()
      }

      QueriedUserPaymentsRequestsForUserHash(hash.map(h => {
        Base64.getEncoder.encodeToString(MessageDigest.getInstance("MD5").digest(h.getBytes("UTF-8")))
      }))
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryUserPaymentRequestForUserHash: " + t)
        DatabaseTaskError
    }
  }
}


  case class QueriedUserPaymentsRequestsForUserHash(hash: Option[String]) extends DatabaseTaskResult