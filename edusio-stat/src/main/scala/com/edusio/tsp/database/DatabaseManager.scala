package com.edusio.tsp.database

import java.sql.{Connection, DriverManager}

import akka.actor.{Props, Actor, ActorLogging}
import com.edusio.tsp.EntryPointService
import com.edusio.tsp.tools.ActorWithTimeoutSupport
import scala.concurrent.duration._

/**
 * Created by Wlsek on 4. 5. 2015.
 */
class MysqlDatabaseManager(
                            url: String,
                            dbName: String,
                            userName: String,
                            password: String,
                            numberOfConnections: Int
                            ) extends ActorWithTimeoutSupport with ActorLogging {

  private var connection: List[Connection] = List()
  private var actualConnection = 0

  reconnect

  def reconnect = {
    try {
      Class.forName("com.mysql.jdbc.Driver").newInstance()
      val conn = DriverManager.getConnection(url + dbName, userName, password);
      connection = conn :: connection
    } catch {
      case e: Exception =>
        connection.map(c => c.close)
        log.error("Error while getting database connections: " + e)
        Thread.sleep(5000)
        EntryPointService.restart
    }
  }

  def receive = {
    case t: DatabaseTask =>
      if (connection.size > 0) {
        context.actorOf(Props(classOf[MysqlDatabaseManagerTaskHandler], connection(actualConnection))).tell(t, sender)
        shiftConnection
      } else {
        sender ! DatabaseTaskError
      }
  }

  def shiftConnection = {
    if (actualConnection + 1 == connection.length) {
      actualConnection = 0
    } else {
      actualConnection += 1
    }
  }

  def onTimeout = reconnect
}

class MysqlDatabaseManagerTaskHandler(connection: Connection) extends Actor {
  def receive = {
    case t: DatabaseTask => sender ! t.doTask(connection)
  }
}