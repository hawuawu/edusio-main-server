package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTask, DatabaseTaskResult}

/**
 * Created by Wlsek on 8. 5. 2015.
 */
case class QueryTransactionCommissions(paymentRequest: PaymentRequest, offset: Int, limit: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT * FROM `edusio_commissions_to_pay` WHERE `user_id`="+paymentRequest.userId+" ORDER BY `created` LIMIT "+limit+" OFFSET "+offset+";"

      val rs = stmt.executeQuery(sql)

      var result: List[TransactionCommission] = List()

      while (rs.next()) {
        result = TransactionCommission(
          rs.getInt("id"),
          rs.getInt("course_id"),
          rs.getInt("user_id"),
          rs.getInt("transaction_id"),
          rs.getInt("from_user"),
          rs.getFloat("commission"),
          rs.getString("commission_bond_to"),
          if(rs.getString("commission_bond_to") == "event") {
            Some(rs.getInt("bound_to_id"))
          } else {
            None
          },
          rs.getString("commission_relation")) :: result
      }

      rs.close()
      stmt.close()

      TransactionCommissions(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError

      case t: Throwable =>
        log.error("Error QuerySubstitutionInstructorForEventId: " + t)
        DatabaseTaskError
    }
  }
}
case class TransactionCommission(id: Int, courseId: Int, userId: Int, transactionId: Int, fromUser: Int, commision: Float, commisionBoundTo: String, boundToId: Option[Int], commissionRelation: String)
case class TransactionCommissions(transactions: List[TransactionCommission]) extends DatabaseTaskResult