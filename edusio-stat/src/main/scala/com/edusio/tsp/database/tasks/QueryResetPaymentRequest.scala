package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 8. 5. 2015.
 */
case class QueryResetPaymentRequest(payment: PaymentRequest) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "DELETE FROM `edsuio_payment_membership` WHERE `request_id`=" + payment.id

      val rs = stmt.executeUpdate(sql)
      stmt.close()
      ResetPaymentRequestCompleted(payment)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError

      case t: Throwable =>
        log.error("Error QueryResetPaymentRequest: " + t)
        DatabaseTaskError
    }
  }
}

case class ResetPaymentRequestCompleted(payment: PaymentRequest) extends  DatabaseTaskResult