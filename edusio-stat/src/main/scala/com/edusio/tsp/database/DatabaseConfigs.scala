package com.edusio.tsp.database

/**
 * Created by Wlsek on 4. 5. 2015.
 */
object DatabaseConfigs {
  val transactionTable = "edusio_transactions"
  val transactionMetaTable = "edusio_transactionsmeta"
  val statisticsTable = "edusio_instructor_statistics"
  val commisionsTable = "edusio_commissions_to_pay"
  val fixedCommissionsTable = "edusio_fixed_contract_commissions_to_pay"
  val paymentsTable = "edusio_payment_requests"
  val referralTable = "edusio_referral_commisions"
  val cooperationTable = "edusio_school_instructor_cooperation"
  val substitutionTable = "edusio_events_susbstitution_rooster"
  val paymentRequest = "edusio_payment_requests"
  val paymentRequestMembership = "edsuio_payment_membership"
  val postsTable = "wp_posts"
  val usersTable = "wp_users"
}
