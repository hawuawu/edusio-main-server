package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTask, DatabaseTaskResult}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 7. 5. 2015.
 */
case object QueryAllContracts extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT *  FROM `edusio_school_instructor_cooperation` WHERE `contract_type`='fixed' AND `contract_flow` = 'school-to-instructor';"

      val rs = stmt.executeQuery(sql)

      var result: List[CooperationContract] = List()

      while (rs.next()) {
        val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS")
        val time = formatter.parseDateTime(rs.getString("time"))

        result = CooperationContract(
          rs.getInt("id"),
          rs.getInt("school_id"),
          rs.getInt("instructor_id"),
          rs.getFloat("provision"),
          time
        ) :: result
      }

      rs.close()
      stmt.close()
      CooperationContracts(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryAllContracts: " + t)
        DatabaseTaskError
    }
  }
}

case class CooperationContract(id: Int, fromUser: Int, toUser: Int, commission: Float, time: DateTime)

case class CooperationContracts(contracts: List[CooperationContract]) extends DatabaseTaskResult