package com.edusio.tsp.database.tasks.monthly

import java.security.MessageDigest
import java.sql.{SQLNonTransientConnectionException, Connection}
import java.util.Base64

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTask, DatabaseTaskError, DatabaseTaskResult}
import com.edusio.tsp.statistic.monthly.generator.{MonthlyStatisticGenerateAbleMessage, MonthlyStatisticJsonSerializeable}
import com.edusio.tsp.tools.DateTimeFromMysql
import org.joda.time.DateTime

import org.json4s.JsonDSL._

/**
 * Created by Wlsek on 4. 11. 2015.
 */


case class EdusioPaymentForCourse(courseTitle: String, courseId: Int, paid: Float, commissionRelation: CommissionRelation.Value, toUser: Int, studentId: Int, orderDate: DateTime) extends MonthlyStatisticJsonSerializeable with MonthlyStatisticGenerateAbleMessage {
  def serialize = {
    ("courseTitle" -> courseTitle) ~
      ("courseId" -> courseId) ~
      ("paid" -> paid) ~
      ("commissionRelation" -> commissionRelation.toString) ~
      ("toUser" -> toUser) ~
      ("studentId" -> studentId) ~
      ("orderDate" -> orderDate.getMillis)
  }
}

case class EdusioPaymentsForCourse(payments: List[EdusioPaymentForCourse], hash: Option[String]) extends DatabaseTaskResult

case class QueryEdusioPaymentsForCourse(limit: Int, offset: Int, timeAnchor: DateTime) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      val sql = "SELECT courses.post_title as course_title, " +
        "courses.id as course_id, " +
        "commissions.commission, " +
        "commissions.commission_relation, " +
        "commissions.user_id," +
        "customer_meta.meta_value as student_id, " +
        "orders.post_date as order_date, " +
        "MD5(CONCAT(courses.post_title, courses.id, commissions.commission, commissions.commission_relation, commissions.user_id, customer_meta.meta_value, orders.post_date)) as hash " +
        "FROM `edusio_commissions_to_pay`  as commissions " +
        "JOIN edusio_transactions as transactions ON transactions.id = commissions.transaction_id " +
        "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
        "JOIN wp_posts as courses ON courses.ID = commissions.course_id " +
        "JOIN wp_postmeta as customer_meta ON customer_meta.post_id = transactions.order_id " +
        "WHERE commission_bond_to = 'course' " +
        "AND transactions.transaction_done = 1 " +
        "AND customer_meta.meta_key = '_customer_user' " +
        "AND MONTH(orders.post_date) = " + timeAnchor.getMonthOfYear + " " +
        "AND commissions.commission_relation IN ('"+CommissionRelation.REFERRAL.toString()+"') " +
        "ORDER BY orders.post_date, orders.ID, commissions.id, commissions.user_id LIMIT " + limit + " OFFSET " + offset

      val rs = stmt.executeQuery(sql)

      var result: List[EdusioPaymentForCourse] = List()
      var hash: Option[String] = None

      while (rs.next()) {

        val orderCreated: Option[DateTime] = DateTimeFromMysql(rs.getString("order_date"), Some(log))

        result = EdusioPaymentForCourse(
          rs.getString("course_title"),
          rs.getInt("course_id"),
          rs.getFloat("commission"),
          CommissionRelation.withName(rs.getString("commission_relation")),
          rs.getInt("user_id"),
          rs.getInt("student_id"),
          orderCreated.getOrElse(new DateTime(0L))
        ) :: result
        if (hash.isEmpty) {
          hash = Some(rs.getString("hash"))
        } else {
          hash = Some(hash.get + rs.getString("hash"))
        }
      }
      hash = hash.map(h => {
        Base64.getEncoder.encodeToString(MessageDigest.getInstance("MD5").digest(h.getBytes("UTF-8")))
      })
      rs.close()
      stmt.close()
      EdusioPaymentsForCourse(result, hash)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryEdusioPaymentsForCourse: " + t)
        DatabaseTaskError
    }
  }
}


case class EdusioPaymentsForCourseHash(hash: Option[String]) extends DatabaseTaskResult

case class QueryEdusioPaymentsForCourseHash(limit: Int, offset: Int, timeAnchor: DateTime) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      val sql = "SELECT " +
        "MD5(CONCAT(courses.post_title, courses.id, commissions.commission, commissions.commission_relation, commissions.user_id, customer_meta.meta_value, orders.post_date)) as hash " +
        "FROM `edusio_commissions_to_pay` as commissions " +
        "JOIN edusio_transactions as transactions ON transactions.id = commissions.transaction_id " +
        "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
        "JOIN wp_posts as courses ON courses.ID = commissions.course_id " +
        "JOIN wp_postmeta as customer_meta ON customer_meta.post_id = transactions.order_id " +
        "WHERE commission_bond_to = 'course' " +
        "AND transactions.transaction_done = 1 " +
        "AND customer_meta.meta_key = '_customer_user' " +
        "AND MONTH(orders.post_date) = " + timeAnchor.getMonthOfYear + " " +
        "AND commissions.commission_relation IN ('"+CommissionRelation.REFERRAL.toString()+"') " +
        "ORDER BY orders.post_date, orders.ID, commissions.id, commissions.user_id  LIMIT " + limit + " OFFSET " + offset

      val rs = stmt.executeQuery(sql)

      var hash: Option[String] = None

      while (rs.next()) {
        if (hash.isEmpty) {
          hash = Some(rs.getString("hash"))
        } else {
          hash = Some(hash.get + rs.getString("hash"))
        }
      }
      hash = hash.map(h => {
        Base64.getEncoder.encodeToString(MessageDigest.getInstance("MD5").digest(h.getBytes("UTF-8")))
      })
      rs.close()
      stmt.close()
      EdusioPaymentsForCourseHash(hash)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryEdusioPaymentsForCourseHash: " + t)
        DatabaseTaskError
    }
  }
}