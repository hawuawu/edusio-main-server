package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseConfigs, DatabaseTaskError, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 5. 5. 2015.
 */
case class QueryUserBatch(limit: Int, offset: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT * FROM "+DatabaseConfigs.usersTable+" LIMIT "+limit+" OFFSET " + offset

      val rs = stmt.executeQuery(sql)

      var result: List[QueriedUser] = List()

      while (rs.next()) {
        result = QueriedUser(rs.getInt("ID"), rs.getString("user_nicename"), rs.getString("display_name")) :: result
      }
      rs.close()
      stmt.close()
      QueriedUsers(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError

      case t: Throwable =>
        log.error("Error QueryUserBatch: " + t)
        DatabaseTaskError
    }
  }
}

case class QueryUserDetailsForId(id: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT * FROM "+DatabaseConfigs.usersTable+" WHERE ID=" + id

      val rs = stmt.executeQuery(sql)

      var result: List[QueriedUser] = List()

      while (rs.next()) {
        result = QueriedUser(rs.getInt("ID"), rs.getString("user_nicename"), rs.getString("display_name")) :: result
      }
      rs.close()
      stmt.close()
      QueriedUsers(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError

      case t: Throwable =>
        log.error("Error QueryUserDetailsForId: " + t)
        DatabaseTaskError
    }
  }
}

case class QueriedUser(id: Int, userName: String, displayName: String)
case class QueriedUsers(users: List[QueriedUser]) extends DatabaseTaskResult
