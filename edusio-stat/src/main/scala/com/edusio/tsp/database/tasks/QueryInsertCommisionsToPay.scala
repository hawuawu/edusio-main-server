package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult, DatabaseTask}
import org.joda.time.DateTime

/**
 * Created by Wlsek on 4. 5. 2015.
 */

case class Commision(courseId: Int, userId: Int, transactionId: Int, fromUser: Int, commision: Float, commisionBoundTo: String, boundToId: Option[Int], commissionRelation: String)

case class QueryInsertCommisionToPay(commision: Commision) extends DatabaseTask {
  override def doTask(conn: Connection): DatabaseTaskResult = {
    try {
      val stmt = conn.createStatement()
      val sql = "INSERT INTO " + DatabaseConfigs.commisionsTable + "(`course_id`, `user_id`, `from_user`, `commission`, `transaction_id`, `commission_bond_to`"+
        commision.boundToId.map(id => ", `bound_to_id`").getOrElse("") +", `commission_relation`, `created`) VALUES "+
        "("+commision.courseId+", "+commision.userId+", "+ commision.fromUser + ", "+commision.commision+"," +commision.transactionId+ ", '"+commision.commisionBoundTo+"'"+commision.boundToId.map(id => ", " + id.toString + " ").getOrElse("")+", '"+commision.commissionRelation+"', '"+new DateTime().toString("yyyy-MM-dd HH:mm:ss")+"')"

      log.info(sql)
      val rs = stmt.executeUpdate(sql)

      stmt.close()

      var result: Option[Int] = None

      if(rs == 1) {
        InsertCommisionToPayDone(commision)
      } else {
        DatabaseTaskError
      }
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryInsertCommisionToPay: " + t)
        DatabaseTaskError
    }
  }
}


case class InsertCommisionToPayDone(commision: Commision) extends DatabaseTaskResult
