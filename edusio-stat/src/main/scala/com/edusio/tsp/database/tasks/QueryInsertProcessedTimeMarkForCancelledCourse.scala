package com.edusio.tsp.database.tasks

import java.sql.{Connection, SQLNonTransientConnectionException}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 13. 10. 2015.
 */
case class QueryInsertProcessedTimeMarkForCancelledCourse(courseId: Int, timeMark: String) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {

      val stmt = connection.createStatement()
      val sql = "INSERT INTO `edusio_cancel_course_processed`(`course_id`, `cancelled_time_mark`, `processed`) VALUES ("+courseId+",'"+timeMark+"',1)"
      val rs = stmt.executeUpdate(sql)
      stmt.close()
      ProcessedCourseAndTimeMarkInserted(courseId, timeMark)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryInsertProcessedTimeMarkForCancelledCourse: " + t)
        DatabaseTaskError
    }
  }
}


case class ProcessedCourseAndTimeMarkInserted(courseId: Int, timeMark: String) extends DatabaseTaskResult