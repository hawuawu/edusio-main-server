package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 4. 5. 2015.
 */

case object QueryNotCompletedTransactions extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement();
      val sql = "SELECT * FROM " + DatabaseConfigs.transactionTable + " WHERE transaction_done=0"
      val rs = stmt.executeQuery(sql)

      var result: List[NotCompletedTransaction] = List();
      while (rs.next()) {
        result = NotCompletedTransaction(rs.getInt("id"), rs.getInt("order_id"), rs.getInt("course_id"), rs.getLong("time"), rs.getString("status"), rs.getFloat("value")) :: result
      }

      rs.close()
      stmt.close()

      NotCompletedTransactions(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryNotCompletedTransactions: " + t)
        DatabaseTaskError
    }
  }
}

case class NotCompletedTransaction(transactionId: Int, orderId: Int, courseId: Int, time: Long, status: String, value: Float)

case class NotCompletedTransactions(transactions: List[NotCompletedTransaction]) extends DatabaseTaskResult