package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseConfigs, DatabaseTaskResult, DatabaseTaskError, DatabaseTask}

/**
 * Created by Wlsek on 8. 5. 2015.
 */
case class QueryInsertTransactionCommissionToPayment(transaction: TransactionCommission, request: PaymentRequest)  extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "INSERT INTO `"+DatabaseConfigs.paymentRequestMembership+"`(`transaction_id`, `transaction_type`, `request_id`) VALUES ("+transaction.transactionId+",'commission',"+request.id+")  ON DUPLICATE KEY UPDATE transaction_type=transaction_type"
      val rs = stmt.executeUpdate(sql)

      stmt.close()
      if(rs > 0) {
        InsertedTransactionCommissionToPayment(transaction)
      } else {
        DatabaseTaskError
      }

    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryInsertTransactionCommissionToPayment: " + t)
        DatabaseTaskError
    }
  }
}

case class InsertedTransactionCommissionToPayment(transaction: TransactionCommission) extends DatabaseTaskResult