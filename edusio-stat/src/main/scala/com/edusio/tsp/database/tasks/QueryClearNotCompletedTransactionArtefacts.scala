package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTask, DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult}

/**
 * Created by Wlsek on 4. 5. 2015.
 */
case class QueryClearNotCompletedTransactionArtefacts(transactionId: Int) extends DatabaseTask {
  override def doTask(conn: Connection): DatabaseTaskResult = {
    try {
      val stmt = conn.createStatement()
      val sql = "DELETE FROM " + DatabaseConfigs.commisionsTable + " WHERE transaction_id=" + transactionId
      val rs = stmt.execute(sql)
      stmt.close

      ClearNotCompletedTransactionArtefactsComplete
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError

      case t: Throwable =>
        log.error("Error QueryClearNotCompletedTransactionArtefacts: " + t)
        DatabaseTaskError
    }
  }
}

case object ClearNotCompletedTransactionArtefactsComplete extends DatabaseTaskResult
