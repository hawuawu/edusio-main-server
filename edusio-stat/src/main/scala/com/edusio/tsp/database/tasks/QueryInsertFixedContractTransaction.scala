package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseConfigs, DatabaseTaskError, DatabaseTaskResult, DatabaseTask}
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 7. 5. 2015.
 */
case class QueryInsertFixedContractTransaction(transaction: FixedContractTransaction) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "INSERT INTO `"+DatabaseConfigs.fixedCommissionsTable+"`(`contract_id`, `commission`, `from_user`, `to_user`, `created`) VALUES "+
        "(" + transaction.contractId + "," + transaction.commission + "," + transaction.fromUser + "," + transaction.toUser + ", '" + transaction.date.toString("yyyy-MM-dd HH:mm:ss") +"' )"

      val rs = stmt.executeUpdate(sql)

      stmt.close()
      if(rs > 0) {
        FixedContractTransactionInserted(transaction)
      } else {
        CannotInsertFixedContractTransaction(transaction)
      }

    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryInsertFixedContractTransaction: " + t)
        CannotInsertFixedContractTransaction(transaction)
    }
  }
}
case class CannotInsertFixedContractTransaction(transaction: FixedContractTransaction) extends DatabaseTaskResult
case class FixedContractTransactionInserted(transaction: FixedContractTransaction) extends DatabaseTaskResult