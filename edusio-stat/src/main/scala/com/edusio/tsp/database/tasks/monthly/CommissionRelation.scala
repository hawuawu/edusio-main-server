package com.edusio.tsp.database.tasks.monthly

/**
 * Created by Wlsek on 30. 10. 2015.
 */
object CommissionRelation extends Enumeration {
  val REFERRAL = Value("referral")
  val SCHOOL_FIXED = Value("school_fixed")
  val SCHOOL_PERCENTAGE = Value("school_percentage")
  val EDUSIO = Value("edusio")
  val SUBSTITUTION = Value("substitution")
  val INSTRUCTING = Value("instructing")

  @Deprecated
  val COOPERATION = Value("cooperation")
}