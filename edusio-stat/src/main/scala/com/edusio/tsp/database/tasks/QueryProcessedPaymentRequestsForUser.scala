package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult, DatabaseTask}
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 12. 5. 2015.
 */
case class QueryProcessedPaymentRequestsForUser(userId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT * FROM `"+DatabaseConfigs.paymentRequest+"` WHERE processed = 1 AND user_id="+userId+";"

      val rs = stmt.executeQuery(sql)

      var result: List[PaymentRequest] = List()

      while (rs.next()) {
        val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS")
        val time = formatter.parseDateTime(   rs.getString("created"))

        result = PaymentRequest(
          rs.getInt("id"),
          time,
          rs.getInt("user_id"),
          rs.getFloat("amount")
        ) :: result
      }

      rs.close()
      stmt.close()
      ProcessedPaymentRequestsForUser(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryAllUnprocessedPaymentRequests: " + t)
        DatabaseTaskError
    }
  }
}

case class ProcessedPaymentRequestsForUser(requests: List[PaymentRequest]) extends DatabaseTaskResult