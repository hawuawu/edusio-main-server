package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTask, DatabaseTaskError, DatabaseTaskResult}

/**
 * Created by Wlsek on 16. 11. 2015.
 */


object PointOfViewCommissionRole extends Enumeration {
  val Primaryffiliate, SecondaryAffiliate, School, Instructor = Value
}

trait AscendOffsetResult

case object OffsetSettingDone extends AscendOffsetResult

trait OffsetSetting extends AscendOffsetResult {
  val offsetGain: Int

  def default: OffsetSetting

  def increaseOffset: OffsetSetting

  def ascendOffset: AscendOffsetResult
}


trait IncreasingOffset

case object IncreasingSecondaryAffiliate extends IncreasingOffset

case object IncreasingSecondaryAffiliateInstructors extends IncreasingOffset

case object IncreasingSecondaryAffiliateInstructorCourses extends IncreasingOffset

case class PrimaryAffiliateOffsetSettings(
                                           offsetGain: Int,
                                           sAOffsetPair: Pair[Int, Int] = 0 -> 0,
                                           sAInstructorOffsetPair: Pair[Int, Int] = 0 -> 0,
                                           sAInstructorCoursesOffsetPair: Pair[Int, Int] = 0 -> 0,
                                           actualIncreasingOffset: IncreasingOffset = IncreasingSecondaryAffiliateInstructorCourses
                                           ) extends OffsetSetting {


  def default = PrimaryAffiliateOffsetSettings(offsetGain, offsetGain -> 0, offsetGain -> 0, offsetGain -> 0, IncreasingSecondaryAffiliateInstructorCourses)

  def increaseOffset = {
    PrimaryAffiliateOffsetSettings(offsetGain, sAOffsetPair, sAInstructorOffsetPair, sAInstructorCoursesOffsetPair._1 -> (sAInstructorCoursesOffsetPair._2 + offsetGain), IncreasingSecondaryAffiliateInstructorCourses)
  }

  def ascendOffset: AscendOffsetResult = {
    actualIncreasingOffset match {
      case IncreasingSecondaryAffiliate =>
        OffsetSettingDone

      case IncreasingSecondaryAffiliateInstructors =>
        PrimaryAffiliateOffsetSettings(offsetGain, sAOffsetPair._1 -> (sAOffsetPair._2 + offsetGain), offsetGain -> 0, offsetGain -> 0, IncreasingSecondaryAffiliate)

      case IncreasingSecondaryAffiliateInstructorCourses =>
        PrimaryAffiliateOffsetSettings(offsetGain, sAOffsetPair, sAInstructorOffsetPair._1 -> (sAInstructorOffsetPair._2 + offsetGain), offsetGain -> 0, IncreasingSecondaryAffiliateInstructors)

    }
  }

}

case object IncreasingAffiliateInstructors extends IncreasingOffset

case object IncreasingAffiliateInstructorCourses extends IncreasingOffset

case class SecondaryAffiliateOffsetSettings(
                                             offsetGain: Int,
                                             directInstructorOffsetPair: Pair[Int, Int] = 0 -> 0,
                                             directInstructorCoursesOffsetPair: Pair[Int, Int] = 0 -> 0,
                                             actualIncreasingOffset: IncreasingOffset = IncreasingAffiliateInstructorCourses
                                             ) extends OffsetSetting {

  def default = SecondaryAffiliateOffsetSettings(offsetGain, offsetGain -> 0, offsetGain -> 0, IncreasingAffiliateInstructorCourses)

  def increaseOffset = SecondaryAffiliateOffsetSettings(offsetGain, directInstructorOffsetPair, directInstructorCoursesOffsetPair._1 -> (directInstructorCoursesOffsetPair._2 + offsetGain), IncreasingAffiliateInstructorCourses)

  def ascendOffset: AscendOffsetResult = {
    actualIncreasingOffset match {
      case IncreasingAffiliateInstructors =>
        OffsetSettingDone

      case IncreasingAffiliateInstructorCourses =>
        SecondaryAffiliateOffsetSettings(offsetGain, directInstructorOffsetPair._1 -> (directInstructorOffsetPair._2 + offsetGain), directInstructorCoursesOffsetPair._1 -> 0, IncreasingAffiliateInstructors)
    }
  }

}

case object IncreasingSchoolInstructors extends IncreasingOffset

case object IncreasingSchoolInstructorCourses extends IncreasingOffset

case class SchoolOffsetSettings(
                                 offsetGain: Int,
                                 directInstructorOffsetPair: Pair[Int, Int] = 0 -> 0,
                                 directInstructorCoursesOffsetPair: Pair[Int, Int] = 0 -> 0,
                                 actualIncreasingOffset: IncreasingOffset = IncreasingSchoolInstructorCourses
                                 ) extends OffsetSetting {

  def default = SchoolOffsetSettings(offsetGain, offsetGain -> 0, offsetGain -> 0)

  def increaseOffset = SchoolOffsetSettings(offsetGain, directInstructorOffsetPair, directInstructorCoursesOffsetPair._1 -> (directInstructorCoursesOffsetPair._2 + offsetGain), IncreasingSchoolInstructorCourses)

  def ascendOffset: AscendOffsetResult = {
    actualIncreasingOffset match {
      case IncreasingSchoolInstructors =>
        OffsetSettingDone

      case IncreasingSchoolInstructorCourses =>
        SchoolOffsetSettings(offsetGain, directInstructorOffsetPair._1 -> (directInstructorOffsetPair._2 + offsetGain), directInstructorCoursesOffsetPair._1 -> 0, IncreasingSchoolInstructors)
    }
  }

}

case object IncreasingInstructorCourses extends IncreasingOffset

case class InstructorOffsetSettings(
                                     offsetGain: Int,
                                     directInstructorCoursesOffsetPair: Pair[Int, Int] = 0 -> 0,
                                     actualIncreasingOffset: IncreasingOffset = IncreasingInstructorCourses
                                     ) extends OffsetSetting {

  def default = InstructorOffsetSettings(offsetGain, offsetGain -> 0, IncreasingInstructorCourses)

  def increaseOffset = InstructorOffsetSettings(offsetGain, directInstructorCoursesOffsetPair._1 -> (directInstructorCoursesOffsetPair._2 + offsetGain), IncreasingInstructorCourses)


  def ascendOffset: AscendOffsetResult = {
    actualIncreasingOffset match {
      case IncreasingInstructorCourses =>
        OffsetSettingDone
    }
  }
}

case class PrimaryAffiliateResult(secondaryAffiliates: List[SecondaryAffiliate]) extends DatabaseTaskResult

case class SecondaryAffiliate(id: Int, displayName: String, directInstructors: List[DirectInstructor], commission: Float, month: Int, year: Int)

case class SecondaryAffiliateResult(directInstructors: List[DirectInstructor]) extends DatabaseTaskResult

case class SchoolResult(directInstructors: List[DirectInstructor]) extends DatabaseTaskResult

case class DirectInstructorResult(courses: List[CourseCommissionPair]) extends DatabaseTaskResult

case class DirectInstructor(id: Int, displayName: String, courses: List[CourseCommissionPair], commission: Float, month: Int, year: Int)

case class CourseCommissionPair(courseId: Int, courseTitle: String, commission: Float, month: Int, year: Int)

case class QueryMonthlyCommissionFromPOV(povUser: Int, povRole: PointOfViewCommissionRole.Value, offsetSettings: OffsetSetting, numberOfPastMonths: Int) extends DatabaseTask {
  override def doTask(conn: Connection): DatabaseTaskResult = {
    povRole match {
      case PointOfViewCommissionRole.Primaryffiliate =>
        try {
          val os = offsetSettings.asInstanceOf[PrimaryAffiliateOffsetSettings]

          val firstStatement = conn.createStatement()
          val firstSql = "SELECT users.display_name, " +
            "users.user_login, " +
            "users.id, " +
            "SUM(payments.commission) as value, " +
            "MONTH(orders.post_date) as month, " +
            "YEAR(orders.post_date) as year " +
            "FROM `edusio_referral_commisions` as affiliates " +
            "JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id " +
            "JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id " +
            "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
            "JOIN wp_users as users ON users.ID = affiliates.child_user_id " +
            "WHERE affiliates.refer_type = \"instructor_manager\" " +
            "AND affiliates.parent_user_id = " + povUser + " " +
            "AND affiliates.child_user_id = payments.from_user " +
            "AND orders.post_date >= (NOW() - INTERVAL " + numberOfPastMonths + " MONTH) " +
            "GROUP BY payments.from_user, year, month " +
            "ORDER BY value " +
            "LIMIT " + os.sAOffsetPair._1 + " OFFSET " + os.sAOffsetPair._2

          var secondaryAffiliates: List[SecondaryAffiliate] = List()

          val saResult = firstStatement.executeQuery(firstSql)
          while (saResult.next()) {
            val displayNameSecondaryAffiliate = saResult.getString("display_name")
            val secondaryAffiliateValue = saResult.getFloat("value")
            if (!saResult.wasNull()) {
              val sa = SecondaryAffiliate(
                saResult.getInt("id"),
                if (displayNameSecondaryAffiliate == "") {
                  saResult.getString("user_login")
                } else {
                  displayNameSecondaryAffiliate
                },
                List(),
                secondaryAffiliateValue,
                saResult.getInt("month"),
                saResult.getInt("year")
              )

              val secondStatement = conn.createStatement()
              val secondQuery = "SELECT users.display_name, " +
                "users.user_login, " +
                "users.id, " +
                "(" +
                "SELECT SUM(innerPayments.commission) FROM edusio_commissions_to_pay as innerPayments " +
                "WHERE innerPayments.transaction_id = transactions.id AND innerPayments.user_id = " + povUser +
                ") as value, " +
                "MONTH(orders.post_date) as month, " +
                "YEAR(orders.post_date) as year FROM `edusio_referral_commisions` as affiliates " +
                "JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id " +
                "JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id " +
                "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
                "JOIN wp_users as users ON users.ID = affiliates.child_user_id " +
                "WHERE affiliates.refer_type = \"instructor\" " +
                "AND affiliates.parent_user_id = " + sa.id + " " +
                "AND affiliates.child_user_id = payments.from_user " +
                "AND orders.post_date >= (NOW() - INTERVAL " + numberOfPastMonths + " MONTH) " +
                "GROUP BY payments.from_user, year, month " +
                "LIMIT " + os.sAInstructorOffsetPair._1 + " OFFSET " + os.sAInstructorOffsetPair._2

              var secondaryAffiliateInstructors: List[DirectInstructor] = List()
              val sAIResult = secondStatement.executeQuery(secondQuery)
              while (sAIResult.next()) {
                val directInstructorDisplayName = sAIResult.getString("display_name")
                val directInstructorValue = sAIResult.getFloat("value")
                if (!sAIResult.wasNull()) {
                  val sai = DirectInstructor(
                    sAIResult.getInt("id"),
                    if (directInstructorDisplayName == "") {
                      sAIResult.getString("user_login")
                    } else {
                      directInstructorDisplayName
                    },
                    List(),
                    directInstructorValue,
                    sAIResult.getInt("month"),
                    sAIResult.getInt("year")
                  )

                  val thirdStatement = conn.createStatement()
                  val thirdQuery = "SELECT courses.post_title, " +
                    "courses.id, " +
                    "(" +
                    "SELECT SUM(innerPayments.commission) " +
                    "FROM edusio_commissions_to_pay as innerPayments " +
                    "WHERE innerPayments.transaction_id = transactions.id " +
                    "AND innerPayments.user_id = " + povUser + " AND innerPayments.course_id = payments.course_id " +
                    ") as value, " +
                    "MONTH(orders.post_date) as month, " +
                    "YEAR(orders.post_date) as year " +
                    "FROM `edusio_referral_commisions` as affiliates " +
                    "JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id " +
                    "JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id " +
                    "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
                    "JOIN wp_users as users ON users.ID = affiliates.child_user_id " +
                    "JOIN wp_posts as courses ON courses.id = payments.course_id " +
                    "WHERE affiliates.refer_type = \"instructor\" " +
                    "AND affiliates.parent_user_id = " + sa.id + " " +
                    "AND affiliates.child_user_id = payments.from_user " +
                    "AND orders.post_date >= (NOW() - INTERVAL " + numberOfPastMonths + " MONTH) " +
                    "GROUP BY payments.course_id, year, month " +
                    "ORDER BY value " +
                    "LIMIT " + os.sAInstructorCoursesOffsetPair._1 + " OFFSET " + os.sAInstructorCoursesOffsetPair._2

                  var secondaryAffiliateInstructorCourses: List[CourseCommissionPair] = List()
                  val sAICoursesResult = thirdStatement.executeQuery(thirdQuery)
                  while (sAICoursesResult.next()) {
                    val courseValue = sAICoursesResult.getFloat("value")
                    if (!sAICoursesResult.wasNull()) {
                      secondaryAffiliateInstructorCourses =
                        CourseCommissionPair(
                          sAICoursesResult.getInt("id"),
                          sAICoursesResult.getString("post_title"),
                          courseValue,
                          sAICoursesResult.getInt("month"),
                          sAICoursesResult.getInt("year")
                        ) :: secondaryAffiliateInstructorCourses
                    }
                  }

                  thirdStatement.close()
                  if (secondaryAffiliateInstructorCourses.size > 0) {
                    secondaryAffiliateInstructors = sai.copy(courses = secondaryAffiliateInstructorCourses) :: secondaryAffiliateInstructors
                  }
                }
              }

              secondStatement.close()
              if (secondaryAffiliateInstructors.size > 0) {
                secondaryAffiliates = sa.copy(directInstructors = secondaryAffiliateInstructors) :: secondaryAffiliates
              }

            }
          }

          firstStatement.close()
          PrimaryAffiliateResult(secondaryAffiliates)
        } catch {
          case e: SQLNonTransientConnectionException =>
            log.error("Error in database, restarting whole connector.")
            EntryPointService.restart
            DatabaseTaskError
          case t: Throwable =>
            log.error("Error QueryMonthlyCommissionFromPOV: " + t)
            DatabaseTaskError
        }



      //Primary - Secondary
      /*
SELECT users.display_name, users.user_login,SUM(payments.commission), MONTH(orders.post_date) as month, YEAR(orders.post_date) as year FROM `edusio_referral_commisions` as affiliates
JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id
JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id
JOIN wp_posts as orders ON orders.id = transactions.order_id
JOIN wp_users as users ON users.ID = affiliates.child_user_id
WHERE affiliates.refer_type = "instructor_manager"
AND affiliates.parent_user_id = 25
AND affiliates.child_user_id = payments.from_user
AND MONTH(orders.post_date) >= MONTH(NOW()) - 3
GROUP BY payments.from_user, year, month
     */

      //Secondary - Instructors
      /*
     SELECT users.display_name, users.user_login, (SELECT SUM(innerPayments.commission) FROM edusio_commissions_to_pay as innerPayments WHERE innerPayments.transaction_id = transactions.id AND innerPayments.user_id = 25) as value, MONTH(orders.post_date) as month, YEAR(orders.post_date) as year FROM `edusio_referral_commisions` as affiliates
JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id
JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id
JOIN wp_posts as orders ON orders.id = transactions.order_id
JOIN wp_users as users ON users.ID = affiliates.child_user_id
WHERE affiliates.refer_type = "instructor"
AND affiliates.parent_user_id = 2
AND affiliates.child_user_id = payments.from_user
AND MONTH(orders.post_date) >= MONTH(NOW()) - 3
GROUP BY payments.from_user, year, month
     */

      //Instructors - Courses
      /*
    SELECT courses.post_title, (SELECT SUM(innerPayments.commission) FROM edusio_commissions_to_pay as innerPayments WHERE innerPayments.transaction_id = transactions.id AND innerPayments.user_id = 25 AND innerPayments.course_id = payments.course_id) as value, MONTH(orders.post_date) as month, YEAR(orders.post_date) as year FROM `edusio_referral_commisions` as affiliates
    JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id
    JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id
    JOIN wp_posts as orders ON orders.id = transactions.order_id
    JOIN wp_users as users ON users.ID = affiliates.child_user_id
    JOIN wp_posts as courses ON courses.id = payments.course_id
    WHERE affiliates.refer_type = "instructor"
    AND affiliates.parent_user_id = 2
    AND affiliates.child_user_id = payments.from_user
    AND MONTH(orders.post_date) >= MONTH(NOW()) - 3
    GROUP BY payments.course_id, year, month
     */

      case PointOfViewCommissionRole.SecondaryAffiliate =>
        try {
          val firstStatement = conn.createStatement()
          val os = offsetSettings.asInstanceOf[SecondaryAffiliateOffsetSettings]

          val firstQuery = "SELECT users.display_name, " +
            "users.user_login, " +
            "users.ID, " +
            "SUM(payments.commission) as value, " +
            "MONTH(orders.post_date) as month, " +
            "YEAR(orders.post_date) as year " +
            "FROM `edusio_referral_commisions` as affiliates " +
            "JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id " +
            "JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id " +
            "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
            "JOIN wp_users as users ON users.ID = affiliates.child_user_id " +
            "WHERE affiliates.refer_type = \"instructor\" " +
            "AND affiliates.parent_user_id = " + povUser + " " +
            "AND affiliates.child_user_id = payments.from_user " +
            "AND orders.post_date >= (NOW() - INTERVAL 3 MONTH) " +
            "GROUP BY payments.from_user, year, month " +
            "ORDER BY value " +
            "LIMIT " + os.directInstructorOffsetPair._1 + " OFFSET " + os.directInstructorOffsetPair._2

          var secondaryAffiliateInstructors: List[DirectInstructor] = List()
          val sAIResult = firstStatement.executeQuery(firstQuery)
          while (sAIResult.next()) {
            val directInstructorDisplayName = sAIResult.getString("display_name")
            val directInstructorValue = sAIResult.getFloat("value")
            if (!sAIResult.wasNull()) {
              val sai = DirectInstructor(sAIResult.getInt("id"),
                if (directInstructorDisplayName == "") {
                  sAIResult.getString("user_login")
                } else {
                  directInstructorDisplayName
                },
                List(),
                directInstructorValue,
                sAIResult.getInt("month"),
                sAIResult.getInt("year")
              )

              val secondStatement = conn.createStatement()
              val secondQuery = "SELECT courses.post_title, " +
                "courses.id, " +
                "SUM(payments.commission) as value, " +
                "MONTH(orders.post_date) as month, " +
                "YEAR(orders.post_date) as year " +
                "FROM `edusio_referral_commisions` as affiliates " +
                "JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id " +
                "JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id " +
                "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
                "JOIN wp_posts as courses ON courses.id = payments.course_id " +
                "WHERE affiliates.refer_type = \"instructor\" " +
                "AND affiliates.parent_user_id = " + povUser + " " +
                "AND affiliates.child_user_id = payments.from_user " +
                "AND orders.post_date >= (NOW() - INTERVAL 3 MONTH) " +
                "GROUP BY payments.course_id, year, month " +
                "ORDER BY value " +
                "LIMIT " + os.directInstructorCoursesOffsetPair._1 + " OFFSET " + os.directInstructorCoursesOffsetPair._2

              var secondaryAffiliateInstructorCourses: List[CourseCommissionPair] = List()
              val sAICoursesResult = secondStatement.executeQuery(secondQuery)
              while (sAICoursesResult.next()) {
                val instructorCourseValue = sAICoursesResult.getFloat("value")
                if (!sAICoursesResult.wasNull()) {
                  secondaryAffiliateInstructorCourses =
                    CourseCommissionPair(
                      sAICoursesResult.getInt("id"),
                      sAICoursesResult.getString("post_title"),
                      instructorCourseValue,
                      sAICoursesResult.getInt("month"),
                      sAICoursesResult.getInt("year")
                    ) :: secondaryAffiliateInstructorCourses
                }
              }
              secondStatement.close()
              if (secondaryAffiliateInstructorCourses.size > 0) {
                secondaryAffiliateInstructors = sai.copy(courses = secondaryAffiliateInstructorCourses) :: secondaryAffiliateInstructors
              }
            }
          }

          firstStatement.close()
          SecondaryAffiliateResult(secondaryAffiliateInstructors)
        } catch {
          case e: SQLNonTransientConnectionException =>
            log.error("Error in database, restarting whole connector.")
            EntryPointService.restart
            DatabaseTaskError
          case t: Throwable =>
            log.error("Error QueryMonthlyCommissionFromPOV: " + t)
            DatabaseTaskError
        }


      /*
      SELECT users.display_name, users.user_login,SUM(payments.commission), MONTH(orders.post_date) as month, YEAR(orders.post_date) as year FROM `edusio_referral_commisions` as affiliates
JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id
JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id
JOIN wp_posts as orders ON orders.id = transactions.order_id
JOIN wp_users as users ON users.ID = affiliates.child_user_id
WHERE affiliates.refer_type = "instructor"
AND affiliates.parent_user_id = 2
AND affiliates.child_user_id = payments.from_user
AND MONTH(orders.post_date) >= MONTH(NOW()) - 3
GROUP BY payments.from_user, year, month
     */

      /*
     SELECT courses.post_title,SUM(payments.commission), MONTH(orders.post_date) as month, YEAR(orders.post_date) as year FROM `edusio_referral_commisions` as affiliates
JOIN edusio_commissions_to_pay as payments ON payments.user_id = affiliates.parent_user_id
JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id
JOIN wp_posts as orders ON orders.id = transactions.order_id
JOIN wp_posts as courses ON courses.id = payments.course_id
WHERE affiliates.refer_type = "instructor"
AND affiliates.parent_user_id = 2
AND affiliates.child_user_id = payments.from_user
AND MONTH(orders.post_date) >= MONTH(NOW()) - 3
GROUP BY payments.course_id, year, month
     */

      case PointOfViewCommissionRole.School =>
        try {
          val os = offsetSettings.asInstanceOf[SchoolOffsetSettings]

          val firstStatement = conn.createStatement()
          val firstQuery = "SELECT users.ID, " +
            "users.display_name, " +
            "users.user_login, " +
            "MONTH(orders.post_date) as month, " +
            "YEAR(orders.post_date) as year, " +
            "SUM(payments.commission) as value FROM `edusio_commissions_to_pay` as payments " +
            "JOIN edusio_school_instructor_cooperation as cooperation ON cooperation.instructor_id = payments.from_user " +
            "JOIN wp_users as users ON users.ID = payments.from_user " +
            "JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id " +
            "JOIN wp_posts as orders ON orders.ID = transactions.order_id " +
            "WHERE orders.post_date >= (NOW() - INTERVAL 3 MONTH) " +
            "AND (payments.commission_relation = \"school_fixed\" OR payments.commission_relation = \"school_percentage\") " +
            "AND payments.user_id = " + povUser + " " +
            "GROUP BY payments.from_user, month, year " +
            "ORDER BY value " +
            "LIMIT " + os.directInstructorOffsetPair._1 + " OFFSET " + os.directInstructorOffsetPair._2

          var schoolInstructors: List[DirectInstructor] = List()
          val schoolInstructorsResult = firstStatement.executeQuery(firstQuery)
          while (schoolInstructorsResult.next()) {
            val schoolInstructorDisplayName = schoolInstructorsResult.getString("display_name")
            val schoolInstructorValue = schoolInstructorsResult.getFloat("value")
            if (!schoolInstructorsResult.wasNull()) {
              val schi = DirectInstructor(
                schoolInstructorsResult.getInt("id"),
                if (schoolInstructorDisplayName == "") {
                  schoolInstructorsResult.getString("user_login")
                } else {
                  schoolInstructorDisplayName
                },
                List(),
                schoolInstructorValue,
                schoolInstructorsResult.getInt("month"),
                schoolInstructorsResult.getInt("year")
              )

              val secondStatement = conn.createStatement()
              val secondQuery = "SELECT courses.ID, " +
                "courses.post_title, " +
                "MONTH(orders.post_date) as month, " +
                "YEAR(orders.post_date) as year, " +
                "SUM(payments.commission) as value FROM `edusio_commissions_to_pay` as payments " +
                "JOIN edusio_school_instructor_cooperation as cooperation ON cooperation.instructor_id = payments.from_user " +
                "JOIN wp_posts as courses ON courses.ID = payments.course_id " +
                "JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id " +
                "JOIN wp_posts as orders ON orders.ID = transactions.order_id " +
                "WHERE orders.post_date >= (NOW() - INTERVAL 3 MONTH) " +
                "AND (payments.commission_relation = \"school_fixed\" OR payments.commission_relation = \"school_percentage\") " +
                "AND payments.from_user = " + schi.id + " " +
                "AND payments.user_id = " + povUser + " " +
                "GROUP BY payments.course_id, month, year " +
                "ORDER BY value " +
                "LIMIT " + os.directInstructorCoursesOffsetPair._1 + " OFFSET " + os.directInstructorCoursesOffsetPair._2

              var schoolInstructorCourses: List[CourseCommissionPair] = List()
              val schoolInstructorCoursesResult = secondStatement.executeQuery(secondQuery)
              while (schoolInstructorCoursesResult.next()) {
                val value = schoolInstructorCoursesResult.getFloat("value")
                if (!schoolInstructorCoursesResult.wasNull()) {
                  schoolInstructorCourses =
                    CourseCommissionPair(
                      schoolInstructorCoursesResult.getInt("id"),
                      schoolInstructorCoursesResult.getString("post_title"),
                      value,
                      schoolInstructorCoursesResult.getInt("month"),
                      schoolInstructorCoursesResult.getInt("year")
                    ) :: schoolInstructorCourses
                }
              }

              secondStatement.close()
              if(schoolInstructorCourses.size > 0) {
                schoolInstructors = schi.copy(courses = schoolInstructorCourses) :: schoolInstructors
              }
            }
          }

          firstStatement.close()
          SchoolResult(schoolInstructors)
        } catch {
          case e: SQLNonTransientConnectionException =>
            log.error("Error in database, restarting whole connector.")
            EntryPointService.restart
            DatabaseTaskError
          case t: Throwable =>
            log.error("Error QueryMonthlyCommissionFromPOV: " + t)
            DatabaseTaskError
        }

      /*
    SELECT users.ID, users.display_name, MONTH(orders.post_date) as month, YEAR(orders.post_date) as year, SUM(payments.commission) as value  FROM `edusio_commissions_to_pay` as payments
JOIN edusio_school_instructor_cooperation as cooperation ON cooperation.instructor_id = payments.from_user
JOIN wp_users as users ON users.ID = payments.from_user
JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id
JOIN wp_posts as orders ON orders.ID = transactions.order_id
WHERE orders.post_date >= (NOW() - INTERVAL 3 MONTH)
AND (payments.commission_relation = "school_fixed" OR payments.commission_relation = "school_percentage")
AND payments.user_id = 2
GROUP BY payments.from_user, month, year
     */

      /*
    SELECT courses.ID, courses.post_title, MONTH(orders.post_date) as month, YEAR(orders.post_date) as year, SUM(payments.commission) as value  FROM `edusio_commissions_to_pay` as payments
JOIN edusio_school_instructor_cooperation as cooperation ON cooperation.instructor_id = payments.from_user
JOIN wp_posts as courses ON courses.ID = payments.course_id
JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id
JOIN wp_posts as orders ON orders.ID = transactions.order_id
WHERE orders.post_date >= (NOW() - INTERVAL 3 MONTH)
AND (payments.commission_relation = "school_fixed" OR payments.commission_relation = "school_percentage")
AND payments.from_user = 2
AND payments.user_id = 2
GROUP BY payments.course_id, month, year
ORDER BY value
     */

      case PointOfViewCommissionRole.Instructor =>

        try {
          val statement = conn.createStatement()
          val os = offsetSettings.asInstanceOf[InstructorOffsetSettings]

          val firstQuery = "SELECT courses.ID, " +
            "courses.post_title, " +
            "MONTH(orders.post_date) as month, " +
            "YEAR(orders.post_date) as year, " +
            "SUM(payments.commission) as value FROM `edusio_commissions_to_pay` as payments " +
            "JOIN wp_posts as courses ON courses.ID = payments.course_id " +
            "JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id " +
            "JOIN wp_posts as orders ON orders.ID = transactions.order_id " +
            "WHERE orders.post_date >= (NOW() - INTERVAL 3 MONTH) " +
            "AND (payments.commission_relation = \"instructing\" OR payments.commission_relation = \"substitution\") " +
            "AND payments.user_id = " + povUser + " " +
            "GROUP BY payments.course_id, month, year " +
            "ORDER BY value " +
            "LIMIT " + os.directInstructorCoursesOffsetPair._1 + " OFFSET " + os.directInstructorCoursesOffsetPair._2

          var instructorCourses: List[CourseCommissionPair] = List()
          val instructorCoursesResult = statement.executeQuery(firstQuery)
          while (instructorCoursesResult.next()) {
            val value = instructorCoursesResult.getFloat("value")
            if (!instructorCoursesResult.wasNull()) {
              instructorCourses =
                CourseCommissionPair(
                  instructorCoursesResult.getInt("id"),
                  instructorCoursesResult.getString("post_title"),
                  value,
                  instructorCoursesResult.getInt("month"),
                  instructorCoursesResult.getInt("year")
                ) :: instructorCourses
            }
          }


          statement.close()
          DirectInstructorResult(instructorCourses)
        } catch {
          case e: SQLNonTransientConnectionException =>
            log.error("Error in database, restarting whole connector.")
            EntryPointService.restart
            DatabaseTaskError
          case t: Throwable =>
            log.error("Error QueryMonthlyCommissionFromPOV: " + t)
            DatabaseTaskError
        }
      /*
    SELECT courses.ID, courses.post_title, MONTH(orders.post_date) as month, YEAR(orders.post_date) as year, SUM(payments.commission) as value  FROM `edusio_commissions_to_pay` as payments
JOIN edusio_school_instructor_cooperation as cooperation ON cooperation.instructor_id = payments.from_user
JOIN wp_posts as courses ON courses.ID = payments.course_id
JOIN edusio_transactions as transactions ON transactions.id = payments.transaction_id
JOIN wp_posts as orders ON orders.ID = transactions.order_id
WHERE orders.post_date >= (NOW() - INTERVAL 3 MONTH)
AND (payments.commission_relation = "instructing" OR payments.commission_relation = "substitution")
AND payments.user_id = 2
GROUP BY payments.course_id, month, year
ORDER BY value
     */
    }
  }
}
