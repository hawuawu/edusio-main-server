package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTask, DatabaseTaskResult}

/**
 * Created by Wlsek on 17. 11. 2015.
 */

object AffiliateLevel extends Enumeration {
  val PrimaryAffiliateLevel, SecondaryAffiliateLevel = Value
}

case class QueryAffiliateLevel(userId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sqlPrimaryAffiliate = "SELECT * FROM edusio_referral_commisions WHERE parent_user_id = " + userId + " AND refer_type = \"instructor_manager\""
      val sqlSecondaryAffiliate = "SELECT * FROM edusio_referral_commisions WHERE parent_user_id = " + userId + " AND refer_type = \"instructor\""

      var affiliateLevel: List[AffiliateLevel.Value] = List()

      if(stmt.executeQuery(sqlPrimaryAffiliate).next()) {
        affiliateLevel = AffiliateLevel.PrimaryAffiliateLevel :: affiliateLevel
      }

      if(stmt.executeQuery(sqlSecondaryAffiliate).next()) {
        affiliateLevel = AffiliateLevel.SecondaryAffiliateLevel :: affiliateLevel
      }

      stmt.close()
      QueriedAffiliateLevel(userId, affiliateLevel)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryAffiliateLevel: " + t)
        DatabaseTaskError
    }
  }
}

case class QueriedAffiliateLevel(userId: Int, affLevel: List[AffiliateLevel.Value]) extends DatabaseTaskResult