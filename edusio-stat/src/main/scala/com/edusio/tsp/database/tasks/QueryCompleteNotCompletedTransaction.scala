package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseTask, DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult}

/**
 * Created by Wlsek on 4. 5. 2015.
 */

case class QueryCompleteNotCompletedTransaction(transaction: NotCompletedTransaction) extends DatabaseTask {
  override def doTask(conn: Connection): DatabaseTaskResult = {
    try {
      val stmt = conn.createStatement()
      val sql = "UPDATE " + DatabaseConfigs.transactionTable + " SET transaction_done=1 WHERE id=" + transaction.transactionId + " AND status='" + transaction.status + "'"
      val rs = stmt.executeUpdate(sql)

      stmt.close
      if (rs > 0) {
        NotCompletedTransactionCompleted
      } else {
        DatabaseTaskError
      }
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryCompleteNotCompletedTransaction: " + t)
        DatabaseTaskError
    }
  }
}

case object NotCompletedTransactionCompleted extends DatabaseTaskResult