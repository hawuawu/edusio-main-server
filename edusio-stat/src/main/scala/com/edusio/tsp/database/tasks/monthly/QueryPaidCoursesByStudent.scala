package com.edusio.tsp.database.tasks.monthly

import java.security.MessageDigest
import java.sql.{SQLNonTransientConnectionException, Connection}
import java.util.Base64

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseConfigs, DatabaseTaskResult, DatabaseTask}
import com.edusio.tsp.statistic.monthly.generator.MonthlyStatisticJsonSerializeable
import com.edusio.tsp.tools.DateTimeFromMysql
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._

/**
 * Created by Wlsek on 29. 10. 2015.
 */

case class PaidCourseByStudent(customerId: Int, courseId: Int, courseTitle: String, instructorId: Int, orderCreated: DateTime, orderComplete: DateTime, total: Float) extends MonthlyStatisticJsonSerializeable {
  def serialize: JValue = {
    implicit lazy val formats = org.json4s.DefaultFormats

    ("customerId" -> customerId) ~
      ("courseId" -> courseId) ~
      ("courseTitle" -> courseTitle) ~
      ("instructorId" -> instructorId) ~
      ("orderCreated" -> orderCreated.getMillis) ~
      ("orderComplete" -> orderComplete.getMillis) ~
      ("total" -> total)
  }
}

case class PaidCoursesByStudent(courses: List[PaidCourseByStudent], hash: Option[String]) extends DatabaseTaskResult

case class QueryPaidCoursesByStudent(studentId: Int, limit: Int, offset: Int, timeAnchor: DateTime) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT meta_user.meta_value as customer, " +
        "meta_total.meta_value as total, " +
        "course.ID as course_id, " +
        "course.post_title, " +
        "course.post_author as instructor, " +
        "orders.post_date as order_created, " +
        "meta_completed.meta_value as order_completed, " +
        "md5(CONCAT(meta_user.meta_value, meta_total.meta_value, course.ID, course.post_title, course.post_author, orders.post_date, meta_completed.meta_value)) as hash " +
        "FROM `wp_posts` as orders " +
        "JOIN `wp_postmeta` as meta_user ON meta_user.post_id = orders.id " +
        "JOIN `wp_postmeta` as meta_total ON meta_total.post_id = meta_user.post_id " +
        "JOIN `wp_postmeta` as meta_completed ON meta_completed.post_id = meta_user.post_id " +
        "JOIN `wp_woocommerce_order_items` as order_items ON order_items.order_id = orders.id " +
        "JOIN `wp_woocommerce_order_itemmeta` as order_items_meta ON order_items_meta.order_item_id = order_items.order_item_id " +
        "JOIN `wp_postmeta` as course_meta ON course_meta.meta_value = order_items_meta.meta_value " +
        "JOIN `wp_posts` as course ON course.ID = course_meta.post_id " +
        "WHERE orders.post_type = 'shop_order'  " +
        "AND meta_user.meta_key = '_customer_user' " +
        "AND meta_total.meta_key = '_order_total' " +
        "AND order_items_meta.meta_key = '_product_id' " +
        "AND meta_completed.meta_key = '_completed_date' " +
        "AND course.post_type = 'course' " +
        "AND meta_user.meta_value = " + studentId + " " +
        "AND MONTH(STR_TO_DATE(meta_completed.meta_value, '%Y-%m-%d %H:%i:%s')) = "+timeAnchor.getMonthOfYear+" " +
        "ORDER BY order_created LIMIT " + limit + " OFFSET " + offset

      val rs =  stmt.executeQuery(sql)

      var result: List[PaidCourseByStudent] = List()
      var hash: Option[String] = None

      while (rs.next()) {

        val orderCreated: Option[DateTime] = DateTimeFromMysql(rs.getString("order_created"), Some(log))
        val orderCompleted: Option[DateTime] =  DateTimeFromMysql(rs.getString("order_completed"), Some(log))

        result = PaidCourseByStudent(
          rs.getInt("customer"),
          rs.getInt("course_id"),
          rs.getString("post_title"),
          rs.getInt("instructor"),
          orderCreated.map(c => c).getOrElse(new DateTime(0L)),
          orderCompleted.map(c => c).getOrElse(new DateTime(0L)),
          rs.getFloat("total")
        ) :: result
        if(hash.isEmpty) {
          hash = Some( rs.getString("hash"))
        } else {
          hash = Some( hash.get + rs.getString("hash"))
        }
      }
      hash = hash.map(h => {
        Base64.getEncoder.encodeToString( MessageDigest.getInstance("MD5").digest(h.getBytes("UTF-8")))
      })
      rs.close()
      stmt.close()
      PaidCoursesByStudent(result, hash)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryPaidCoursesByStudent: " + t)
        DatabaseTaskError
    }
  }
}


case class PaidCoursesByStudentHash(hash: Option[String]) extends DatabaseTaskResult

case class QueryPaidCoursesByStudentHash(studentId: Int, limit: Int, offset: Int, timeAnchor: DateTime) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT md5(CONCAT(meta_user.meta_value, meta_total.meta_value, course.ID, course.post_title, course.post_author, orders.post_date, meta_completed.meta_value)) as hash " +
        "FROM `wp_posts` as orders " +
        "JOIN `wp_postmeta` as meta_user ON meta_user.post_id = orders.id " +
        "JOIN `wp_postmeta` as meta_total ON meta_total.post_id = meta_user.post_id " +
        "JOIN `wp_postmeta` as meta_completed ON meta_completed.post_id = meta_user.post_id " +
        "JOIN `wp_woocommerce_order_items` as order_items ON order_items.order_id = orders.id " +
        "JOIN `wp_woocommerce_order_itemmeta` as order_items_meta ON order_items_meta.order_item_id = order_items.order_item_id " +
        "JOIN `wp_postmeta` as course_meta ON course_meta.meta_value = order_items_meta.meta_value " +
        "JOIN `wp_posts` as course ON course.ID = course_meta.post_id " +
        "WHERE orders.post_type = 'shop_order'  " +
        "AND meta_user.meta_key = '_customer_user' " +
        "AND meta_total.meta_key = '_order_total' " +
        "AND order_items_meta.meta_key = '_product_id' " +
        "AND meta_completed.meta_key = '_completed_date' " +
        "AND course.post_type = 'course' " +
        "AND meta_user.meta_value = " + studentId + " " +
        "AND MONTH(STR_TO_DATE(meta_completed.meta_value, '%Y-%m-%d %H:%i:%s')) = "+timeAnchor.getMonthOfYear+" "+
        "ORDER BY orders.post_date LIMIT " + limit + " OFFSET " + offset

      val rs = stmt.executeQuery(sql)

      var result: List[PaidCourseByStudent] = List()
      val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
      var hash: Option[String] = None
      while (rs.next()) {
        if(hash.isEmpty) {
          hash = Some( rs.getString("hash"))
        } else {
          hash = Some( hash.get + rs.getString("hash"))
        }
      }

      hash = hash.map(h => {
        Base64.getEncoder.encodeToString( MessageDigest.getInstance("MD5").digest(h.getBytes("UTF-8")))
      })

      rs.close()
      stmt.close()
      PaidCoursesByStudentHash(hash)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryPaidCoursesByStudent: " + t)
        DatabaseTaskError
    }
  }

}

