package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseConfigs, DatabaseTaskError, DatabaseTask, DatabaseTaskResult}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 8. 5. 2015.
 */
case object QueryAllUnprocessedPaymentRequests  extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT * FROM `"+DatabaseConfigs.paymentRequest+"` WHERE processed = 0;"

      val rs = stmt.executeQuery(sql)

      var result: List[PaymentRequest] = List()

      while (rs.next()) {
        val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS")
        val time = formatter.parseDateTime(   rs.getString("created"))

        result = PaymentRequest(
          rs.getInt("id"),
          time,
          rs.getInt("user_id"),
          rs.getFloat("amount")
        ) :: result
      }

      rs.close()
      stmt.close()
      PaymentRequests(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
      log.error("Error in database, restarting whole connector.")
      EntryPointService.restart
      DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryAllUnprocessedPaymentRequests: " + t)
        DatabaseTaskError
    }
  }
}

case class PaymentRequest(id: Int, created: DateTime, userId: Int, amount: Float)
case class PaymentRequests(requests: List[PaymentRequest]) extends DatabaseTaskResult