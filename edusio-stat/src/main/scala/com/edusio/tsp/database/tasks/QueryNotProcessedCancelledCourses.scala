package com.edusio.tsp.database.tasks

import java.sql.{Connection, SQLNonTransientConnectionException}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}

/**
 * Created by Wlsek on 13. 10. 2015.
 */

case object QueryNotProcessedCancelledCourses extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT transactions.id as transaction_id, transactions.order_id, transactions.course_id, "+
        "users.user_login, users.ID as user_id, meta.meta_value as timeMark "+
        "FROM edusio.wp_posts as courses "+
        "JOIN wp_postmeta as meta ON meta.post_id = courses.ID "+
        "LEFT JOIN edusio_services.edusio_cancel_course_processed as processed ON processed.course_id = courses.ID "+
        "JOIN edusio.edusio_transactions as transactions ON transactions.course_id = courses.ID "+
        "JOIN edusio.wp_posts as orders ON orders.ID = transactions.order_id "+
        "JOIN edusio.wp_postmeta as orders_meta ON orders_meta.post_id = orders.ID "+
        "JOIN edusio.wp_users as users ON users.ID = orders_meta.meta_value "+
        "WHERE courses.post_type = 'course' "+
        "AND meta.meta_key = 'edusio_cancelled' "+
        "AND processed.cancelled_time_mark IS NULL "+
        "AND orders.post_type = 'shop_order' "+
        "AND orders_meta.meta_key = '_customer_user'"

      val rs = stmt.executeQuery(sql)

      var result: List[CancelledTransaction] = List()

      while (rs.next()) {

        result = CancelledTransaction(
          rs.getInt("course_id"),
          rs.getInt("order_id"),
          rs.getInt("transaction_id"),
          rs.getInt("user_id"),
          rs.getString("user_login"),
          rs.getString("timeMark")
        ) :: result
      }

      rs.close()
      stmt.close()
      CancelledCoursesWithTransactions(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryNotProcessedCancelledCourses: " + t)
        DatabaseTaskError
    }
  }
}


case class CancelledTransaction(courseId: Int, orderId: Int, transactionId: Int, userId: Int, userName: String, timeMark: String)
case class CancelledCoursesWithTransactions(transactions: List[CancelledTransaction]) extends DatabaseTaskResult
