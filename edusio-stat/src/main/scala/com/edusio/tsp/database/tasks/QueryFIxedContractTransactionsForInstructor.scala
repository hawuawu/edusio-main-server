package com.edusio.tsp.database.tasks

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.tasks.QueryAllContracts._
import com.edusio.tsp.database.{DatabaseConfigs, DatabaseTaskError, DatabaseTaskResult, DatabaseTask}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by Wlsek on 7. 5. 2015.
 */
case class QueryFixedContractTransactionsForInstructor(instructorId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()
      val sql = "SELECT * FROM `"+DatabaseConfigs.fixedCommissionsTable+"` WHERE `to_user`=" + instructorId

      val rs = stmt.executeQuery(sql)

      var result: List[FixedContractTransaction] = List()

      while (rs.next()) {
        val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS")
        val time = formatter.parseDateTime(rs.getString("created"))

        result = FixedContractTransaction(
          rs.getInt("id"),
          rs.getInt("contract_id"),
          rs.getInt("from_user"),
          rs.getInt("to_user"),
          rs.getFloat("commission"),
          time
        ) :: result
      }

      rs.close()
      stmt.close()
      FixedContractCommissions(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryFixedContractTransactionsForInstructor: " + t)
        DatabaseTaskError
    }
  }
}

case class FixedContractTransaction(id: Int, contractId: Int, fromUser: Int, toUser: Int, commission: Float, date: DateTime)

case class FixedContractCommissions(transactions: List[FixedContractTransaction]) extends DatabaseTaskResult