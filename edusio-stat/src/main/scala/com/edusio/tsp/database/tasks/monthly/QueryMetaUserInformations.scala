package com.edusio.tsp.database.tasks.monthly

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTaskError, DatabaseTaskResult, DatabaseTask}
import com.edusio.tsp.statistic.monthly.generator.MonthlyStatisticGenerateAbleMessage
import org.json4s.JValue

import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._

/**
 * Created by Wlsek on 12. 11. 2015.
 */


object EdusioProfileFields extends Enumeration {
  val EDUSIO_SCHOOL_FIRST_NAME = Value("contact person first name")
  val EDUSIO_SCHOOL_MIDDLE_NAME = Value("contact person middle name")
  val EDUSIO_SCHOOL_LAST_NAME = Value("contact person last name")
  val EDUSIO_SCHOOL_DATE_OF_BIRTH = Value("contact person date of birth")
  val EDUSIO_SCHOOL_PHONE = Value("contact person phone")

  val EDUSIO_INSTRUCTOR_AFFILIATE_FIRST_NAME = Value("first name")
  val EDUSIO_INSTRUCTOR_AFFILIATE_MIDDLE_NAME = Value("middle name")
  val EDUSIO_INSTRUCTOR_AFFILIATE_LAST_NAME = Value("last name")
  val EDUSIO_INSTRUCTOR_AFFILIATE_DATE_OF_BIRTH = Value("date of birth")
  val EDUSIO_INSTRUCTOR_AFFILIATE_PHONE = Value("phone")

  val EDUSIO_ALL_LEGAL_RELATIONSHIP = Value("legal relationship")
  val EDUSIO_ALL_ADDRESS = Value("address")
  val EDUSIO_ALL_COUNTRY = Value("country")
  val EDUSIO_ALL_COMPANY_IDENTIFICATION_NUMBER = Value("company identification number")
}

case class MetaUserInformations(
                                 id: Option[Int] = None,
                                 firstName: Option[String] = None,
                                 middleName: Option[String] = None,
                                 lastName: Option[String] = None,
                                 dateOfBirth: Option[String] = None,
                                 phone: Option[String] = None,
                                 legalRelationship: Option[String] = None,
                                 address: Option[String] = None,
                                 country: Option[String] = None,
                                 companyIdentificationNumber: Option[String] = None
                                 ) extends MonthlyStatisticGenerateAbleMessage {
  def serialize: JValue = {
    ("id" -> id.getOrElse(-1)) ~
      ("firstName" -> firstName.getOrElse("")) ~
      ("middleName" -> middleName.getOrElse("")) ~
      ("lastName" -> lastName.getOrElse("")) ~
      ("dateOfBirth" -> dateOfBirth.getOrElse("")) ~
      ("phone" -> phone.getOrElse("")) ~
      ("legalRelationship" -> legalRelationship.getOrElse("")) ~
      ("address" -> address.getOrElse("")) ~
      ("country" -> country.getOrElse("")) ~
      ("companyIdentificationNumber" -> companyIdentificationNumber.getOrElse(""))
  }

  def deserialize(raw: JValue) = {
    implicit lazy val formats = org.json4s.DefaultFormats

    raw match {
      case JNothing =>
        MetaUserInformations(None)
      case t =>
        MetaUserInformations(
          Some((raw \ "id").extract[Int]),
          Some((raw \ "firstName").extract[String]),
          Some((raw \ "middleName").extract[String]),
          Some((raw \ "lastName").extract[String]),
          Some((raw \ "dateOfBirth").extract[String]),
          Some((raw \ "phone").extract[String]),
          Some((raw \ "legalRelationship").extract[String]),
          Some((raw \ "address").extract[String]),
          Some((raw \ "country").extract[String]),
          Some((raw \ "companyIdentificationNumber").extract[String])
        )

    }

  }
}

case class QueryMetaUserInformations(userId: Int) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      val sql = "SELECT fields.name, fields_data.value FROM wp_bp_xprofile_fields as fields " +
        "LEFT JOIN wp_bp_xprofile_data as fields_data ON fields_data.field_id = fields.id " +
        "WHERE fields_data.user_id = "+userId+" AND LOWER(fields.name) " +
        "IN (" +
        "'" + EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_FIRST_NAME + "'," +
        "'" + EdusioProfileFields.EDUSIO_SCHOOL_FIRST_NAME + "'," +
        "'" + EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_MIDDLE_NAME + "'," +
        "'" + EdusioProfileFields.EDUSIO_SCHOOL_MIDDLE_NAME + "'," +
        "'" + EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_LAST_NAME + "'," +
        "'" + EdusioProfileFields.EDUSIO_SCHOOL_LAST_NAME + "'," +
        "'" + EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_DATE_OF_BIRTH + "'," +
        "'" + EdusioProfileFields.EDUSIO_SCHOOL_DATE_OF_BIRTH + "'," +
        "'" + EdusioProfileFields.EDUSIO_SCHOOL_PHONE + "'," +
        "'" + EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_PHONE + "'," +
        "'" + EdusioProfileFields.EDUSIO_ALL_ADDRESS + "'," +
        "'" + EdusioProfileFields.EDUSIO_ALL_COUNTRY + "'," +
        "'" + EdusioProfileFields.EDUSIO_ALL_LEGAL_RELATIONSHIP + "'," +
        "'" + EdusioProfileFields.EDUSIO_ALL_COMPANY_IDENTIFICATION_NUMBER + "'" +
        ")"

      val rs = stmt.executeQuery(sql)

      var result: MetaUserInformations = MetaUserInformations(id = Some(userId))

      while (rs.next()) {
        EdusioProfileFields.withName(rs.getString("name").toLowerCase) match {
          case EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_FIRST_NAME =>
            result = result.copy(firstName = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_SCHOOL_FIRST_NAME =>
            result = result.copy(firstName = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_MIDDLE_NAME =>
            result = result.copy(middleName = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_SCHOOL_MIDDLE_NAME =>
            result = result.copy(middleName = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_LAST_NAME =>
            result = result.copy(lastName = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_SCHOOL_LAST_NAME =>
            result = result.copy(lastName = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_DATE_OF_BIRTH =>
            result = result.copy(dateOfBirth = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_SCHOOL_DATE_OF_BIRTH =>
            result = result.copy(dateOfBirth = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_SCHOOL_PHONE =>
            result = result.copy(phone = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_INSTRUCTOR_AFFILIATE_PHONE =>
            result = result.copy(phone = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_ALL_ADDRESS =>
            result = result.copy(address = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_ALL_COUNTRY =>
            result = result.copy(country = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_ALL_LEGAL_RELATIONSHIP =>
            result = result.copy(legalRelationship = Some(rs.getString("value")))
          case EdusioProfileFields.EDUSIO_ALL_COMPANY_IDENTIFICATION_NUMBER =>
            result = result.copy(companyIdentificationNumber = Some(rs.getString("value")))
        }
      }

      rs.close()
      stmt.close()
      QueriedMetaUserInformations(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QueryMetaUserInformations: " + t)
        DatabaseTaskError
    }
  }
}

case class QueriedMetaUserInformations(informations: MetaUserInformations) extends DatabaseTaskResult