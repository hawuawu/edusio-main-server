package com.edusio.tsp.database.tasks.monthly

import java.sql.{SQLNonTransientConnectionException, Connection}

import com.edusio.tsp.EntryPointService
import com.edusio.tsp.database.{DatabaseTask, DatabaseTaskError, DatabaseTaskResult}
import com.edusio.tsp.statistic.monthly.generator.MonthlyStatisticGenerateAbleMessage
import org.joda.time.DateTime

/**
 * Created by Wlsek on 30. 10. 2015.
 */

case class SumOfLockedEventCommissionByUserAndRelation(value: Option[Float]) extends DatabaseTaskResult with MonthlyStatisticGenerateAbleMessage {
  def serialize = throw new NotImplementedError()
}
case class QuerySumOfLockedEventCommissionByUserAndRelation (userId: Int, complaintLimit: Long, timeAnchor: DateTime, relation: CommissionRelation.Value*) extends DatabaseTask {
  override def doTask(connection: Connection): DatabaseTaskResult = {
    try {
      val stmt = connection.createStatement()

      var relationWhere = ""
      relation.map(r => relationWhere = relationWhere + "'" + r.toString + "',")
      relationWhere = "AND commissions.commission_relation IN (" + (if (relation.size > 0) {
        relationWhere.substring(0, relationWhere.size - 1)
      } else {
        relationWhere
      }) + ") "

      val sql = "SELECT SUM(commissions.commission) as value " +
        "FROM `edusio_commissions_to_pay`  as commissions " +
        "JOIN edusio_transactions as transactions ON transactions.id = commissions.transaction_id " +
        "JOIN wp_posts as orders ON orders.id = transactions.order_id " +
        "JOIN wp_users as instructors ON instructors.id = from_user " +
        "JOIN wp_my_calendar_events as occurs ON occurs.occur_id = bound_to_id " +
        "JOIN wp_posts as courses ON courses.ID = commissions.course_id " +
        "JOIN wp_postmeta as customer_meta ON customer_meta.post_id = transactions.order_id " +
        "WHERE user_id = "+userId+" " +
        "AND commission_bond_to = 'event' " +
        "AND transactions.transaction_done = 1 " +
        "AND customer_meta.meta_key = '_customer_user' " +
        "AND occurs.occur_end + INTERVAL 172800 SECOND >= '"+timeAnchor.toString("yyyy-MM-dd hh:mm:ss")+"' " +
        "AND MONTH(orders.post_date) = " + timeAnchor.getMonthOfYear() + " "
        relationWhere

      val rs = stmt.executeQuery(sql)

      var result: Option[Float] = None

      while (rs.next()) {
        result = Some(rs.getFloat("value"))
      }
      rs.close()
      stmt.close()
      SumOfLockedEventCommissionByUserAndRelation(result)
    } catch {
      case e: SQLNonTransientConnectionException =>
        log.error("Error in database, restarting whole connector.")
        EntryPointService.restart
        DatabaseTaskError
      case t: Throwable =>
        log.error("Error QuerySumOfLockedEventCommissionByUserAndRelation: " + t)
        DatabaseTaskError
    }
  }
}


/*
SELECT SUM(commissions.commission)
FROM `edusio_commissions_to_pay`  as commissions
JOIN edusio_transactions as transactions ON transactions.id = commissions.transaction_id
JOIN wp_posts as orders ON orders.id = transactions.order_id
JOIN wp_users as instructors ON instructors.id = from_user
JOIN wp_my_calendar_events as occurs ON occurs.occur_id = bound_to_id
JOIN wp_posts as courses ON courses.ID = commissions.course_id
JOIN wp_postmeta as customer_meta ON customer_meta.post_id = transactions.order_id
WHERE user_id = 26
AND commission_bond_to = 'event'
AND transactions.transaction_done = 1
AND customer_meta.meta_key = '_customer_user'
AND occurs.occur_end + INTERVAL 172800 SECOND >= '2015-08-30 23:00:00'
AND MONTH(orders.post_date) = 8
 */