package com.edusio.tsp.statistic

import akka.actor._
import com.edusio.tsp.database.DatabaseTaskError
import com.edusio.tsp.database.tasks._
import com.edusio.tsp.database.tasks.monthly.{RemovedOlderMonthlyCommissionStatistics, QueryRemoveOlderMonthlyCommissionStatistic}
import com.edusio.tsp.statistic
import com.edusio.tsp.tools.ActorWithTimeoutSupport
import org.joda.time.DateTime
import scala.collection.immutable.HashMap
import scala.collection.mutable.Queue
import scala.concurrent.duration._

/**
 * Created by Wlsek on 4. 5. 2015.
 */

case class EnqueueUser(userId: Int)

case class UserDone(userId: Int)

case class UserError(userId: Int)

case object BatchComplete

class StatisticManager(databaseManager: ActorRef, timing: Long, simultaneousProcessing: Int, complaintLimit: Long, numberOfMonthsInMonthlyStatistic: Int, chunkSize: Int) extends ActorWithTimeoutSupport {

  private val queue: Queue[QueuedUser] = Queue()

  context.actorOf(Props(classOf[InitialBatch], databaseManager, self))
  private var actualProcessing: Int = 0
  private var processing: HashMap[Int, ActorRef] = HashMap()

  def receive = {
    case BatchComplete =>
      startTimeout(timing millis)

    case eu: EnqueueUser =>
      if (!queue.exists(i => i.id == eu.userId)) {
        queue.enqueue(QueuedUser(eu.userId, sender))
        dequeue
      }

    case ud: UserDone =>
      actualProcessing -= 1
      dequeue

    case ue: UserError =>
      actualProcessing -= 1
      //queue.enqueue(QueuedUser(ue.userId, sender))
      dequeue
  }

  def dequeue = {
    if (actualProcessing < simultaneousProcessing && queue.size > 0) {
      actualProcessing += 1
      val d = queue.dequeue
      if (processing.exists(p => p._1 == d.id)) {
        context.stop(processing(d.id))
        processing = processing.filter(p => p._1 != d.id)
      }
      processing += d.id -> context.actorOf(Props(classOf[UserStatisticProcessor], databaseManager, List(self, d.handler), d.id, complaintLimit, numberOfMonthsInMonthlyStatistic, chunkSize))
    }
  }

  def onTimeout = {
    context.actorOf(Props(classOf[InitialBatch], databaseManager, self))
  }

  case class QueuedUser(id: Int, handler: ActorRef)

}

class InitialBatch(databaseManager: ActorRef, parent: ActorRef) extends Actor {
  private var limit = 20
  private var offset = 0
  private var workingOnUsers: List[QueriedUser] = List()

  next

  def receive = {
    case qu: QueriedUsers =>
      workingOnUsers = qu.users
      offset += limit
      if (qu.users.size == 0) {
        parent ! BatchComplete
        context.stop(self)
      } else {
        workingOnUsers.map(wou => parent ! EnqueueUser(wou.id))
      }
    case usr: UserDone =>
      workingOnUsers = workingOnUsers.filter(wou => wou.id != usr.userId)
      next
    case usr: UserError =>
      workingOnUsers = workingOnUsers.filter(wou => wou.id != usr.userId)
      next
    case DatabaseTaskError =>
      context.stop(self)
  }

  def next = {
    if (workingOnUsers.size == 0) {
      databaseManager ! QueryUserBatch(20, offset)
    }
  }
}

class UserStatisticProcessor(
                              databaseManager: ActorRef,
                              listeners: List[ActorRef],
                              userId: Int,
                              complaintLimit: Long,
                              numberOfMonthsInMonthlyStatistic: Int,
                              chunkSize: Int
                              ) extends Actor with ActorLogging {

  log.debug("Initializing UserStatisticProcessor for user {}.", userId)

  private var paidCommisions: List[Commision] = List()
  private var receivedCommissions: List[Commision] = List()
  private var receivedFixedCommissions: List[FixedContractTransaction] = List()

  private var userDetails: HashMap[Int, QueriedUser] = HashMap(0 -> QueriedUser(0, "Edusio", "Edusio"))
  private var courseEvents: HashMap[Int, EventsForCourse] = HashMap()

  private var courseForEventsQuery: Set[Int] = Set()
  private var alreadyPaid: Float = 0

  private var statisticToUpdate: List[Statistic] = List()

  databaseManager ! QueryProcessedPaymentRequestsForUser(userId)

  def receive = {
    case ppr: ProcessedPaymentRequestsForUser =>
      ppr.requests.map(r => alreadyPaid += r.amount)
      databaseManager ! QueryFixedContractTransactionsForInstructor(userId)

    case qft: FixedContractCommissions =>
      receivedFixedCommissions = qft.transactions
      databaseManager ! QueryPaidCommissionsForUserId(userId)

    case pc: PaidCommissions =>
      paidCommisions = pc.commissions
      databaseManager ! QueryReceivedCommissionsForUserId(userId)

    case rc: ReceivedCommissions =>
      receivedCommissions = rc.commissions

      paidCommisions.map(pc => {
        courseForEventsQuery += pc.courseId
      })

      receivedCommissions.map(rc => {
        courseForEventsQuery += rc.courseId
      })

      if (courseForEventsQuery.size > 0) {
        log.debug("Query events for course {} and user {}.", courseForEventsQuery.head, userId)
        databaseManager ! QueryEventsForCourseId(courseForEventsQuery.head)
      } else {
        log.debug("Reset statistic for user {}.", userId)
        databaseManager ! QueryRemoveStatisticsForUser(userId)
      }

    case QueryRemoveStatisticsForUserDone =>
      log.debug("Initializing empty basic statistic for user {}.", userId)
      val nowDate = new DateTime()
      val now = nowDate.getMillis
      statisticToUpdate = Statistic(userId, userId, "courses-sold-locked", "", 0, nowDate) :: Statistic(userId, userId, "courses-sold-available", "", 0, nowDate) :: statisticToUpdate
      statisticToUpdate.map(s => databaseManager ! QueryUpdateStatistic(s))

    case rc: EventsForCourse =>
      courseEvents += courseForEventsQuery.head -> rc.copy(events = rc.events.sortWith(_.end.getMillis > _.end.getMillis))
      courseForEventsQuery = courseForEventsQuery.tail

      if (courseForEventsQuery.size > 0) {
        databaseManager ! QueryEventsForCourseId(courseForEventsQuery.head)
      } else {

        var lockedValue = 0f
        var availableValue = 0f
        val nowDate = new DateTime()
        val now = nowDate.getMillis

        receivedFixedCommissions.map(c => {
          availableValue += c.commission
        })

        receivedCommissions.map(c => {
          if (c.commisionBoundTo == "event") {
            val events = courseEvents(c.courseId).events

            if (now >= (events.head.end.getMillis() + complaintLimit)) {
              availableValue += c.commision
            } else {
              lockedValue += c.commision
            }
          } else {
            availableValue += c.commision
          }
        })

        statisticToUpdate = Statistic(userId, userId, "courses-sold-locked", "", lockedValue, nowDate) :: Statistic(userId, userId, "courses-sold-available", "", availableValue - alreadyPaid, nowDate) :: statisticToUpdate
        statisticToUpdate.map(s => databaseManager ! QueryUpdateStatistic(s))
      }

    case u: StatisticUpdated =>
      statisticToUpdate = statisticToUpdate.filter(s => u.statistic != s)
      if (statisticToUpdate.size == 0) {
        context.actorOf(Props(classOf[UpdateMonthlyCommissionStatisticProcessor], databaseManager, self, userId, numberOfMonthsInMonthlyStatistic, chunkSize))
      }

    case MonthlyCommissionStatisticUpdated =>
      listeners.map(l => l ! UserDone(userId))

    case MonthlyCommissionStatisticCannotBeUpdated =>
      listeners.map(l => l ! UserError(userId))

    case DatabaseTaskError =>
      listeners.map(l => l ! UserError(userId))
  }
}

case object MonthlyCommissionStatisticUpdated

case object MonthlyCommissionStatisticCannotBeUpdated

trait MonthlyCommissionStatisticProcessorStates

case object FSMInitialization extends MonthlyCommissionStatisticProcessorStates

case object ProcessingPrimaryAffiliate extends MonthlyCommissionStatisticProcessorStates

case object ProcessingSecondaryAffiliate extends MonthlyCommissionStatisticProcessorStates

case object ProcessingInstructor extends MonthlyCommissionStatisticProcessorStates

case object ProcessingSchool extends MonthlyCommissionStatisticProcessorStates

case object ProcessOlderStatistics extends MonthlyCommissionStatisticProcessorStates

case class MonthlyCommissionStatisticProcessorData(
                                                    userRoles: List[MonthlyReportRoles.Value] = List(),
                                                    affiliateLevels: List[AffiliateLevel.Value] = List(),
                                                    processingStatistics: List[Statistic] = List(),
                                                    actualOffsetSettings: Option[OffsetSetting] = None
                                                    )

class UpdateMonthlyCommissionStatisticProcessor(
                                                 databaseManager: ActorRef,
                                                 parent: ActorRef,
                                                 userId: Int,
                                                 numberOfMonths: Int,
                                                 chunkSize: Int
                                                 ) extends Actor with FSM[MonthlyCommissionStatisticProcessorStates, MonthlyCommissionStatisticProcessorData] {

  log.debug("Initializing UpdateMonthlyCommissionStatisticProcessor for user {} ", userId)

  databaseManager ! QueryUserRoles(userId)

  startWith(FSMInitialization, MonthlyCommissionStatisticProcessorData())

  when(FSMInitialization) {
    case Event(QueriedUserRoles(roles), data) => {
      log.debug("There are roles {} for user {}.", roles, userId)

      if (roles.contains(MonthlyReportRoles.Affiliate)) {
        log.debug("There is affiliate, queriying primary and secondary affiliate for user {}.", userId)
        databaseManager ! QueryAffiliateLevel(userId)
        stay using data.copy(userRoles = roles)
      } else {
        nextRoleState(data.copy(userRoles = roles))
      }
    }

    case Event(QueriedAffiliateLevel(userId, affLevels), data) => {
      log.debug("Affiliate levels {} came. for user {} ", affLevels, userId)
      nextRoleState(data.copy(affiliateLevels = affLevels))
    }
  }

  when(ProcessingPrimaryAffiliate) {
    case Event(PrimaryAffiliateResult(secondaryAffiliates), data) => {
      if (secondaryAffiliates.isEmpty) {
        data.actualOffsetSettings.get.ascendOffset match {
          case ns: OffsetSetting =>
            databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.Primaryffiliate, ns, numberOfMonths)
            stay using data.copy(actualOffsetSettings = Some(ns))
          case OffsetSettingDone =>
            nextRoleState(data)
        }
      } else {
        val ns = data.actualOffsetSettings.get.increaseOffset
        stay using data.copy(actualOffsetSettings = Some(ns)).copy(processingStatistics = secondaryAffiliates.map(sa => {
          val secondaryAffiliateStatistic = Statistic(
            userId,
            sa.id,
            "monthly.commission.gain.primary_affiliate.secondary_affiliate." + sa.year.toString + "." + sa.month.toString,
            sa.displayName,
            sa.commission,
            new DateTime()
          )
          databaseManager ! QueryUpdateStatistic(secondaryAffiliateStatistic)
          secondaryAffiliateStatistic :: sa.directInstructors.map(di => {
            val directorStatistic = Statistic(
              userId, di.id, "monthly.commission.gain.primary_affiliate."+sa.id+".instructor." + di.year.toString + "." + di.month.toString, di.displayName, di.commission, new DateTime()
            )
            databaseManager ! QueryUpdateStatistic(directorStatistic)
            directorStatistic :: di.courses.map(c => {
              val courseStatistic = Statistic(userId, c.courseId, "monthly.commission.gain.primary_affiliate."+di.id+".course." + c.year.toString + "." + c.month.toString, c.courseTitle, c.commission, new DateTime())
              databaseManager ! QueryUpdateStatistic(courseStatistic)
              courseStatistic
            })
          }).flatten
        }).flatten)
      }
    }

    case Event(StatisticUpdated(stat), data) => {
      val statistics = data.processingStatistics.filter(s => s.statisticName != stat.statisticName)
      if (statistics.size == 0) {
        databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.Primaryffiliate, data.actualOffsetSettings.get, numberOfMonths)
      }
      stay using data.copy(processingStatistics = statistics)
    }
  }

  when(ProcessingSecondaryAffiliate) {
    case Event(SecondaryAffiliateResult(directInstructors), data) => {
      log.debug("SecondaryAffiliateResult for user {}.", userId)
      if (directInstructors.isEmpty) {
        log.debug("SecondaryAffiliateResult is empty, queriying new offset for user {}.", userId)
        data.actualOffsetSettings.get.ascendOffset match {
          case ns: OffsetSetting =>
            log.debug("Offset result is OffsetSetting for user {}.", userId)
            databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.SecondaryAffiliate, ns, numberOfMonths)
            stay using data.copy(actualOffsetSettings = Some(ns))
          case OffsetSettingDone =>
            log.debug("Offset result is OffsetSettingDone for user {}.", userId)
            nextRoleState(data)
        }
      } else {
        log.debug("SecondaryAffiliateResult contains data for user {}.", userId)
        val ns = data.actualOffsetSettings.get.increaseOffset
        stay using data.copy(actualOffsetSettings = Some(ns)).copy(processingStatistics = directInstructors.map(di => {
          val directorStatistic = Statistic(
            userId, di.id, "monthly.commission.gain.secondary_affiliate.instructor." + di.year.toString + "." + di.month.toString, di.displayName, di.commission, new DateTime()
          )
          databaseManager ! QueryUpdateStatistic(directorStatistic)
          directorStatistic :: di.courses.map(c => {
            val courseStatistic = Statistic(userId, c.courseId, "monthly.commission.gain.secondary_affiliate."+di.id+".course." + c.year.toString + "." + c.month.toString, c.courseTitle, c.commission, new DateTime())
            databaseManager ! QueryUpdateStatistic(courseStatistic)
            courseStatistic
          })
        }).flatten
        )
      }
    }

    case Event(StatisticUpdated(stat), data) => {
      val statistics = data.processingStatistics.filter(s => s.statisticName != stat.statisticName)
      log.debug("Statistics updated, number of statistics update left {}.", statistics.size)
      if (statistics.size == 0) {
        log.debug("Query new QueryMonthlyCommissionFromPOV with offset {} for user {}.", data.actualOffsetSettings.get, userId)
        databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.SecondaryAffiliate, data.actualOffsetSettings.get, numberOfMonths)
      }
      stay using data.copy(processingStatistics = statistics)
    }
  }

  when(ProcessingSchool) {
    case Event(SchoolResult(directInstructors), data) => {
      if (directInstructors.isEmpty) {
        data.actualOffsetSettings.get.ascendOffset match {
          case ns: OffsetSetting =>
            databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.School, ns, numberOfMonths)
            stay using data.copy(actualOffsetSettings = Some(ns))
          case OffsetSettingDone =>
            nextRoleState(data)
        }
      } else {
        val ns = data.actualOffsetSettings.get.increaseOffset
        stay using data.copy(actualOffsetSettings = Some(ns)).copy(processingStatistics = directInstructors.map(di => {
          val directorStatistic = Statistic(
            userId, di.id, "monthly.commission.gain.school.instructor." + di.year.toString + "." + di.month.toString, di.displayName, di.commission, new DateTime()
          )
          databaseManager ! QueryUpdateStatistic(directorStatistic)
          directorStatistic :: di.courses.map(c => {
            val courseStatistic = Statistic(userId, c.courseId, "monthly.commission.gain.school."+di.id+".course." + c.year.toString + "." + c.month.toString, c.courseTitle, c.commission, new DateTime())
            databaseManager ! QueryUpdateStatistic(courseStatistic)
            courseStatistic
          })
        }).flatten
        )
      }
    }

    case Event(StatisticUpdated(stat), data) => {
      val statistics = data.processingStatistics.filter(s => s.statisticName != stat.statisticName)
      if (statistics.size == 0) {
        databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.School, data.actualOffsetSettings.get, numberOfMonths)
      }
      stay using data.copy(processingStatistics = statistics)
    }
  }

  when(ProcessingInstructor) {
    case Event(DirectInstructorResult(courses), data) => {
      if (courses.isEmpty) {
        data.actualOffsetSettings.get.ascendOffset match {
          case ns: OffsetSetting =>
            databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.Instructor, ns, numberOfMonths)
            stay using data.copy(actualOffsetSettings = Some(ns))
          case OffsetSettingDone =>
            nextRoleState(data)
        }
      } else {
        val ns = data.actualOffsetSettings.get.increaseOffset
        stay using data.copy(actualOffsetSettings = Some(ns)).copy(processingStatistics = courses.map(c => {
          val courseStatistic = Statistic(userId, c.courseId, "monthly.commission.gain.instructor.course." + c.year.toString + "." + c.month.toString, c.courseTitle, c.commission, new DateTime())
          databaseManager ! QueryUpdateStatistic(courseStatistic)
          courseStatistic
        })
        )
      }
    }

    case Event(StatisticUpdated(stat), data) => {
      val statistics = data.processingStatistics.filter(s => s.statisticName != stat.statisticName)
      if (statistics.size == 0) {
        databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.Instructor, data.actualOffsetSettings.get, numberOfMonths)
      }
      stay using data.copy(processingStatistics = statistics)
    }
  }

  when(ProcessOlderStatistics) {
    case Event(RemovedOlderMonthlyCommissionStatistics, data) => {
      log.debug("Older commissions removed for user {}.", userId)
      parent ! MonthlyCommissionStatisticUpdated
      //  stop()
      stay()
    }
  }

  whenUnhandled {
    case Event(DatabaseTaskError, data) => {
      log.debug("Error in database, cannot update user {}.", userId)
      parent ! MonthlyCommissionStatisticCannotBeUpdated
      //   stop()
      stay()
    }

    case Event(t, data) => {
      log.error("Unhandled message {} for user {}.", t, userId)
      stay()
    }
  }


  onTransition {
    case _ -> ProcessingPrimaryAffiliate =>
      databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.Primaryffiliate, nextStateData.actualOffsetSettings.get, numberOfMonths)
    case _ -> ProcessingSecondaryAffiliate =>
      databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.SecondaryAffiliate, nextStateData.actualOffsetSettings.get, numberOfMonths)
    case _ -> ProcessingSchool =>
      databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.School, nextStateData.actualOffsetSettings.get, numberOfMonths)
    case _ -> ProcessingInstructor =>
      databaseManager ! QueryMonthlyCommissionFromPOV(userId, PointOfViewCommissionRole.Instructor, nextStateData.actualOffsetSettings.get, numberOfMonths)
    case _ -> ProcessOlderStatistics =>
      databaseManager ! QueryRemoveOlderMonthlyCommissionStatistic(userId, numberOfMonths)

  }

  def nextRoleState(data: MonthlyCommissionStatisticProcessorData): UpdateMonthlyCommissionStatisticProcessor.this.type#State = {
    if (data.userRoles.size > 0) {
      data.userRoles.head match {
        case MonthlyReportRoles.Instructor =>
          goto(ProcessingInstructor) using data
            .copy(userRoles = data.userRoles.tail)
            .copy(actualOffsetSettings = Some(InstructorOffsetSettings(chunkSize).default))

        case MonthlyReportRoles.School =>
          goto(ProcessingSchool) using data
            .copy(userRoles = data.userRoles.tail)
            .copy(actualOffsetSettings = Some(SchoolOffsetSettings(chunkSize).default))

        case MonthlyReportRoles.Affiliate =>
          if (data.affiliateLevels.size > 0) {
            data.affiliateLevels.head match {
              case AffiliateLevel.PrimaryAffiliateLevel =>
                goto(ProcessingPrimaryAffiliate) using data
                  .copy(affiliateLevels = data.affiliateLevels.tail)
                  .copy(actualOffsetSettings = Some(PrimaryAffiliateOffsetSettings(chunkSize).default))

              case AffiliateLevel.SecondaryAffiliateLevel =>
                goto(ProcessingSecondaryAffiliate) using data
                  .copy(affiliateLevels = data.affiliateLevels.tail)
                  .copy(actualOffsetSettings = Some(SecondaryAffiliateOffsetSettings(chunkSize).default))
            }
          } else {
            nextRoleState(data.copy(userRoles = data.userRoles.tail))
          }
        case t =>
          nextRoleState(data.copy(userRoles = data.userRoles.tail))
      }
    } else {
      goto(ProcessOlderStatistics) using data
    }
  }
}