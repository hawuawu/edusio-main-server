package com.edusio.tsp.log

import akka.event.LoggingAdapter
/**
 * Created by Wlsek on 8. 5. 2015.
 */
object Logger {
  var addapter: Option[LoggingAdapter] = None
}

trait EdusioLoggable {
  def log = Logger.addapter.get
}