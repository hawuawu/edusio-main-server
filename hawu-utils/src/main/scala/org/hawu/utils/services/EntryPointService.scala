package org.hawu.utils.services

/**
 * Created by Wlsek on 4. 6. 2015.
 */
trait EntryPointService {
  def start
  def stop
  def restart
}
