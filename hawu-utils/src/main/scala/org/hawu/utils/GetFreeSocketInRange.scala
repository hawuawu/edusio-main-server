package org.hawu.utils

import java.net.ServerSocket

/**
 * Created by arnostkuchar on 11.02.15.
 */

object AllDone extends Exception {}

trait GetFreeSocketInRangeTrait {
  /*
    Range(1, 2)
   */
  def freePortInRange(range: Range): Option[Int] = {
    var toReturn: Option[Int] = None
    try {
      for (a <- range) {
        try {
          val socket = new ServerSocket(a)
          toReturn = Some(a)
          socket.close()
          throw AllDone
        } catch {
          case t: java.net.BindException =>
            toReturn = None;
        }
      }
    } catch {
      case AllDone =>
      case t: Throwable =>
        toReturn = None;
    }
    toReturn
  }
}
