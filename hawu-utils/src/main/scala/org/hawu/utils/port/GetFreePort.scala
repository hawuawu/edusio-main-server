package org.hawu.utils.port

import java.net.{BindException, ServerSocket}

import scala.util.Random

/**
 * Created by Wlsek on 16. 6. 2015.
 */
class PortFoundException extends Exception
object GetFreePort {
  def apply(range: Range): Option[Int] = {
    var portToReturn: Option[Int] = None
    try {
      for (i <- range) {
        try {
          val ss = new ServerSocket(i)
          ss.close()
          portToReturn = Some(i)
        } catch {
          case t: Throwable =>
        }
        if (portToReturn.isDefined) throw new PortFoundException()
      }
    } catch {
      case e: PortFoundException =>
    }
    portToReturn
  }
}
