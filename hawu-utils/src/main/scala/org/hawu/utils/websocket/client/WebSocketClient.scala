package org.hawu.utils.websocket.client

import java.io.{IOException, File}
import java.net.{URLDecoder, URI}
import java.security.KeyStore
import java.util.logging.Level

import akka.actor.ActorRef
import org.eclipse.jetty.util.resource.Resource
import org.eclipse.jetty.util.ssl.SslContextFactory
import org.eclipse.jetty.websocket.api.{RemoteEndpoint, Session}
import org.eclipse.jetty.websocket.client.{WebSocketClient, ClientUpgradeRequest}
import org.eclipse.jetty.websocket.api.annotations._
;

import org.json4s.JsonAST._
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
/**
 * Created by arnostkuchar on 17.02.15.
 */
object WebSocketClientContextFactory {
  def apply(uri: String, parent: ActorRef,  keyStore: Resource): WebSocketClientContext = {
    new WebSocketClientContext(uri, parent, keyStore)
  }
}


case object ClientConnected
case object ClientDisconnected
case object ClientCannotConnect

case class TextMessage(message: String)
case class JsonMessage(message: JValue)

@WebSocket(maxTextMessageSize = 64 * 1024)
class WebSocketClientContext(uri: String, parent: ActorRef, keyStore: Resource) {
  (org.slf4j.LoggerFactory.getLogger("org.eclipse.jetty")).asInstanceOf[ch.qos.logback.classic.Logger].setLevel(ch.qos.logback.classic.Level.WARN)

  val dest = new URI(uri);
  val request = new ClientUpgradeRequest()

  val sslContextFactory = new SslContextFactory();
   sslContextFactory.setKeyStoreResource(keyStore);
   sslContextFactory.setKeyStorePassword("classconnector12214");
   sslContextFactory.setKeyManagerPassword("classconnector12214");

  val client = new WebSocketClient(sslContextFactory)

  private var session: Option[Session] = None
  private var remote: Option[RemoteEndpoint] = None

  @OnWebSocketClose
  def onClose(statusCode: Int, reason: String ): Unit = {
    if(session.isDefined) {
      parent ! ClientDisconnected
      session = None
      remote = None
    } else {
      parent ! ClientCannotConnect
    }
  }

  @OnWebSocketError
  def onError(session: Session, th: Throwable): Unit = {
    if(session == null) {
      parent ! ClientCannotConnect
    }
  }

  @OnWebSocketConnect
  def  onConnect(session: Session): Unit = {
    this.session = Some(session)
    remote = Some(session.getRemote)
    parent ! ClientConnected
  }

  @OnWebSocketMessage
  def onMessage(msg: String): Unit = {
    val json = parse(msg)
    if(json != null) {
      parent ! JsonMessage(json)
    } else {
      parent ! msg
    }
  }

  def send(msg: String): Unit = {
    try {
      remote.map(endpoint => session.map(s => {
        if (s.isOpen) endpoint.sendString(msg)
      }))
    } catch {
      case e: IOException =>
    }
  }

  def send(msg: org.json4s.JsonAST.JObject): Unit = {
    send(pretty(render(msg)))
  }

  def connect (): Unit = {
    client.start()
    client.connect(this, dest, request);
  }

  def disconnect(): Unit = {
    //remote.map(r => r.flush())
    session.map(s => s.close(0, ""))
  }
}

