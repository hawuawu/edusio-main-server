package org.hawu.utils.file

import java.io.File
import java.net.URLDecoder

/**
 * Created by Wlsek on 4. 6. 2015.
 */
trait JarLocation {
  val actualPath = URLDecoder.decode( new File(this.getClass.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile.getPath, "UTF-8")
  def / = File.separator
}
