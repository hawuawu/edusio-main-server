package org.hawu.utils.string


import java.security.MessageDigest

import org.bouncycastle.util.encoders.Hex

/**
 * Created by Wlsek on 9. 6. 2015.
 */
object SimpleNumberToStringEncoding {
  def encode(integer: Int) = {
    new String(Hex.encode(MessageDigest.getInstance("MD5").digest(integer.toString.getBytes)), "UTF-8")
  }
}
