package org.hawu.utils

import akka.actor.{ActorRef, Actor, Props, ActorSystem}
import org.hawu.utils.port.GetFreePort
import org.hawu.utils.string.SimpleNumberToStringEncoding

/**
 * Created by arnostkuchar on 03.06.15.
 */
object Killers extends App  {
  override def main (args: Array[String]): Unit = {
    println(GetFreePort(32000 to 48000))
    //println(SimpleNumberToStringEncoding.encode(2532))

      /*
        val as = ActorSystem("as")
        try {
          val killer = as.actorOf(Props[ProcessKiller])
          as.actorOf(Props(classOf[Uii], killer))
        }
        readLine()
        as.shutdown()
      }*/
  }
}



class Uii(killer: ActorRef) extends Actor {
  killer ! TargetProcess(1230000000, "ngrok")
  killer ! TargetProcess(1240000000, "ngrok")
  killer ! TargetProcess(1250000000, "ngrok")
  killer ! TargetProcess(1260000000, "vnc")
  killer ! TargetProcess(1270000000, "vnc")
  killer ! TargetProcess(1280000000, "vnc")

  killer ! KillProcess(Some(1230000000))
  killer ! KillProcess(None, Some("vnc"))

  def receive = {
    case pc: ProcessKilled =>
    case pc: CannotKillProcess =>
  }
}
