package org.hawu.utils

import java.io.{File, FileNotFoundException}

import akka.actor.{Actor, ActorLogging}

import scala.xml.{Elem, XML}

/**
 * Created by arnostkuchar on 03.06.15.
 */

case class TargetProcess(pid: Int, context: String)

case class KillProcess(pid: Option[Int] = None, context: Option[String] = None)

case class ProcessKilled(killCommand: KillProcess)

case class CannotKillProcess(killCommand: KillProcess)

class ProcessKiller extends Actor with ActorLogging {

  private val processKillScript = getClass().getClassLoader().getResource("process_kill.sh").getPath

  val rt = Runtime.getRuntime
  rt.exec("chmod +x " + processKillScript)

  private val killList: String = new File(this.getClass.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile.getPath + File.separator + "kill_list.xml"
  private var targets: List[TargetProcess] = List()
  load

  def receive = {
    case tp: TargetProcess =>
      if(!targets.exists(t => t.pid == tp.pid && t.context == tp.context)) targets = tp :: targets
      save

    case kp: KillProcess =>
      kp.pid.map(pid => {
          val command = processKillScript + " " + pid
          val rt = Runtime.getRuntime
          val p = rt.exec(command)
          p.waitFor()

          if (p.exitValue() == 0) {
            sender ! ProcessKilled(kp)
            targets = targets.filter(t => t.pid != pid)
          } else {
            sender ! CannotKillProcess(kp)
          }
      })

      kp.context.map(context => {
        val filtered = targets.filter(f => f.context == context)
        if(filtered.isEmpty) {
          sender ! ProcessKilled(kp)
        } else {
          var ok = true
          filtered.map(f => {
            val command = processKillScript + " " + f.pid
            val rt = Runtime.getRuntime
            val p = rt.exec(command)
            p.waitFor()
            if (p.exitValue() != 0) {
              ok = false
            } else {
              targets = targets.filter(t => t.pid != f.pid)
            }
          })
          if(ok) {
            sender ! ProcessKilled(kp)
          } else {
            sender ! CannotKillProcess(kp)
          }
        }
      })

      save
  }

  def toXml(processes: List[TargetProcess]): Elem = {
    <killTargets>
      {processes.map(pt => {
      <killTarget>
        <pid>
          {pt.pid}
        </pid>

        <context>
          {pt.context}
        </context>
      </killTarget>
    })}
    </killTargets>
  }

  def fromXml(xml: Elem): List[TargetProcess] = (xml \ "killTargets" \ "killTarget").map(kt => {
    TargetProcess((kt \ "pid").text.toInt, (kt \ "context").text)
  }).toList

  def save = XML.save(killList, toXml(targets))

  def load = {
    try {
      targets = fromXml(XML.loadFile(killList))
    } catch {
      case e: FileNotFoundException =>
        new File(killList).createNewFile()
        XML.save(killList, <killTargets></killTargets>)
    }
  }
}
