#!/bin/bash
ps cax | grep "^$1" > /dev/null
if [ "$?" -eq 0 ]; then
kill $1
ps cax | grep "^$1" > /dev/null
exit $?
else
exit 0
fi
